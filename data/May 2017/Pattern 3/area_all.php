<?php $BreadList = 0; ?>  <!--Pang Ku Su validation-->
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php if (function_exists("get_header_tag")) {get_header_tag();} ?>
<?php if (function_exists("canonical_url")) {canonical_url();} ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php home_url(); ?>css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php home_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php home_url(); ?>js/mh.js"></script>
<script type="text/javascript">
  $(function() {
    $('.height').matchHeight();
    $('.height').matchHeight();
  });

  jQuery(document).ready(function($){
     var url = window.location.href;
     $('.NAVI ul li a[href="'+url+'"]').addClass('active');

   });
</script>

</head>
<body>

  <div class="HEADER">

    <div class="NAVI">
      <ul>
        <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
      </ul>
    </div><!--NAVI-->
        <div class="H1">
      <h1><a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a></h1>
    </div>
    <div class="MAIN_IMAGE">
		<div class="block_main_img">
    		<?php if ($filename == "index.php") : ?>
          <div class="main_banner">

      		<img class="banner" src="<?php home_url(); ?>images/main_image.jpg" alt="">

      		</div><!--main_banner-->

    		<div class="block_top_main">
    			<?php if (1 <= $BlockCount) : ?>
    				<div class="BLOCK_FRAME">
    				  <?php if(function_exists("d_block")) {d_block(1, 1,
    					  array(
    						'DisplayImage' => 0,
    						'Variation' => 7,
    						'Skip'      => 1,
    						'BlockTag'  => 'div',
    						'BlockCss'  => 'frame_01 composite_block',
    						'TitleCss'  => 'title_h2',
    						'ImageCss'  => 'image_main_04',
    						'DigestCss' => '',
    						'PrCss'     => '',
    						'MoreCss'   => 'continue_reading',
    						'MoreText'  => 'More Detail ',
    					  )
    					);
    				  }?>
    				</div><!--End BLOCK_FRAME-->
    			<?php endif; ?>
    		</div><!--top_block-->


    		<?php else: ?>
    		<div class="sub_banner">
    		<img class="banner" src="<?php home_url(); ?>images/sub_image.jpg" alt="">
    		</div>
    		<?php endif; ?>
    	</div>
		</div>




  </div><!--HEADER -->

<?php //TOPページ@page_0@****************************************************************************************************
if ($filename == "index.php") : ?>
<div class="WRAPPER">

  <div class="site_left">
    <div class="BLOCK_FRAME">
      <div class="SIDE_NAVI">
      <h2>最新記事一覧</h2>
      <ul class="menu">
        <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
      </ul>
      </div><!-- end NAVI_main -->
    </div><!-- end BLOCK_FRAME -->

    <?php if (1 <= $BlockCount) : ?>
    <div class="BLOCK_FRAME">
      <?php if(function_exists("d_block")) {
      d_block(2, $BlockCount,
        array(
        'DisplayImage' => 1,
        'Variation' => 7,
        'Skip'      => 1,
        'BlockTag'  => 'div',
        'BlockCss'  => 'frame_04 composite_block',
        'TitleCss'  => 'title_h2',
        'ImageCss'  => '',
        'DigestCss' => '',
        'PrCss'     => '',
        'MoreCss'   => 'detail',
        'MoreText'  => '詳細を読む',
        )
      );
      }?>
    </div><!--End BLOCK_FRAME-->
    <?php endif; ?>

    <?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
    <div class="BLOCK_FRAME">
      <div class="BLOCK_PRLINK composite_block">
      <h2>関連サイトを見る</h2>
      <ul>
        <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
        <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
        <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
        <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
        <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
      </ul>
      </div>
    </div><!-- end BLOCK_FRAME -->
    <?php } ?>

  </div><!-- site_left -->

  <div class="site_main">

    <div class="BLOCK_FRAME">
      <div class="TOP_CONTENTS composite_block">
      <?php if (!empty($pagetitle[1][1])):?> <h2><?php echo $pagetitle[1][1]; ?></h2> <?php endif;?>
      <?php $DisplayTopImage = 1;
      if($DisplayTopImage == 1) {if (!empty($pageimage[1][1])) {echo '<img src="'. $read_home_url . 'images/' . $pageimage[1][1] . '" alt="' . $pagealt[1][1] . '" />';}} ?>
      <?php if (!empty($pr_link[6])) {echo '<p class="pr_link">'. $pr_link[6] . '</p>'; }?>
      <?php if (!empty($pr_link[7])) {echo '<p class="pr_link">'. $pr_link[7] . '</p>'; }?>
      <?php if (!empty($top_original_content)) {echo $top_original_content; }?>
      <?php if (!empty($pr_link[8])) {echo '<p class="pr_link">'. $pr_link[8] . '</p>'; }?>
      <?php if (!empty($pr_link[9])) {echo '<p class="pr_link">'. $pr_link[9] . '</p>'; }?>
      <?php if (!empty($pr_link[10])) {echo '<p class="pr_link">'. $pr_link[10] . '</p>'; }?>
      </div><!-- blocks -->
    </div><!-- end BLOCK_FRAME -->

    <?php if (1 <= $BlockCount) : ?>
    <div class="BLOCK_FRAME">
      <?php if(function_exists("d_block")) {
      d_block(2, $BlockCount,
        array(
        'DisplayImage' => 1,
        'Variation' => 7,
        'Skip'      => 1,
        'BlockTag'  => 'div',
        'BlockCss'  => 'frame_03 composite_block height',
        'TitleCss'  => 'title_h2',
        'ImageCss'  => '',
        'DigestCss' => '',
        'PrCss'     => '',
        'MoreCss'   => 'detail',
        'MoreText'  => '詳細を読む',
        )
      );
      }?>
    </div><!--End BLOCK_FRAME-->
    <?php endif; ?>

    <?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
    <div class="BLOCK_FRAME">
      <div class="BLOCK_PRLINK composite_block">
      <h2>関連サイトを見る</h2>
      <ul>
        <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
        <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
        <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
        <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
        <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
      </ul>
      </div>
    </div><!-- end BLOCK_FRAME -->
    <?php } ?>

    <?php if (1 <= $BlockCount) : ?>
    <div class="BLOCK_FRAME">
      <?php if(function_exists("d_block")) {
      d_block(2, $BlockCount,
        array(
        'DisplayImage' => 1,
        'Variation' => 7,
        'Skip'      => 1,
        'BlockTag'  => 'div',
        'BlockCss'  => 'frame_02 composite_block height',
        'TitleCss'  => 'title_h2',
        'ImageCss'  => '',
        'DigestCss' => '',
        'PrCss'     => '',
        'MoreCss'   => 'detail',
        'MoreText'  => '詳細を読む',
        )
      );
      }?>
    </div><!--End BLOCK_FRAME-->
    <?php endif; ?>

  </div><!-- end site_main -->


</div><!-- end WRAPPER -->
<?php //内部ページ@end_page_0@@page_1@************************************************************************************************************************
elseif ($filename == $flname[2] || $filename == $flname[3] || $filename == $flname[4] || $filename == $flname[5] || $filename == $flname[6]) :?>
 <!--Start Code for Pang Ku Su-->
 <?php if($BreadList == 1) : ?>
   <div class="WRAPPER">
  	<ol class="in-menu_blocks_boss">
          <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
          <li><?php echo $page_title; ?></li>
       </ol>
   </div>
 <?php endif; ?>
 <!--End Code Pang Ku Su-->
<div class="WRAPPER">
	<div class="site_left">
		<div class="BLOCK_FRAME">
		  <div class="SIDE_NAVI">
			<h2>最新記事一覧</h2>
			<ul class="menu">
			  <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
			</ul>
		  </div><!-- end NAVI_main -->
		</div><!-- end BLOCK_FRAME -->
	</div><!-- site_left -->

	<div class="SUB_PAGE">
	  <div class="BLOCK_FRAME">
		<div class="TOP_CONTENTS composite_block">
		  <h2><?php if (!empty($page_title)) {echo $page_title;}?></h2>
			<?php $DisplayInnerImage = 1;
              if($DisplayInnerImage == 1) { if (!empty($sub_content_image)) {echo $sub_content_image;}} ?>
			<?php if (!empty($sub_content)) {echo $sub_content;}?>
		</div>
	  </div><!-- end BLOCK_FRAME -->
	</div><!-- site_main_sub -->



</div><!-- end of WRAPPER -->

<?php //サイトマップ@page_0@****************************************************************************************************
	elseif ($filename == "sitemap.php") : ?>
      <?php if($BreadList == 1) : ?>
          <div class="WRAPPER">
    	       <ol class="in-menu_blocks_boss">
              <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
              <li>サイトマップ</li>
            </ol>
          </div>
      <?php endif; ?>

      <?php if($BreadList == 0) : ?>
        <div class="WRAPPER">
        	<div class="article_layer_three">
        		<div class="article_content_column">
        			<?php sitemap_article_list("HOME"); ?>
        		</div>
          </div>
        <?php endif; ?>

      </div>
<?php //ページ分けおしまい@end_page_1@**************************************************************************************************
endif; ?>

<div class="FOOTER">
  <div class="footer_inner">
  	<div class="footer_navigation">
  		<ul>
  		<?php if(function_exists("footer_link") || (!empty($ft)))  {footer_link($BlockCount);} ?>
  		</ul>
  	</div>

    <p>Copyright &copy; 2017 <a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a> All Rights Reserved.</p>
  </div>
</div>

</body>
</html>
