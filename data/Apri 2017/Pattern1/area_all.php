<?php $BreadList = 1; ?>  <!--Pang Ku Su validation-->
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php if (function_exists("get_header_tag")) {get_header_tag();} ?>
<?php if (function_exists("canonical_url")) {canonical_url();} ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php home_url(); ?>css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php home_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php home_url(); ?>js/mh.js"></script>
<script type="text/javascript">
  $(function() {
    $('.height').matchHeight();
    $('.height').matchHeight();
  });	
</script>

</head>
<body>
	<div class="HEADER">
	    <div class="H1">
			<h1><a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a></h1>		
  		</div>

  <div class="MAIN">	  
      <div class="MAIN_IMAGE">
      	<div class="MAIN_IMAGE_INNER">
         <?php if ($filename == "index.php") : ?>
			  <img src="<?php home_url(); ?>images/main_image.jpg" alt="">
			  <?php else: ?>
			  <img src="<?php home_url(); ?>images/sub_image.jpg" alt="">
		 <?php endif; ?>         
      </div>
	</div>
  </div>
  	<div class="NAVI">
		<ul class="menu">
			<?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
		</ul>
	</div><!--NAVI-->
</div><!-- end_HEADER -->

<?php //TOPページ@page_0@****************************************************************************************************
if ($filename == "index.php") : ?>

  <div class="WRAPPER">

	<div class="contents_body_top">
	
			<?php if (1 <= $BlockCount) : ?>
			<div class="POSTLIST">
			  <?php if(function_exists("d_block")) {
				d_block(1, $BlockCount,
				  array(
					'DisplayImage' => 1,
					'Variation' => 4,     
					'Skip'      => 1,
					'BlockTag'  => 'div',
					'BlockCss'  => 'blog_cont_main02 blog_main_container_frame height',
					'TitleCss'  => 'tag_title',
					'ImageCss'  => 'img',
					'DigestCss' => '',
					'PrCss'     => '',
					'MoreCss'   => '',
					'MoreText'  => 'Read More..',
				  )
				);
			  }?>
			</div><!--End blog_main_container_frame-->
			<?php endif; ?>

				<?php if (1 <= $BlockCount) : ?>		
			<div class="POSTLIST">
			  <?php if(function_exists("d_block")) {
				d_block(1, $BlockCount,
				  array(
					'DisplayImage' => 1,
					'Variation' => 4,
					'Skip'      => 1,
					'BlockTag'  => 'div',
					'BlockCss'  => 'blog_cont_main01 blog_main_container_frame',
					'TitleCss'  => 'data_blocktitle',
					'ImageCss'  => 'img',
					'DigestCss' => '',
					'PrCss'     => '',
					'MoreCss'   => '',
					'MoreText'  => 'Read More..',
				  )
				);
			  }?>
			</div><!--End blog_main_container_frame-->
			<?php endif; ?>

			 <div class="TOP_CONTENTS blog_main_container_frame">
			<?php if (!empty($pagetitle[1][1])):?> <h2><?php echo $pagetitle[1][1]; ?></h2> <?php endif;?>
			<?php $DisplayTopImage = 1; //Display top image (number 0 and 1)
              if($DisplayTopImage == 1) {if (!empty($pageimage[1][1])) {echo '<img src="'. $read_home_url . 'images/' . $pageimage[1][1] . '" alt="' . $pagealt[1][1] . '" />';}} ?>
			<?php if (!empty($pr_link[6])) {echo '<p class="pr_link">'. $pr_link[6] . '</p>'; }?>
			<?php if (!empty($pr_link[7])) {echo '<p class="pr_link">'. $pr_link[7] . '</p>'; }?>
			<?php if (!empty($top_original_content)) {echo $top_original_content; }?>
			<?php if (!empty($pr_link[8])) {echo '<p class="pr_link">'. $pr_link[8] . '</p>'; }?>
			<?php if (!empty($pr_link[9])) {echo '<p class="pr_link">'. $pr_link[9] . '</p>'; }?>
			<?php if (!empty($pr_link[10])) {echo '<p class="pr_link">'. $pr_link[10] . '</p>'; }?>
		  </div><!-- end blog_main_container_frame -->


			<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
			<div class="POSTLIST">
			  <div class="BLOCK_PRLINK blog_main_container_frame">
				<h2>Recommended</h2> <ul>
				  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
				  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
				  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
				  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
				  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
				</ul>
			  </div>
			</div><!-- end blog_main_container_frame -->
			<?php } ?>

	</div><!--End Middle site -->

  	  	  	<div class="section_postlist_right">
       <div class="POSTLIST">
		  <div class="SIDE_NAVI">
			<h2>MENU</h2>
			<ul class="menu">
			  <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
			</ul>
		  </div><!-- end transmit_MENU_Site -->

       </div>

       <!--block side -->

		<?php if (1 <= $BlockCount) : ?>
		<div class="POSTLIST">
		  <?php if(function_exists("d_block")) {
			d_block(1, $BlockCount,
			  array(
				'DisplayImage' => 1,
				'Variation' => 4,
				'Skip'      => 1,
				'BlockTag'  => 'div',
				'BlockCss'  => 'blog_cont_main03 blog_main_container_frame',
				'TitleCss'  => 'tag_title',
				'ImageCss'  => 'img',
				'DigestCss' => '',
				'PrCss'     => '',
				'MoreCss'   => '',
				'MoreText'  => 'Read More..',
			  )
			);
		  }?>
		</div><!--End blog_main_container_frame-->
		<?php endif; ?>  

		<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
		<div class="POSTLIST">
		  <div class="BLOCK_PRLINK blog_main_container_frame">
			<h2>Recommended</h2>
			<ul>
			  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
			  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
			  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
			  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
			  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
			</ul>
		  </div>
		</div><!-- end blog_main_container_frame -->
		<?php } ?>     

  	</div><!--End left bar -->

  </div>

<?php //内部ページ@end_page_0@@page_1@************************************************************************************************************************
elseif ($filename == $flname[2] || $filename == $flname[3] || $filename == $flname[4] || $filename == $flname[5] || $filename == $flname[6]) :?>
 <!--Start Code for Pang Ku Su-->
 <?php if($BreadList == 1) : ?>
 <div class="WRAPPER">
	<ol class="in-navi_mains_boss">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li><?php echo $page_title; ?></li>
     </ol>
  </div>
 <?php endif; ?>
 <!--End Pang Ku Su-->

  <div class="WRAPPER">

  	<div class="section_postlist_right">
       <div class="POSTLIST">
		  <div class="SIDE_NAVI">
       		<h2>MENU</h2>
			<ul class="menu">
			  <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
			</ul>
		  </div><!-- end transmit_MENU_Site -->

       </div>
     </div> 

    <div class="contents_body_topsubpages">
	  <div class="POSTLIST">
		<div class="TOP_CONTENTS blog_main_container_frame">
		  <h2><?php if (!empty($page_title)) {echo $page_title;}?></h2>
			<?php $DisplayInnerImage = 1;
              if($DisplayInnerImage == 1) { if (!empty($sub_content_image)) {echo $sub_content_image;}} ?>
			<?php if (!empty($sub_content)) {echo $sub_content;}?>
		</div>
	  </div><!-- end blog_main_container_frame -->
    </div><!-- end subpage -->

 </div>

<?php //サイトマップ@page_0@****************************************************************************************************
	elseif ($filename == "sitemap.php") : ?>
 <?php if($BreadList == 1) : ?>
 <div class="WRAPPER">
	<ol class="in-navi_mains_boss">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li>サイトマップ</li>
    </ol>
  </div>
 <?php endif; ?>
 <?php if($BreadList == 0) : ?>
 	<div class="WRAPPER">
		<div class="article_layer_three">
			<div class="article_content_column">	
				<?php sitemap_article_list("HOME"); ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php //ページ分けおしまい@end_page_1@**************************************************************************************************
endif; ?>

<div class="FOOTER">

 <div class="FOOTER_Inner">
	<div class="FOOTER_Inner_menu">
		<ul>
		<?php if(function_exists("footer_link") || (!empty($ft)))  {footer_link($BlockCount);} ?>
		</ul>
	</div>	
  <p>Copyright &copy; 2017 <a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a> All Rights Reserved.</p>
 </div>
</div>

</body>
</html>

