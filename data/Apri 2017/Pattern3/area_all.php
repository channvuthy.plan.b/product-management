<?php $BreadList = 0; ?>  <!--Pang Ku Su validation-->
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php if (function_exists("get_header_tag")) {get_header_tag();} ?>
<?php if (function_exists("canonical_url")) {canonical_url();} ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php home_url(); ?>css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php home_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php home_url(); ?>js/mh.js"></script>
<script type="text/javascript">
  $(function() {
    $('.height').matchHeight();
    $('.height').matchHeight();
  });	
</script>

</head>
<body>

<?php //TOPページ@page_0@****************************************************************************************************
if ($filename == "index.php") : ?>

<div class="headecontroll"><!--start headecontroll-->

<div class="site_left">

    <!--start HEADER--> 

    <div class="HEADER">
	       <div class="H1">
					<h1><a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a></h1>
		    </div>

			<div class="NAVI">
						<ul class="menu">
							<?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
						</ul>
			</div><!--NAVI-->

	        <div class="MAIN_IMAGE">
				  <img src="<?php home_url(); ?>images/main_image.jpg" alt="">   
			</div>
    </div><!--end HEADER--> 

  <div class="WRAPPER">
	<div class="events_maincont">

		  <div class="TOP_CONTENTS main_frame">
			<?php if (!empty($pagetitle[1][1])):?> <h2><?php echo $pagetitle[1][1]; ?></h2> <?php endif;?>
			<?php $DisplayTopImage = 1; //Display top image (number 0 and 1)
              if($DisplayTopImage == 1) {if (!empty($pageimage[1][1])) {echo '<img src="'. $read_home_url . 'images/' . $pageimage[1][1] . '" alt="' . $pagealt[1][1] . '" />';}} ?>
			<?php if (!empty($pr_link[6])) {echo '<p class="pr_link">'. $pr_link[6] . '</p>'; }?>
			<?php if (!empty($pr_link[7])) {echo '<p class="pr_link">'. $pr_link[7] . '</p>'; }?>
			<?php if (!empty($top_original_content)) {echo $top_original_content; }?>
			<?php if (!empty($pr_link[8])) {echo '<p class="pr_link">'. $pr_link[8] . '</p>'; }?>
			<?php if (!empty($pr_link[9])) {echo '<p class="pr_link">'. $pr_link[9] . '</p>'; }?>
			<?php if (!empty($pr_link[10])) {echo '<p class="pr_link">'. $pr_link[10] . '</p>'; }?>
		  </div><!-- end main_frame -->

			<?php if (1 <= $BlockCount) : ?>
			<div class="POSTLIST">
			  <?php if(function_exists("d_block")) {
				d_block(1, $BlockCount,
				  array(
					'DisplayImage' => 1,
					'Variation' => 7,     
					'Skip'      => 1,
					'BlockTag'  => 'div',
					'BlockCss'  => 'site_block02 main_frame',
					'TitleCss'  => '',
					'ImageCss'  => 'img',
					'DigestCss' => '',
					'PrCss'     => '',
					'MoreCss'   => 'more_content',
					'MoreText'  => 'もっと読む',
				  )
				);
			  }?>
			</div><!--End main_frame-->
			<?php endif; ?>	
			
			<?php if (1 <= $BlockCount) : ?>		
			<div class="POSTLIST">
			  <?php if(function_exists("d_block")) {
				d_block(1, $BlockCount,
				  array(
					'DisplayImage' => 1,
					'Variation' => 7,
					'Skip'      => 1,
					'BlockTag'  => 'div',
					'BlockCss'  => 'site_block03 main_frame height',
					'TitleCss'  => '',
					'ImageCss'  => 'img',
					'DigestCss' => '',
					'PrCss'     => '',
					'MoreCss'   => 'more_content',
					'MoreText'  => 'もっと読む',
				  )
				);
			  }?>
			</div><!--End main_frame-->
			<?php endif; ?>	 

			<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
			<div class="POSTLIST">
			  <div class="BLOCK_PRLINK main_frame">
				<h2>おすすめ関連リンク</h2>
				<ul>
				  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
				  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
				  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
				  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
				  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
				</ul>
			  </div>
			</div><!-- end main_frame -->
			<?php } ?> 

	</div><!--End Middle site -->

  </div><!--End WRAPPER -->


</div><!--End site_left -->


<div class="site_right">

       <!--block side -->  

		<?php if (1 <= $BlockCount) : ?>
		<div class="POSTLIST">
		  <?php if(function_exists("d_block")) {
			d_block(1, $BlockCount,
			  array(
				'DisplayImage' => 1,
				'Variation' => 7,
				'Skip'      => 1,
				'BlockTag'  => 'div',
				'BlockCss'  => 'site_block04 main_frame',
				'TitleCss'  => '',
				'ImageCss'  => 'img',
				'DigestCss' => '',
				'PrCss'     => '',
				'MoreCss'   => 'more_content',
				'MoreText'  => 'もっと読む',
			  )
			);
		  }?>
		</div><!--End main_frame-->
		<?php endif; ?>   

		<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
		<div class="POSTLIST">
		  <div class="BLOCK_PRLINK main_frame">
			<h2>おすすめ関連リンク</h2>
			<ul>
			  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
			  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
			  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
			  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
			  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
			</ul>
		  </div>
		</div><!-- end main_frame -->
		<?php } ?>  

</div><!--End site_right -->

</div><!--End headecontroll site -->
<?php //内部ページ@end_page_0@@page_1@************************************************************************************************************************
elseif ($filename == $flname[2] || $filename == $flname[3] || $filename == $flname[4] || $filename == $flname[5] || $filename == $flname[6]) :?>
 <!--Start Code for Pang Ku Su-->
 <?php if($BreadList == 1) : ?>
	<ol class="events_pangusu">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li><?php echo $page_title; ?></li>
     </ol>
 <?php endif; ?>
 <!--End Pang Ku Su-->
 
 <!--open sub-->

   <div class="headecontroll"><!--start headecontroll-->

    <div class="subpage_banner">
		    <div class="H1">
            <h1><a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a></h1>
			</div>

		    <div class="NAVI">
				<ul class="menu">
					<?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
				</ul>
		    </div><!--NAVI-->

            <div class="commercial_sub_banner">
			  <img src="<?php home_url(); ?>images/sub_image.jpg" alt="">
			</div>
    </div> <!--end subpage_banner--> 

    <div class="WRAPPER">
    <div class="events_subpages">
	  <div class="POSTLIST">
		<div class="TOP_CONTENTS main_frame">
		  <h2><?php if (!empty($page_title)) {echo $page_title;}?></h2>
			<?php $DisplayInnerImage = 1;
              if($DisplayInnerImage == 1) { if (!empty($sub_content_image)) {echo $sub_content_image;}} ?>
			<?php if (!empty($sub_content)) {echo $sub_content;}?>
		</div>
	  </div><!-- end main_frame -->
    </div><!-- end subpage -->

 </div>

</div><!--End headecontroll site -->
<?php //サイトマップ@page_0@****************************************************************************************************
	elseif ($filename == "sitemap.php") : ?>
 <?php if($BreadList == 1) : ?>
	<ol class="events_pangusu">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li>サイトマップ</li>
    </ol>
 <?php endif; ?>

	<div class="events_sitemap_content">
		<div class="events_content_column">	
			<?php sitemap_article_list("HOME"); ?>
		</div>
	</div>

<?php //ページ分けおしまい@end_page_1@**************************************************************************************************
endif; ?>

<div class="FOOTER">

 <div class="FOOTER_Inner">
	<div class="FOOTER_Inner_menu">
		<ul>
		<?php if(function_exists("footer_link") || (!empty($ft)))  {footer_link($BlockCount);} ?>
		</ul>
	</div>	

  <p>Copyright &copy; 2017 <a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a> All Rights Reserved.</p>
 </div>
</div>
</body>
</html>

