<?php
//read.php ver 3.0 for ver 2.0

//※※※※※※※※※※このファイルは絶対触らないでください※※※※※※※※※※
//---------【PRリンクの変数の変換】----------
$pr_link[1] = $index[0];
$pr_link[2] = $index[1];
$pr_link[3] = $index[2];
$pr_link[4] = $index[3];
$pr_link[5] = $index[4];
$pr_link[6] = $side[0][0];
$pr_link[7] = $side[0][1];
$pr_link[8] = $side[0][2];
$pr_link[9] = $side[0][3];
$pr_link[10] = $side[0][4];
$pr_link[11] = $side[1][0];
$pr_link[12] = $side[1][1];
$pr_link[13] = $side[1][2];
$pr_link[14] = $side[1][3];
$pr_link[15] = $side[1][4];
$pr_link[16] = $contents[0];
$pr_link[17] = $contents[1];
$pr_link[18] = $contents[2];
$pr_link[19] = $contents[3];
$pr_link[20] = $contents[4];
$pr_link[21] = $kousin[0][0];
$pr_link[22] = $kousin[0][1];
$pr_link[23] = $kousin[0][2];
$pr_link[24] = $kousin[0][3];
$pr_link[25] = $kousin[0][4];
$pr_link[26] = $kousin[1][0];
$pr_link[27] = $kousin[1][1];
$pr_link[28] = $kousin[1][2];
$pr_link[29] = $kousin[1][3];
$pr_link[30] = $kousin[1][4];


//---------【ディスクリプション・タイトル・コンテンツ・画像・altの振り分け】----------
//TOPオリジナルコンテンツ
//サブタイトル1段落目
  $top_original_content  = '';

if (!empty($pagesubtitle[1][1])) {
  $top_original_content .= $pagesubtitle[1][1]."\n";
}
//コンテンツ1段落目
if (!empty($content[1][1])) {
  $top_original_content .= $content[1][1]."\n";
}
//サブタイトル2段落目
if (!empty($pagesubtitle[1][2])) {
  $top_original_content .= $pagesubtitle[1][2]."\n";
}
//コンテンツ2段落目
if (!empty($content[1][2])) {
  $top_original_content .= $content[1][2]."\n";
}
//サブタイトル3段落目
if (!empty($pagesubtitle[1][3])) {
  $top_original_content .= $pagesubtitle[1][3]."\n";
}
//コンテンツ3段落目
if (!empty($content[1][3])) {
  $top_original_content .= $content[1][3]."\n";
}
//サブタイトル4段落目
if (!empty($pagesubtitle[1][4])) {
  $top_original_content .= $pagesubtitle[1][4]."\n";
}
//コンテンツ4段落目
if (!empty($content[1][4])) {
  $top_original_content .= $content[1][4]."\n";
}
//サブタイトル5段落目
if (!empty($pagesubtitle[1][5])) {
  $top_original_content .= $pagesubtitle[1][5]."\n";
}
//コンテンツ5段落目
if (!empty($content[1][5])) {
  $top_original_content .= $content[1][5]."\n";
}

//ディスクリプション
if (!empty($pn)) {
  $description = strip_tags($pagedigest[$pn]);
} else {
  $description = $introduction;
}

//タイトル・サブタイトル・コンテンツ・画像・alt
if (!empty($pn)) {
  //タイトル
  $page_title = $pagetitle[$pn][1];

  //サブタイトル
  $page_subtitle[1] = $pagesubtitle[$pn][1];
  $page_subtitle[2] = $pagesubtitle[$pn][2];
  $page_subtitle[3] = $pagesubtitle[$pn][3];
  $page_subtitle[4] = $pagesubtitle[$pn][4];
  $page_subtitle[5] = $pagesubtitle[$pn][5];

  //コンテンツ
  $page_content[1] = $content[$pn][1];
  $page_content[2] = $content[$pn][2];
  $page_content[3] = $content[$pn][3];
  $page_content[4] = $content[$pn][4];
  $page_content[5] = $content[$pn][5];

  //画像
  $page_image[1] = $pageimage[$pn][1];

  //alt
  $page_alt[1] = $pagealt[$pn][1];
}

//内部ページのコンテンツとサブタイトルを1つの変数に格納
//サブタイトル1段落目
  $sub_content = '';

if(!empty($page_image[1])) {
  $sub_content .= '<img src="'. home_url_image() .'images/'.$page_image[1].'" alt="'.$page_alt[1].'" />'."\n";
}
if (!empty($page_subtitle[1])) {
  $sub_content .= $page_subtitle[1]."\n";
}
//コンテンツ1段落目
if (!empty($page_content[1])) {
  $sub_content .= $page_content[1]."\n";
}
//サブタイトル2段落目
if (!empty($page_subtitle[2])) {
  $sub_content .= $page_subtitle[2]."\n";
}
//コンテンツ2段落目
if (!empty($page_content[2])) {
  $sub_content .= $page_content[2]."\n";
}
//サブタイトル3段落目
if (!empty($page_subtitle[3])) {
  $sub_content .= $page_subtitle[3]."\n";
}
//コンテンツ3段落目
if (!empty($page_content[3])) {
  $sub_content .= $page_content[3]."\n";
}
//サブタイトル4段落目
if (!empty($page_subtitle[4])) {
  $sub_content .= $page_subtitle[4]."\n";
}
//コンテンツ4段落目
if (!empty($page_content[4])) {
  $sub_content .= $page_content[4]."\n";
}
//サブタイトル5段落目
if (!empty($page_subtitle[5])) {
  $sub_content .= $page_subtitle[5]."\n";
}
//コンテンツ5段落目
if (!empty($page_content[5])) {
  $sub_content .= $page_content[5]."\n";
}

//コンテンツがない内部ページに、<meta name="robots" content="noindex,nofollow" />、タイトルに「404」、コンテンツに「ただいま準備中です。」が入る
  $noindex_nofollow = '';
if (empty($page_subtitle[1]) && empty($page_content[1]) && empty($page_subtitle[2]) && empty($page_content[2]) && empty($page_subtitle[3]) && empty($page_content[3]) && empty($page_subtitle[4]) && empty($page_content[4]) && empty($page_subtitle[5]) && empty($page_content[5])) {
  $page_title = '404';
  $sub_content = '<p>ただいま準備中です。</p>';
  if (!($filename == "index.php")) {
    $noindex_nofollow .= '<meta name="robots" content="noindex,nofollow" />'."\n";
  }
}

?>
