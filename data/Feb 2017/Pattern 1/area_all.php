<?php $BreadList = 0; ?>  <!--Pang Ku Su validation-->
<!doctype html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if (function_exists("get_header_tag")) {get_header_tag();} ?>
<?php if (function_exists("canonical_url")) {canonical_url();} ?>
<link href="<?php home_url(); ?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php home_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php home_url(); ?>js/mh.js"></script>
    <script type="text/javascript">
      $(function() {
        $('.height').matchHeight();
        $('.height').matchHeight();
      });
    </script>
    
</head>

<body>
<div class="member_head">
	 <div class="member_head_h1">
		<h1><a href="<?php home_url(); ?>"><?php echo $sitetitle; ?></a></h1>
	</div>
	<div class="member_head_nav">
		<ul>
			<?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
		</ul>
	</div>
   
    <div class="member_head_inside">
    	     
        <div class="member_head_banner">
            <?php if ($filename == "index.php") : ?>
                <img src="<?php home_url(); ?>images/TopImage.jpg" alt="" />
            <?php else :?>
                <img src="<?php home_url(); ?>images/SubImage.jpg" alt="" />
            <?php endif;?>
        </div>
     
    </div>
   	
</div>

<?php //TOPページ@page_0@****************************************************************************************************
if ($filename == "index.php") : ?>

    <div class="give_site">
		
    	<div class="main_plus"> 
        		<?php if (1 <= $BlockCount) : ?>
				<div class="style_face_fm_full">		
	            	<?php if(function_exists("d_block")) {
					    d_block(1, $BlockCount,
					    array(
							'DisplayImage' => 1,
					        'Variation' => 3,
					        'Skip'      => 1,
					        'BlockTag'  => 'div',
					        'BlockCss'  => 'grp_text1 height',
					        'TitleCss'  => 'title',
					        'ImageCss'  => 'img',
					        'DigestCss' => '',
					        'PrCss'     => '',
					        'MoreCss'   => 'view_more',
					        'MoreText'  => 'Read',
					        )
					    );
					} ?>
				</div>    	
			<?php endif; ?>   
    	 	     
            <?php if (1 <= $BlockCount) : ?>
				<div class="style_face_fm_2">
					<?php if(function_exists("d_block")) {
						d_block(1, $BlockCount,
						array(
							'DisplayImage' => 1,
							'Variation' => 3,
							'Skip'      => 1,
							'BlockTag'  => 'div',
							'BlockCss'  => 'grp_text2 height',
							'TitleCss'  => 'title',
							'ImageCss'  => 'img',
							'DigestCss' => '',
							'PrCss'     => '',
							'MoreCss'   => 'view_more',
							'MoreText'  => 'Read',
							)
						);
					} ?>
				</div>	
			<?php endif; ?> 
            	 <?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
                <div class="link_style">
	                <h2>Recommended Links</h2>
	               <ul>
        				<?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
       					<?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
        				<?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
        				<?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
        				<?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
				</ul>
                </div>
		    <?php } ?>                                            	 	
        	  <div class="content_post">
                <?php if (!empty($pagetitle[1][1])):?> <h2><?php echo $pagetitle[1][1]; ?></h2> <?php endif;?>
				<?php $DisplayTopImage = 1; //Display top image (number 0 and 1)
              if($DisplayTopImage == 1) {if (!empty($pageimage[1][1])) {echo '<img src="'. $read_home_url . 'images/' . $pageimage[1][1] . '" alt="' . $pagealt[1][1] . '" />';}} ?>
				<?php if (!empty($pr_link[6])) {echo '<p class="pr_link">'. $pr_link[6] . '</p>'; }?>
				<?php if (!empty($pr_link[7])) {echo '<p class="pr_link">'. $pr_link[7] . '</p>'; }?>
				<?php if (!empty($top_original_content)) {echo $top_original_content; }?>
				<?php if (!empty($pr_link[8])) {echo '<p class="pr_link">'. $pr_link[8] . '</p>'; }?>
				<?php if (!empty($pr_link[9])) {echo '<p class="pr_link">'. $pr_link[9] . '</p>'; }?>
				<?php if (!empty($pr_link[10])) {echo '<p class="pr_link">'. $pr_link[10] . '</p>'; }?>
			</div>
            
         	   
        </div> 
         <div class="div_bar">
        	<div class="div_bar_nav">
            	<h2>MENU</h2>
                <ul>
                    <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
                </ul>
            </div>
            <?php if (1 <= $BlockCount) : ?>
				<div class="style_face_fm_full">		
	            	<?php if(function_exists("d_block")) {
					    d_block(1, $BlockCount,
					    array(
							'DisplayImage' => 1,
					        'Variation' => 3,
					        'Skip'      => 1,
					        'BlockTag'  => 'div',
					        'BlockCss'  => 'grp_text_s height',
					        'TitleCss'  => 'title',
					        'ImageCss'  => 'img',
					        'DigestCss' => '',
					        'PrCss'     => '',
					        'MoreCss'   => 'view_more',
					        'MoreText'  => 'Read',
					        )
					    );
					} ?>
				</div>    	
			<?php endif; ?>
            <?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
                <div class="link_style">
	                <h2>Recommended Links</h2>
	               <ul>
        				<?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
       					<?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
        				<?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
        				<?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
        				<?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
				</ul>
                </div>
		    <?php } ?> 
        </div> 	
    </div>

<?php //内部ページ@end_page_0@@page_1@************************************************************************************************************************
elseif ($filename == $flname[2] || $filename == $flname[3] || $filename == $flname[4] || $filename == $flname[5] || $filename == $flname[6]) :?>
 <!--Start Code for Pang Ku Su-->
 <?php if($BreadList == 1) : ?>
    <ol class="in-menu_blocks_boss">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li><?php echo $page_title; ?></li>
     </ol>
 <?php endif; ?>
 <!--End Code Pang Ku Su-->

<div class="give_site">
	    <div class="sub_plus">
				<h2><?php if (!empty($page_title)) {echo $page_title;}?></h2>
                <?php $DisplayInnerImage = 1; //Display image (number 0 and 1)
              if($DisplayInnerImage == 1) { if (!empty($sub_content_image)) {echo $sub_content_image;}} ?>
				<?php if (!empty($sub_content)) {echo $sub_content;}?> 
	    </div>	
         <div class="div_bar">
        	<div class="div_bar_nav">
            	<h2>MENU</h2>
                <ul>
                    <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
                </ul>
            </div>
        </div>	  
</div>

<?php //サイトマップ@page_0@****************************************************************************************************
	elseif ($filename == "sitemap.php") : ?>
	 <?php if($BreadList == 1) : ?>
		<ol class="link_helper_pangusu">
	        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
	        <li>サイトマップ</li>
	    </ol>
 <?php endif; ?>
	<div class="layer_article_content">
		<div class="article_content_column">	
			<?php sitemap_article_list("HOME"); ?>
		</div>
	</div>

<?php //ページ分けおしまい@end_page_1@**************************************************************************************************
endif; ?>

<div class="footer">
			 <div class="footer_nav">
		<ul>
			<?php if(function_exists("footer_link") || (!empty($ft)))  {footer_link($BlockCount);} ?>
		</ul>
	</div>
			<?php if (file_exists("include/social.php")) {require_once $rootpass.'include/social.php';} ?>
			<p>Copyright&copy; 2017 <a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a> All Rights Reserved.</p>
	
</div>
</body>
</html>