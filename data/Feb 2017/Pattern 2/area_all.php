<?php $BreadList = 1; ?>  <!--Pang Ku Su validation-->
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php if (function_exists("get_header_tag")) {get_header_tag();} ?>
<?php if (function_exists("canonical_url")) {canonical_url();} ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php home_url(); ?>css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php home_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php home_url(); ?>js/mh.js"></script>
<script type="text/javascript">
  $(function() {
    $('.height').matchHeight();
    $('.height').matchHeight();
  });	

		jQuery(document).ready(function($){
        var url = window.location.href;
        $('.Main_TopMenu ul li a[href="'+url+'"]').addClass('active');
    });

</script>

</head>
<body>

<div class="master_header">

	<div class="area_h1">
		<h1><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></h1>
	</div>
	
	<div class="Main_TopMenu">
		<ul class="menu">
			<?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
		</ul>
	</div><!--Main_TopMenu-->
	
	<div class="slide_main_img">
		<div class="inner_slide_main_img">
			  <?php if ($filename == "index.php") : ?>
			  <img class="banner" src="<?php home_url(); ?>images/main_image.jpg" alt="">
			  <?php else: ?>
			  <img class="banner" src="<?php home_url(); ?>images/sub_image.jpg" alt="">
			  <?php endif; ?>
		</div><!--inner_slide_main_img-->
	</div>
		
</div><!--master_header -->

<?php //TOPページ@page_0@****************************************************************************************************
if ($filename == "index.php") : ?>
<div class="top_wrapper">

	<div class="postlist_block_main">
			
		<div class="section_block_frame">
		  <div class="contents_body blg_content">
			<?php if (!empty($pagetitle[1][1])):?> <h2><?php echo $pagetitle[1][1]; ?></h2> <?php endif;?>
			<?php $DisplayTopImage = 1; 
			if($DisplayTopImage == 1) {if (!empty($pageimage[1][1])) {echo '<img src="'. $read_home_url . 'images/' . $pageimage[1][1] . '" alt="' . $pagealt[1][1] . '" />';}} ?>
			<?php if (!empty($pr_link[6])) {echo '<p class="pr_link">'. $pr_link[6] . '</p>'; }?>
			<?php if (!empty($pr_link[7])) {echo '<p class="pr_link">'. $pr_link[7] . '</p>'; }?>
			<?php if (!empty($top_original_content)) {echo $top_original_content; }?>
			<?php if (!empty($pr_link[8])) {echo '<p class="pr_link">'. $pr_link[8] . '</p>'; }?>
			<?php if (!empty($pr_link[9])) {echo '<p class="pr_link">'. $pr_link[9] . '</p>'; }?>
			<?php if (!empty($pr_link[10])) {echo '<p class="pr_link">'. $pr_link[10] . '</p>'; }?>
		  </div><!-- blocks -->
		</div><!-- end section_block_frame -->
		
		<?php if (1 <= $BlockCount) : ?>
		<div class="section_block_frame">
		  <?php if(function_exists("d_block")) {
			d_block(1, $BlockCount,
			  array(
				'DisplayImage' => 1,
				'Variation' => 4,
				'Skip'      => 1,
				'BlockTag'  => 'div',
				'BlockCss'  => 'cont_blocks_01 blg_content height',
				'TitleCss'  => '',
				'ImageCss'  => 'action_image_01',
				'DigestCss' => '',
				'PrCss'     => '',
				'MoreCss'   => 'info_read_more',
				'MoreText'  => 'もっと見る',
			  )
			);
		  }?>
		</div><!--End section_block_frame-->
		<?php endif; ?>	
		
		
		<?php if (1 <= $BlockCount) : ?>
		<div class="section_block_frame">
		  <?php if(function_exists("d_block")) {
			d_block(1, $BlockCount,
			  array(
				'DisplayImage' => 1,
				'Variation' => 4,
				'Skip'      => 1,
				'BlockTag'  => 'div',
				'BlockCss'  => 'cont_blocks_02 blg_content height',
				'TitleCss'  => '',
				'ImageCss'  => 'action_image_02',
				'DigestCss' => '',
				'PrCss'     => '',
				'MoreCss'   => 'info_read_more',
				'MoreText'  => 'もっと見る',
			  )
			);
		  }?>
		</div><!--End section_block_frame-->
		<?php endif; ?>
		
		<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
		<div class="section_block_frame">
		  <div class="block_article_Prlink blg_content">
			<h2>おすすめサイト</h2>
			<ul>
			  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
			  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
			  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
			  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
			  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
			</ul>
		  </div>
		</div><!-- end section_block_frame -->
		<?php } ?>
		
	</div><!-- end postlist_block_main -->
	

	<div class="postlist_block_right">
		<div class="section_block_frame">
		  <div class="Main_TopMenu_main">
			<h2>サブメニュー</h2>
			<ul class="menu">
			  <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
			</ul>
		  </div><!-- end Main_TopMenu_main -->
		</div><!-- end section_block_frame -->
		
		
		<?php if (1 <= $BlockCount) : ?>
		<div class="section_block_frame">
		  <?php if(function_exists("d_block")) {
			d_block(1, $BlockCount,
			  array(
				'DisplayImage' => 1,
				'Variation' => 4,
				'Skip'      => 1,
				'BlockTag'  => 'div',
				'BlockCss'  => 'cont_blocks_03 blg_content',
				'TitleCss'  => '',
				'ImageCss'  => 'action_image_03',
				'DigestCss' => '',
				'PrCss'     => '',
				'MoreCss'   => 'info_read_more',
				'MoreText'  => 'もっと見る',
			  )
			);
		  }?>
		</div><!--End section_block_frame-->
		<?php endif; ?>

		<?php if (!empty($pr_link[1]) || !empty($pr_link[2]) || !empty($pr_link[3]) || !empty($pr_link[4]) || !empty($pr_link[5])) { ?>
		<div class="section_block_frame">
		  <div class="block_article_Prlink blg_content">
			<h2>おすすめサイト</h2>
			<ul>
			  <?php if (!empty($pr_link[1])) {echo '<li class="first">'. $pr_link[1] . '</li>'; }?>
			  <?php if (!empty($pr_link[2])) {echo '<li>'. $pr_link[2] . '</li>'; }?>
			  <?php if (!empty($pr_link[3])) {echo '<li>'. $pr_link[3] . '</li>'; }?>
			  <?php if (!empty($pr_link[4])) {echo '<li>'. $pr_link[4] . '</li>'; }?>
			  <?php if (!empty($pr_link[5])) {echo '<li>'. $pr_link[5] . '</li>'; }?>
			</ul>
		  </div>
		</div><!-- end section_block_frame -->
		<?php } ?>
		
	</div><!-- postlist_block_right -->

</div><!-- end top_wrapper -->
<?php //内部ページ@end_page_0@@page_1@************************************************************************************************************************
elseif ($filename == $flname[2] || $filename == $flname[3] || $filename == $flname[4] || $filename == $flname[5] || $filename == $flname[6]) :?>
 <!--Start Code for Pang Ku Su-->
 <?php if($BreadList == 1) : ?>
	<ol class="in-menu_blocks_boss">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li><?php echo $page_title; ?></li>
     </ol>
 <?php endif; ?>
 <!--End Code Pang Ku Su-->
<div class="top_wrapper">

	<div class="postlist_block_sub">
	  <div class="section_block_frame">
		<div class="contents_body blg_content">
		  <h2><?php if (!empty($page_title)) {echo $page_title;}?></h2>
			<?php $DisplayInnerImage = 1;
              if($DisplayInnerImage == 1) { if (!empty($sub_content_image)) {echo $sub_content_image;}} ?>
			<?php if (!empty($sub_content)) {echo $sub_content;}?>
		</div>
	  </div><!-- end section_block_frame -->
	</div><!-- postlist_block_sub -->
	
	<div class="postlist_block_right">	  
		<div class="section_block_frame">
		  <div class="Main_TopMenu_main">
			<h2>サブメニュー</h2>
			<ul class="menu">
			  <?php if(function_exists("global_nav")) {global_nav($BlockCount,'Home') ;} ?>
			</ul>                             
		  </div><!-- end Main_TopMenu_main -->
		</div><!-- end section_block_frame -->
	</div><!-- postlist_block_right -->
	
</div><!-- end of top_wrapper -->


<?php //サイトマップ@page_0@****************************************************************************************************
	elseif ($filename == "sitemap.php") : ?>

	<ol class="in-menu_blocks_boss">
        <li><a href="<?php home_url(); ?>">ホーム</a> ＞ </li>
        <li>サイトマップ</li>
     </ol>
	<div class="article_layer_three">
		<div class="article_content_column">	
			<?php sitemap_article_list("HOME"); ?>
		</div>
</div>
<?php //ページ分けおしまい@end_page_1@**************************************************************************************************
endif; ?>

<div class="main_info_footer"> 
	<div class="inner_main_info_footer">
		<div class="main_info_footer_menu">
			<ul>
			<?php if(function_exists("footer_link") || (!empty($ft)))  {footer_link($BlockCount);} ?>
			</ul>
		</div>	
		<div class="main_info_footer_right">
			<?php if (file_exists("include/social.php")) {require_once $rootpass.'include/social.php';} ?>
		</div>
		<div class="main_info_footer_left">	 
			<p>Copyright &copy; 2017 <a href="<?php home_url(); ?>"><?php if(!empty($sitetitle)) {echo $sitetitle;} ?></a> All Rights Reserved.</p>
		</div>
	</div>
</div>

</body>
</html>

