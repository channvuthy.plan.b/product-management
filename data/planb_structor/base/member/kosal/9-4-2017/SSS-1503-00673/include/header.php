﻿<?php
//header.php ver 3.5 for ver 2.2.0

    $filename = basename($_SERVER['SCRIPT_NAME']);
    if (file_exists("include/PR_link.php")) require_once 'PR_link.php';
    if (file_exists("include/item.php")) require_once 'item.php';
    if (file_exists("include/main.php")) require_once 'main.php';
    if (file_exists("include/read.php")) require_once 'read.php';

    //for Sittitle, pagetitle, description, function.
    function get_header_tag() {
        global $sitetitle, $pn, $page_title, $description, $noindex_nofollow;
        //Sittitle, pagetitle.
        if (!empty($pn)){
            echo "<title>";
        if (!empty($page_title)) {echo $page_title;}
        if (!empty($sitetitle) and !empty($page_title)){echo "|" ;}
        if (!empty($sitetitle)) {echo $sitetitle ;}
            echo "</title>" . PHP_EOL;
        }
        else {
        if (!empty($sitetitle)) {echo "<title>".$sitetitle ."</title>" . PHP_EOL ;}
        }
        //description
        if (!empty($description)) {echo "<meta name=\"description\" content=\"$description\" />" . PHP_EOL;}
        //noindex_nofollow
        if (!empty($noindex_nofollow)) {echo $noindex_nofollow . PHP_EOL ;}
    }


/**
//Don't tuch the individual code . //////////////////////////////////////////////////////////////
**/
    //$read_home_url Initialize
    $read_home_url = home_url_image();

    //canonical
    function canonical_url() {
        global $pn, $flname, $Canon;
        $tempty = debug_backtrace();
        $TempURL = "";
        if(isset($pn)) {
            $canonical = ( empty( $_SERVER["HTTPS"] ) ? "http://" : "https://" ) . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
            $parse_url = parse_url( $canonical );

            if (!empty($parse_url) && basename($_SERVER["REQUEST_URI"]) == $flname[$pn] && file_exists(__FILE__)){
                if($Canon == 0) echo "<link rel=\"canonical\" href=\"";
                $TempURL = $parse_url['host'].$parse_url['path'];
                $TempURL = str_replace("index.php", "", $TempURL);
                $TempURL = preg_replace('/\/{2,}/', '/', $TempURL);

                echo $parse_url['scheme'].'://'. $TempURL;
                if($Canon == 0) echo "\"/>" . PHP_EOL;
            }else{

                $emp = stristr($tempty[1]["file"], $_SERVER["HTTP_HOST"]);
                $emp = str_replace($_SERVER["HTTP_HOST"], "", $emp);

                if($emp == $_SERVER["REQUEST_URI"] && basename($_SERVER["REQUEST_URI"]) == $flname[$pn] ) {
                    if($Canon == 0) echo "<link rel=\"canonical\" href=\"";

                    $TempURL = $parse_url['host'].$parse_url['path'];
                    $TempURL = str_replace("index.php", "", $TempURL);
                    $TempURL = preg_replace('/\/{2,}/', '/', $TempURL);
                    
                    echo $parse_url['scheme'].'://'.$TempURL;
                    if($Canon == 0) echo "\"/>" . PHP_EOL;
                }else{
                    if($Canon == 0) echo "<link rel=\"canonical\" href=\"";
                    echo url_Legitimate();
                    if($Canon == 0) echo "\"/>" . PHP_EOL;
                }
            }
        }
    }

    //canonical URL inner page;
    function url_Legitimate() {
        $full = ( empty( $_SERVER["HTTPS"] ) ? "http://" : "https://" ) . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        $parse_full = parse_url( $full );

        $Legitimate_URL = $parse_full['scheme'].'://'.$TempURL;

        $TempURL = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $TempURL = str_replace("index.php", "", $TempURL);
        $TempURL = preg_replace('/\/{2,}/', '/', $TempURL);
        $home_url =  'http://' .$TempURL;
        if(strstr($home_url, '?')){
            $pos = strpos($home_url, '?');
            $home_url = substr($home_url,0,$pos);

        }
        if(strstr($home_url, ".php") != "") {
            $pos = strpos($home_url, '.php');
            $pos += 4;
            $home_url = substr($home_url, 0, $pos);
        }
        return $home_url;
    }

    //getFull path, function
    function home_url() {
        $full = ( empty( $_SERVER["HTTPS"] ) ? "http://" : "https://" ) . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        $parse_full = parse_url( $full );
        $Legitimate_URL = $parse_full['scheme'].'://'.$parse_full['host'].$parse_full['path'];

        if(preg_match("/[a-z0-9]*\.php$/", $Legitimate_URL)){
            $Deformation_URL = preg_replace("/[a-z0-9]*\.php$/", "", $Legitimate_URL);
            echo $Deformation_URL;
        }else{
            $home_url =  'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if(strstr($home_url, '?')){
                $pos = strpos($home_url, '?');
                $home_url = substr($home_url,0,$pos);
                $home_url = preg_replace("/[a-z0-9]*\.php$/", "", $home_url);
            };
            if(strstr($home_url, ".php") != "") {
                $pos = strpos($home_url, '.php');
                $pos += 4;
                $home_url = substr($home_url, 0, $pos);
                $home_url = preg_replace("/[a-z0-9]*\.php$/", "", $home_url);
            }
            echo $home_url;
        }
    }

    function home_url_image() {
        $full = ( empty( $_SERVER["HTTPS"] ) ? "http://" : "https://" ) . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        $parse_full = parse_url( $full );
        $Legitimate_URL = $parse_full['scheme'].'://'.$parse_full['host'].$parse_full['path'];

        if(preg_match("/[a-z0-9]*\.php$/", $Legitimate_URL)){
            $Deformation_URL = preg_replace("/[a-z0-9]*\.php$/", "", $Legitimate_URL);
            return $Deformation_URL;
        }else{
            $home_url =  'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            if(strstr($home_url, '?')){
                $pos = strpos($home_url, '?');
                $home_url = substr($home_url,0,$pos);
                $home_url = preg_replace("/[a-z0-9]*\.php$/", "", $home_url);
            };
            if(strstr($home_url, ".php") != "") {
                $pos = strpos($home_url, '.php');
                $pos += 4;
                $home_url = substr($home_url, 0, $pos);
                $home_url = preg_replace("/[a-z0-9]*\.php$/", "", $home_url);
            }
            return $home_url;
        }
    }

/**
//Don't tuch the individual code . //////////////////////////////////////////////////////////////
**/



    //Globalnavigation function.
    function global_nav($cnt, $global_text = ''){
        global $BlockCount, $flname, $pagetitle, $read_home_url;

            $navigate = '';

        if(!empty($global_text)){
            $navigate .= '<li class="first"><a href="' . $read_home_url . '" data-hover="'.$global_text.'">' . $global_text . '</a></li>' . PHP_EOL;
        }

        for($n = 1;$n < $BlockCount + 1;$n++){
            if($n > $cnt){
                break;
            }
            $navigate .= '<li><a href="' . $read_home_url . $flname[$n + 1] . '" data-hover="'.$pagetitle[$n + 1][1].'">' . $pagetitle[$n + 1][1] . '</a></li>' . PHP_EOL;
        }

        if(file_exists("./sitemap.php")) {
            $navigate .= '<li class="first"><a href="' . $read_home_url . 'sitemap.php" data-hover="サイトマップ">サイトマップ</a></li>' . PHP_EOL;
        }

        echo $navigate;

    }

    //sitemap list
    function sitemap_article_list($global_text){
        global $BlockCount, $flname, $pagetitle, $read_home_url;

        $posts = '<li class="first"><a href="' . $read_home_url . '" data-hover="'.$global_text.'">' . $global_text . '</a></li>' . PHP_EOL;

        for($y = 1; $y < $BlockCount+1 ; $y++){
                $posts .= '<li><a href="' . $read_home_url . $flname[$y+1] . '">' . $pagetitle[$y+1][1] . '</a></li>' . PHP_EOL;
        }
        echo $posts;
    }

    //Latest article list.
    function post_list($cnt, $rand = ''){
        global $BlockCount, $flname, $pagetitle, $read_home_url;

        $posts = '';

        if(!empty($rand)){
            $list = range(2,$cnt+1);
            shuffle($list);
            $y = 0;
            foreach($list as $random){
                if($y == 0){
                    $posts .= '<li class="first"><a href="' . $read_home_url . $flname[$random] . '">' . $pagetitle[$random][1] . '</a></li>' . PHP_EOL;
                }else{
                    $posts .= '<li><a href="' . $read_home_url . $flname[$random] . '">' . $pagetitle[$random][1] . '</a></li>' . PHP_EOL;
                }
            $y++;
            }

        }else{

            for($y = 0; $y < $cnt; $y++){
                if($y == 0){
                    $posts .= '<li class="first"><a href="' . $read_home_url . $flname[$BlockCount + 1 - $y] . '">' . $pagetitle[$BlockCount + 1 - $y][1] . '</a></li>' . PHP_EOL;
                }else{
                        $posts .= '<li><a href="' . $read_home_url . $flname[$BlockCount + 1 - $y] . '">' . $pagetitle[$BlockCount + 1 - $y][1] . '</a></li>' . PHP_EOL;
                }
            }
        }
        echo $posts;
    }

    //footer link function.
    function footer_link($cnt, $footer_text = ''){
        global $BlockCount, $flname, $pagetitle, $read_home_url;
        $ft = '';
        if(!empty($footer_text)){
            $ft .= '<li class="first"><a href="' . $read_home_url . '">' . $footer_text . '</a></li>' . PHP_EOL;
        }
        for($n = 1;$n < $BlockCount + 1;$n++){
            if($n > $cnt){
                break;
            }
            $ft .= '<li><a href="' . $read_home_url . $flname[$n+1] . '">' . $pagetitle[$n+1][1] . '</a></li>' . PHP_EOL;
        }

        if(file_exists("./sitemap.php")) {
           $ft .= '<li><a href="' . $read_home_url . 'sitemap.php">サイトマップ</a></li>' . PHP_EOL;
        }
        echo $ft;
    }

    function image_block($m, $cnt, $options = array()) {

        extract($GLOBALS);

        $defaults = array(
            'BlockTag'  => 'div',
            'BlockCss'  => 'article_text_column',
            'TitleCss'  => '',
            'ImageCss'  => 'image_of_block_02 height'
        );

        $options = array_merge($defaults, $options);
     
        for($i = $m + 1; $i <= $BlockCount + 1; $i++){
            if($i > $cnt + $m){ break; }

                if (!empty($options['BlockCss'])){
                    $bk .= '<' . $options['BlockTag'] . ' class="' . $options['BlockCss'] . '">' . PHP_EOL;
                } else {
                    $bk .= '<' . $options['BlockTag'] . ' id="block0' . ($i - 1) . '_container">' . PHP_EOL;
                }

                if (!empty($options['MoreCss'])) {
                    $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . PHP_EOL;
                } else {
                    $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . PHP_EOL;
                }

                if(!empty($options['TitleCss'])){
                    $bk .= '<h2 class="' . $options['TitleCss'] . '">' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                }else{
                    $bk .= '<h2>' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                }


                if(!empty($pageimage[$i][1])){
                    if (!empty($options['ImageCss'])) {
                        $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                        $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                    } else {
                        $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                    }
                }

                $bk .= "</a>" . PHP_EOL;
                $bk .= '</' . $options['BlockTag'] . '>' . PHP_EOL;
            }

        echo $bk;
    }

    //For block function.
    function d_block($m, $cnt, $options = array()){

        extract($GLOBALS);

        // $options = array_filter($options, "strlen");

        $defaults = array(
            'DisplayImage' => 1,
            'Variation'    => 0,
            'Skip'         => 1,
            'BlockTag'     => 'article',
            'BlockCss'     => '',
            'TitleCss'     => '',
            'ImageCss'     => '',
            'DigestCss'    => '',
            'PrCss'        => '',
            'MoreCss'      => '',
            // 'BlockCss'  => 'item',
            // 'TitleCss'  => '',
            // 'ImageCss'  => 'page_image',
            // 'DigestCss' => 'digest',
            // 'PrCss'     => 'pr_link',
            // 'MoreCss'   => 'read_more',
            'MoreText'  => 'ReadMore',
        );

        $options = array_merge($defaults, $options);

                $bk = '';

        for($i = $m + 1; $i <= $BlockCount + 1; $i = $i + $options['Skip']){
            if($i > $cnt + $m){ break; }

                if (!empty($options['BlockCss'])){
                    $bk .= '<' . $options['BlockTag'] . ' id="block0' . ($i - 1) . '_container" class="' . $options['BlockCss'] . '">' . PHP_EOL;
                } else {
                    $bk .= '<' . $options['BlockTag'] . ' id="block0' . ($i - 1) . '_container">' . PHP_EOL;
                }

            if ($options['Variation'] == 0){


                    if(!empty($options['TitleCss'])){
                        $bk .= '<h2 class="' . $options['TitleCss'] . '">' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }else{
                        $bk .= '<h2>' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if (!empty($options['DigestCss'])) {
                        $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                    } else {
                        $bk .= $pagedigest[$i];// . PHP_EOL;

                    }

                        if($i - 1 <= 10){
                            if (!empty($pr_link[$i - 1 + 10])) {
                                if (!empty($options['PrCss'])) {
                                    $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                } else {
                                    $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                }
                        }
                    }


                    if (!empty($options['MoreCss'])) {
                        $bk .= '<p class="' . $options['MoreCss'] . '"><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    } else {
                        $bk .= '<p><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    }




            } else if($options['Variation'] == 1){

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if(!empty($options['TitleCss'])){
                        $bk .= '<h2 class="' . $options['TitleCss'] . '">' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }else{
                        $bk .= '<h2>' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }

                    if (!empty($options['DigestCss'])) {
                        $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                    } else {
                        $bk .= $pagedigest[$i];// . PHP_EOL;

                    }

                        if($i - 1 <= 10){
                            if (!empty($pr_link[$i - 1 + 10])) {
                                if (!empty($options['PrCss'])) {
                                    $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                } else {
                                    $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                }
                        }
                    }


                    if (!empty($options['MoreCss'])) {
                        $bk .= '<p class="' . $options['MoreCss'] . '"><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    } else {
                        $bk .= '<p><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    }

            } else if($options['Variation'] == 2){

                    if($options['DisplayImage'] == 1) {                        
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if(!empty($options['TitleCss'])){
                        $bk .= '<h2 class="' . $options['TitleCss'] . '">' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }else{
                        $bk .= '<h2>' . $pagetitle[$i][1] . '</h2>' . PHP_EOL;
                    }

                    if (!empty($options['DigestCss'])) {
                        $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                    } else {
                        $bk .= $pagedigest[$i];// . PHP_EOL;

                    }

                    if (!empty($options['MoreCss'])) {
                        $bk .= '<p class="' . $options['MoreCss'] . '"><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    } else {
                        $bk .= '<p><a href="' . $read_home_url . $flname[$i] . '">' . $options['MoreText'] . '</a></p>' . PHP_EOL;
                    }

                        if($i - 1 <= 10){
                            if (!empty($pr_link[$i - 1 + 10])) {
                                if (!empty($options['PrCss'])) {
                                    $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                } else {
                                    $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                                }
                        }
                    }

            } else if($options['Variation'] == 3){

                        if(!empty($options['TitleCss'])){
                            $bk .= '<h2 class="' . $options['TitleCss'] . '">';
                        }else{
                            $bk .= '<h2>';
                        }

                        $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . $pagetitle[$i][1] . '</a></h2>' . PHP_EOL;

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if (!empty($options['DigestCss'])) {
                        $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                    } else {
                        $bk .= $pagedigest[$i];// . PHP_EOL;

                    }

                    if($i - 1 <= 10){
                        if (!empty($pr_link[$i - 1 + 10])) {
                            if (!empty($options['PrCss'])) {
                                $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            } else {
                                $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            }
                    }
                }
            } else if($options['Variation'] == 4){

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if(!empty($options['TitleCss'])){
                        $bk .= '<h2 class="' . $options['TitleCss'] . '">';
                    }else{
                        $bk .= '<h2>';
                    }

                    $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . $pagetitle[$i][1] . '</a></h2>' . PHP_EOL;

                        if (!empty($options['DigestCss'])) {
                            $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                        } else {
                            $bk .= $pagedigest[$i];// . PHP_EOL;

                        }

                    if($i - 1 <= 10){
                        if (!empty($pr_link[$i - 1 + 10])) {
                            if (!empty($options['PrCss'])) {
                                $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            } else {
                                $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            }
                    }
                }
            }else if($options['Variation'] == 5){

                        if(!empty($options['TitleCss'])){
                            $bk .= '<h2 class="' . $options['TitleCss'] . '">';
                        }else{
                            $bk .= '<h2>';
                        }

                        $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . $pagetitle[$i][1] . '</a></h2>' . PHP_EOL;

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            } else {
                                $bk .= '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . PHP_EOL;
                            }
                        }
                    }

                    if (!empty($options['TextCss'])) {
                            $bk .= '<div class="' . $options['TextCss'] . '">';
                        } else {
                            $bk .= '<div>';
                        }

                        if (!empty($options['DigestCss'])) {
                            $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                        } else {
                            $bk .= $pagedigest[$i];// . PHP_EOL;

                        }

                    if($i - 1 <= 10){
                        if (!empty($pr_link[$i - 1 + 10])) {
                            if (!empty($options['PrCss'])) {
                                $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            } else {
                                $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            }
                    }
                }
                    $bk .= '</div>';
            }else if($options['Variation'] == 6){

                        if(!empty($options['TitleCss'])){
                            $bk .= '<h2 class="' . $options['TitleCss'] . '">';
                        }else{
                            $bk .= '<h2>';
                        }

                        $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . $pagetitle[$i][1] . '</a></h2>' . PHP_EOL;

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . '</a>'. PHP_EOL;
                            } else {
                                $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . '</a>' . PHP_EOL;
                            }
                        }
                    }

                    if (!empty($options['TextCss'])) {
                            $bk .= '<div class="' . $options['TextCss'] . '">';
                        } else {
                            $bk .= '<div>';
                        }

                        if (!empty($options['DigestCss'])) {
                            $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                        } else {
                            $bk .= $pagedigest[$i];// . PHP_EOL;

                        }

                    if($i - 1 <= 10){
                        if (!empty($pr_link[$i - 1 + 10])) {
                            if (!empty($options['PrCss'])) {
                                $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            } else {
                                $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            }
                    }
                }
                    $bk .= '</div>';

              }else if($options['Variation'] == 7){

                    if($options['DisplayImage'] == 1) {
                        if(!empty($pageimage[$i][1])){
                            if (!empty($options['ImageCss'])) {
                                $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . '<img class="' . $options['ImageCss'] . '" src="' . $read_home_url .'images/' . $pageimage[$i][1] . '"';
                                $bk .= ' alt="' . $pagealt[$i][1] . '" />' . '</a>'. PHP_EOL;
                            } else {
                                $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . '<img src="' . $read_home_url .'images/' . $pageimage[$i][1] . '" alt="' . $pagealt[$i][1] . '" />' . '</a>' . PHP_EOL;
                            }
                        }
                    }

                    if (!empty($options['TextCss'])) {
                            $bk .= '<div class="' . $options['TextCss'] . '">';
                        } else {
                            $bk .= '<div>';
                        }

                        if(!empty($options['TitleCss'])){
                            $bk .= '<h2 class="' . $options['TitleCss'] . '">';
                        }else{
                            $bk .= '<h2>';
                        }

                        $bk .= '<a href="' . $read_home_url . $flname[$i] . '">' . $pagetitle[$i][1] . '</a></h2>' . PHP_EOL;

                        if (!empty($options['DigestCss'])) {
                            $bk .= str_replace('<p>', '<p class="' . $options['DigestCss'] . '">', $pagedigest[$i]);// . PHP_EOL;
                        } else {
                            $bk .= $pagedigest[$i];// . PHP_EOL;

                        }

                    if($i - 1 <= 10){
                        if (!empty($pr_link[$i - 1 + 10])) {
                            if (!empty($options['PrCss'])) {
                                $bk .= '<p class="' . $options['PrCss'] . '">'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            } else {
                                $bk .= '<p>'. $pr_link[$i - 1 + 10] . '</p>' . PHP_EOL;
                            }
                    }
                }
                    $bk .= '</div>';
            }

            $bk .= '</' . $options['BlockTag'] . '>' . PHP_EOL;

        }
        echo $bk;
    }

?>