<!-- social.php  for ver 1.6 -->
<div id="socialBtn">
    <div class='social'>
        <div id="boxArea" style="display:table;padding:0 0 0 2px;">

          <!-- google plus-->
            <div style="float:left;">
                <div class="g-plusone" data-size="medium" data-annotation="none"></div>
            </div>
            <!--end-->

            <!--bookmark-->
            <div style="width:77px;float:left;">
                <a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="standard-noballoon" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"> <img src="http://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a>
            </div>
            <!-- end -->

            <!-- pocket -->
            <div style="float:left;">
                <a data-pocket-label="pocket" data-pocket-count="none" class="pocket-btn" data-lang="en"></a>
            </div>
            <!-- end pocket-->

            <!-- linkedin -->
            <div style="float:left;">
                <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
                <script type="IN/Share"></script>
            </div>
            <!-- end linkedin-->

            <!-- line -->
            <div style="float:left;">
            <span>
            <script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script>
            <script type="text/javascript">
            new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a"});
            </script>
            </span>
            </div>
            <!-- end line-->

             <!-- t-post -->
            <a class="tumblr-share-button" data-color="white" data-notes="none" href="https://embed.tumblr.com/share"></a>
             <!-- end t-post -->
             
            <script>!function(d,s,id){var js,ajs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://secure.assets.tumblr.com/share-button.js";ajs.parentNode.insertBefore(js,ajs);}}(document, "script", "tumblr-js");</script>
        </div>
    </div>
</div>
<script type="text/javascript">
! function(d, i) {
    if (!d.getElementById(i)) {
        var j = d.createElement("script");
        j.id = i;
        j.src = "https://widgets.getpocket.com/v1/j/btn.js?v=1";
        var w = d.getElementById(i);
        d.body.appendChild(j);
    }
}(document, "pocket-btn-js");
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript" src="http://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
