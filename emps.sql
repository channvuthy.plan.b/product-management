-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2017 at 02:55 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emps`
--

-- --------------------------------------------------------

--
-- Table structure for table `bases`
--

CREATE TABLE `bases` (
  `id` int(10) UNSIGNED NOT NULL,
  `variation_id` int(10) UNSIGNED DEFAULT NULL,
  `pattern_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `user_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version_name` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version_id` int(200) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `column` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_by` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `used_by_id` int(11) NOT NULL,
  `used` int(10) UNSIGNED DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `your_url` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leader_base_url` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leader_check_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leader_check_result` int(10) UNSIGNED DEFAULT NULL,
  `leader_check_problem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_checker_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_checker_result` int(11) DEFAULT NULL,
  `first_checker_problem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_checker_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_checker_result` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_checker_problem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `get_it` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `day` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `month` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bases`
--

INSERT INTO `bases` (`id`, `variation_id`, `pattern_id`, `user_id`, `user_name`, `version_name`, `version_id`, `type_id`, `type_name`, `note`, `column`, `name`, `used_by`, `used_by_id`, `used`, `url`, `your_url`, `leader_base_url`, `leader_check_name`, `leader_check_result`, `leader_check_problem`, `first_checker_name`, `first_checker_result`, `first_checker_problem`, `second_checker_name`, `second_checker_result`, `second_checker_problem`, `get_it`, `created_at`, `updated_at`, `day`, `month`, `year`) VALUES
(13, 11, 11, 23, 'HOK KOSAL', 'VSS 2.3.0', 3, 25, 'VSSS', '', NULL, 'SSS-1503-00634', 'TRY SREYNETH', 9, 1, '  C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\leader\\bach\\9-4-2017   ', 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\member\\kosal\\9-4-2017 ', NULL, 'UY BACH', 2, ' jbu', 'Chhun Chamrong', NULL, 'vcdvdgvdfgvrfd', NULL, NULL, NULL, 1, '2017-09-05 01:40:23', '2017-09-06 22:43:33', '05', '09', '2017'),
(14, 11, 11, 23, 'HOK KOSAL', 'VSS 2.3.0', 3, 25, 'VSSS', '', NULL, 'SSS-1503-00637', 'TRY SREYNETH', 9, 1, '  C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\leader\\bach\\9-4-2017   ', 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\member\\kosal\\9-4-2017 ', NULL, 'UY BACH', 2, ' ko nib', 'Chhun Chamrong', NULL, 'cdsvcdv', NULL, NULL, NULL, 1, '2017-09-06 14:50:43', '2017-09-06 22:46:42', '06', '09', '2017'),
(15, 11, 11, 23, 'HOK KOSAL', 'VSS 2.3.0', 3, 25, 'VSSS', '', NULL, 'SSS-1503-00673', 'TRY SREYNETH', 9, 1, '  C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\leader\\bach\\9-4-2017   ', 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\member\\kosal\\9-4-2017 ', NULL, 'UY BACH', 1, 'Error COde', 'Chhun Chamrong', NULL, 'scsdcdscs', NULL, NULL, NULL, 1, '2017-09-06 14:50:43', '2017-09-07 01:15:46', '06', '09', '2017');

-- --------------------------------------------------------

--
-- Table structure for table `base_layout`
--

CREATE TABLE `base_layout` (
  `id` int(11) NOT NULL,
  `base_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `base_layout`
--

INSERT INTO `base_layout` (`id`, `base_id`, `layout_id`, `created_at`, `updated_at`) VALUES
(117, 16, 1, NULL, NULL),
(118, 16, 2, NULL, NULL),
(119, 16, 3, NULL, NULL),
(120, 16, 4, NULL, NULL),
(121, 16, 5, NULL, NULL),
(122, 16, 6, NULL, NULL),
(123, 16, 7, NULL, NULL),
(124, 16, 8, NULL, NULL),
(125, 16, 9, NULL, NULL),
(126, 16, 10, NULL, NULL),
(127, 16, 11, NULL, NULL),
(128, 16, 12, NULL, NULL),
(129, 16, 13, NULL, NULL),
(130, 16, 14, NULL, NULL),
(131, 16, 15, NULL, NULL),
(132, 16, 16, NULL, NULL),
(133, 16, 17, NULL, NULL),
(134, 16, 18, NULL, NULL),
(135, 16, 19, NULL, NULL),
(136, 16, 20, NULL, NULL),
(137, 16, 21, NULL, NULL),
(138, 9, 26, NULL, NULL),
(139, 9, 27, NULL, NULL),
(140, 9, 28, NULL, NULL),
(141, 9, 29, NULL, NULL),
(142, 9, 30, NULL, NULL),
(143, 9, 31, NULL, NULL),
(144, 9, 32, NULL, NULL),
(145, 9, 33, NULL, NULL),
(146, 9, 34, NULL, NULL),
(147, 9, 35, NULL, NULL),
(148, 9, 36, NULL, NULL),
(149, 9, 37, NULL, NULL),
(150, 9, 38, NULL, NULL),
(151, 9, 39, NULL, NULL),
(152, 9, 40, NULL, NULL),
(153, 9, 41, NULL, NULL),
(154, 9, 42, NULL, NULL),
(155, 9, 43, NULL, NULL),
(156, 9, 44, NULL, NULL),
(157, 9, 45, NULL, NULL),
(158, 8, 26, NULL, NULL),
(159, 8, 27, NULL, NULL),
(160, 8, 28, NULL, NULL),
(161, 8, 29, NULL, NULL),
(162, 8, 30, NULL, NULL),
(163, 8, 31, NULL, NULL),
(164, 8, 32, NULL, NULL),
(165, 8, 33, NULL, NULL),
(166, 8, 34, NULL, NULL),
(167, 8, 35, NULL, NULL),
(168, 8, 36, NULL, NULL),
(169, 8, 37, NULL, NULL),
(170, 8, 38, NULL, NULL),
(171, 8, 39, NULL, NULL),
(172, 8, 40, NULL, NULL),
(173, 8, 41, NULL, NULL),
(174, 8, 42, NULL, NULL),
(175, 8, 43, NULL, NULL),
(176, 8, 44, NULL, NULL),
(177, 8, 45, NULL, NULL),
(178, 7, 45, NULL, NULL),
(179, 7, 46, NULL, NULL),
(180, 7, 47, NULL, NULL),
(181, 7, 48, NULL, NULL),
(182, 7, 49, NULL, NULL),
(183, 7, 50, NULL, NULL),
(184, 7, 51, NULL, NULL),
(185, 7, 52, NULL, NULL),
(186, 7, 53, NULL, NULL),
(187, 7, 54, NULL, NULL),
(188, 7, 55, NULL, NULL),
(189, 7, 56, NULL, NULL),
(190, 7, 57, NULL, NULL),
(191, 7, 58, NULL, NULL),
(192, 7, 59, NULL, NULL),
(193, 7, 60, NULL, NULL),
(194, 7, 61, NULL, NULL),
(195, 7, 62, NULL, NULL),
(196, 7, 63, NULL, NULL),
(197, 7, 64, NULL, NULL),
(198, 7, 65, NULL, NULL),
(199, 7, 66, NULL, NULL),
(200, 7, 67, NULL, NULL),
(201, 7, 68, NULL, NULL),
(202, 7, 69, NULL, NULL),
(203, 12, 26, NULL, NULL),
(204, 12, 27, NULL, NULL),
(205, 12, 28, NULL, NULL),
(206, 12, 29, NULL, NULL),
(207, 12, 30, NULL, NULL),
(208, 12, 31, NULL, NULL),
(209, 12, 32, NULL, NULL),
(210, 12, 33, NULL, NULL),
(211, 12, 34, NULL, NULL),
(212, 12, 35, NULL, NULL),
(213, 12, 36, NULL, NULL),
(214, 12, 37, NULL, NULL),
(215, 12, 38, NULL, NULL),
(216, 12, 39, NULL, NULL),
(217, 12, 40, NULL, NULL),
(218, 12, 41, NULL, NULL),
(219, 12, 42, NULL, NULL),
(220, 12, 43, NULL, NULL),
(221, 12, 44, NULL, NULL),
(222, 12, 45, NULL, NULL),
(223, 11, 46, NULL, NULL),
(224, 11, 47, NULL, NULL),
(225, 11, 48, NULL, NULL),
(226, 11, 49, NULL, NULL),
(227, 11, 50, NULL, NULL),
(228, 11, 51, NULL, NULL),
(229, 11, 52, NULL, NULL),
(230, 11, 53, NULL, NULL),
(231, 11, 54, NULL, NULL),
(232, 11, 55, NULL, NULL),
(233, 11, 56, NULL, NULL),
(234, 11, 57, NULL, NULL),
(235, 11, 58, NULL, NULL),
(236, 11, 59, NULL, NULL),
(237, 11, 60, NULL, NULL),
(238, 11, 61, NULL, NULL),
(239, 11, 62, NULL, NULL),
(240, 11, 63, NULL, NULL),
(241, 11, 64, NULL, NULL),
(242, 11, 65, NULL, NULL),
(243, 10, 66, NULL, NULL),
(244, 10, 67, NULL, NULL),
(245, 10, 68, NULL, NULL),
(246, 10, 69, NULL, NULL),
(247, 10, 70, NULL, NULL),
(248, 10, 71, NULL, NULL),
(249, 10, 72, NULL, NULL),
(250, 10, 73, NULL, NULL),
(251, 10, 74, NULL, NULL),
(252, 10, 75, NULL, NULL),
(253, 10, 76, NULL, NULL),
(254, 10, 77, NULL, NULL),
(255, 10, 78, NULL, NULL),
(256, 10, 79, NULL, NULL),
(257, 10, 80, NULL, NULL),
(258, 10, 81, NULL, NULL),
(259, 10, 82, NULL, NULL),
(260, 10, 83, NULL, NULL),
(261, 10, 84, NULL, NULL),
(262, 10, 85, NULL, NULL),
(263, 10, 86, NULL, NULL),
(264, 13, 26, NULL, NULL),
(265, 13, 27, NULL, NULL),
(266, 13, 28, NULL, NULL),
(267, 13, 29, NULL, NULL),
(268, 13, 30, NULL, NULL),
(269, 13, 31, NULL, NULL),
(270, 13, 32, NULL, NULL),
(271, 13, 33, NULL, NULL),
(272, 13, 34, NULL, NULL),
(273, 13, 35, NULL, NULL),
(274, 13, 36, NULL, NULL),
(275, 13, 37, NULL, NULL),
(276, 13, 38, NULL, NULL),
(277, 13, 39, NULL, NULL),
(278, 13, 40, NULL, NULL),
(279, 13, 41, NULL, NULL),
(280, 13, 42, NULL, NULL),
(281, 13, 43, NULL, NULL),
(282, 13, 44, NULL, NULL),
(283, 13, 45, NULL, NULL),
(284, 15, 26, NULL, NULL),
(285, 15, 27, NULL, NULL),
(286, 15, 28, NULL, NULL),
(287, 15, 29, NULL, NULL),
(288, 15, 30, NULL, NULL),
(289, 15, 31, NULL, NULL),
(290, 15, 32, NULL, NULL),
(291, 15, 33, NULL, NULL),
(292, 15, 34, NULL, NULL),
(293, 15, 35, NULL, NULL),
(294, 15, 36, NULL, NULL),
(295, 15, 37, NULL, NULL),
(296, 15, 38, NULL, NULL),
(297, 15, 39, NULL, NULL),
(298, 15, 40, NULL, NULL),
(299, 15, 41, NULL, NULL),
(300, 15, 42, NULL, NULL),
(301, 15, 43, NULL, NULL),
(302, 15, 44, NULL, NULL),
(303, 15, 45, NULL, NULL),
(304, 15, 46, NULL, NULL),
(305, 14, 26, NULL, NULL),
(306, 14, 27, NULL, NULL),
(307, 14, 28, NULL, NULL),
(308, 14, 29, NULL, NULL),
(309, 14, 30, NULL, NULL),
(310, 14, 31, NULL, NULL),
(311, 14, 32, NULL, NULL),
(312, 14, 33, NULL, NULL),
(313, 14, 34, NULL, NULL),
(314, 14, 35, NULL, NULL),
(315, 14, 36, NULL, NULL),
(316, 14, 37, NULL, NULL),
(317, 14, 38, NULL, NULL),
(318, 14, 39, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exports`
--

CREATE TABLE `exports` (
  `id` int(11) NOT NULL,
  `Base_Name` varchar(100) NOT NULL,
  `Layout_Name` varchar(100) NOT NULL,
  `type` varchar(110) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `active`, `type`, `description`, `status`, `created_at`, `updated_at`) VALUES
(9, 'TRY SREYNETH', 1, 'first', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, '2017-09-03 23:37:03', '2017-09-04 19:09:42'),
(10, 'UY BACH', 1, 'base', 'Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown ...', 1, '2017-09-03 23:40:10', '2017-09-03 23:42:46'),
(11, 'TEAV PISETH', 1, 'qc', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages...', 1, '2017-09-03 23:42:23', '2017-09-03 23:42:38'),
(12, 'Vanna Chorn', 1, 'first', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', 1, '2017-09-05 23:50:12', '2017-09-05 23:50:12'),
(14, 'AAA', 1, 'first', 'aaaa', 1, '2017-09-06 22:14:26', '2017-09-06 22:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `layouts`
--

CREATE TABLE `layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `layouts`
--

INSERT INTO `layouts` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(26, 'S01-001', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(27, ' S01-002', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(28, ' S01-003', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(29, ' S01-004', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(30, ' S01-005', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(31, ' S01-006', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(32, ' S01-007', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(33, ' S01-008', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(34, ' S01-009', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(35, ' S01-010', '<p>Layout</p>', 1, '2017-09-04 01:24:23', '2017-09-04 01:24:23'),
(36, ' S01-011', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(37, ' S01-012', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(38, ' S01-013', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(39, ' S01-014', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(40, ' S01-015', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(41, ' S01-016', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(42, ' S01-017', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(43, ' S01-018', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(44, ' S01-019', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(45, ' S01-020', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(46, ' S01-021', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(47, ' S01-022', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(48, ' S01-023', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(49, ' S01-024', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(50, ' S01-025', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(51, ' S01-026', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(52, ' S01-027', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(53, ' S01-028', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(54, ' S01-029', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(55, ' S01-030', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(56, ' S01-031', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(57, ' S01-032', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(58, ' S01-033', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(59, ' S01-034', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(60, ' S01-035', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(61, ' S01-036', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(62, ' S01-037', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(63, ' S01-038', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(64, ' S01-039', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(65, ' S01-040', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(66, ' S01-041', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(67, ' S01-042', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(68, ' S01-043', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(69, ' S01-044', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(70, ' S01-045', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(71, ' S01-046', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(72, ' S01-047', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(73, ' S01-048', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(74, ' S01-049', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(75, ' S01-050', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(76, ' S01-051', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(77, ' S01-052', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(78, ' S01-053', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(79, ' S01-054', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(80, ' S01-055', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(81, ' S01-056', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(82, ' S01-057', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(83, ' S01-058', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(84, ' S01-059', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(85, ' S01-060', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(86, ' S01-061', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(87, ' S01-062', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(88, ' S01-063', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(89, ' S01-064', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(90, ' S01-065', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(91, ' S01-066', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(92, ' S01-067', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(93, ' S01-068', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(94, ' S01-069', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(95, ' S01-070', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(96, ' S01-071', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(97, ' S01-072', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(98, ' S01-073', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(99, ' S01-074', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(100, ' S01-075', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(101, ' S01-076', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(102, ' S01-077', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(103, ' S01-078', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(104, ' S01-079', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(105, ' S01-080', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(106, ' S01-081', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(107, ' S01-082', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(108, ' S01-083', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(109, ' S01-084', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24'),
(110, ' S01-085', '<p>Layout</p>', 1, '2017-09-04 01:24:24', '2017-09-04 01:24:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(222) NOT NULL,
  `category` varchar(100) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `top_page` varchar(50) NOT NULL,
  `sub_page` varchar(50) NOT NULL,
  `dateline` varchar(100) NOT NULL,
  `base_name` varchar(100) NOT NULL,
  `layout` varchar(100) NOT NULL,
  `version` varchar(200) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `member_id` int(12) NOT NULL,
  `member_name` varchar(100) NOT NULL,
  `leader_check_result` varchar(255) NOT NULL,
  `leader_check_description` varchar(255) NOT NULL,
  `qc_check_name` varchar(255) NOT NULL,
  `qc_check_result` varchar(255) NOT NULL,
  `qc_check_description` varchar(255) NOT NULL,
  `qc_second_check_name` varchar(255) NOT NULL,
  `qc_second_check_result` varchar(255) NOT NULL,
  `qc_second_check_description` varchar(22) NOT NULL,
  `date_upload` varchar(22) NOT NULL,
  `upload_status` varchar(22) NOT NULL,
  `group_name` varchar(222) NOT NULL,
  `old_url` varchar(255) NOT NULL,
  `group_type` varchar(50) NOT NULL,
  `date_ready` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  `isdelete` int(11) DEFAULT '1',
  `day` varchar(20) NOT NULL,
  `month` varchar(20) NOT NULL,
  `year` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `category`, `keyword`, `top_page`, `sub_page`, `dateline`, `base_name`, `layout`, `version`, `type`, `member_id`, `member_name`, `leader_check_result`, `leader_check_description`, `qc_check_name`, `qc_check_result`, `qc_check_description`, `qc_second_check_name`, `qc_second_check_result`, `qc_second_check_description`, `date_upload`, `upload_status`, `group_name`, `old_url`, `group_type`, `date_ready`, `status`, `isdelete`, `day`, `month`, `year`, `created_at`, `updated_at`) VALUES
(1, 'SSS-1503-00516', '', '', '', '', '2017-09-10', 'SSS-1503-00634', 'S01-001', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', 'TRY SREYNETH', '', '', '', 0, 1, '', '', '', '2017-09-07 08:36:37', '2017-09-06 22:52:31'),
(2, 'SSS-1503-00521', '', '', '', '', '2017-09-10', 'SSS-1503-00634', 'S01-012', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 08:35:35', '2017-09-06 22:52:31'),
(3, 'SSS-1503-00535', '', '', '', '', '2017-09-22', 'SSS-1503-00634', 'S01-013', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(4, 'SSS-1503-00540', '', '', '', '', '2017-09-23', 'SSS-1503-00634', 'S01-014', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(5, 'SSS-1503-00703', '', '', '', '', '2017-09-24', 'SSS-1503-00634', 'S01-015', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(6, 'SSS-1503-00600', '', '', '', '', '2017-09-25', 'SSS-1503-00634', 'S01-016', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(7, 'SSS-1503-00634', '', '', '', '', '2017-09-26', 'SSS-1503-00634', 'S01-017', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(8, 'SSS-1503-00637', '', '', '', '', '2017-09-27', 'SSS-1503-00634', 'S01-018', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(9, 'SSS-1503-00645\n', '', '', '', '', '2017-09-28', 'SSS-1503-00634', 'S01-019', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(10, 'SSS-1503-00672', '', '', '', '', '2017-09-29', 'SSS-1503-00634', 'S01-011', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-06 22:52:31', '2017-09-06 22:52:31'),
(11, 'SSS-1503-00673', '', '', '', '', '2017-09-30', 'SSS-1503-00634', 'S01-010', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(12, 'SSS-1503-00678', '', '', '', '', '2017-10-01', 'SSS-1503-00634', 'S01-002', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(13, 'SSS-1503-00679', '', '', '', '', '2017-10-02', 'SSS-1503-00634', 'S01-003', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(14, 'SSS-1503-00684', '', '', '', '', '2017-10-03', 'SSS-1503-00634', 'S01-004', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(15, 'SSS-1503-00687', '', '', '', '', '2017-10-04', 'SSS-1503-00634', 'S01-005', 'VSS 2.3.0', 'VSSS', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(16, 'SSS-1503-00692', '', '', '', '', '2017-10-05', 'SSS-1503-00634', 'S01-006', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(17, 'SSS-1503-00694', '', '', '', '', '2017-10-06', 'SSS-1503-00634', 'S01-007', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(18, 'SSS-1503-00698', '', '', '', '', '2017-10-07', 'SSS-1503-00634', 'S01-008', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35'),
(20, 'SSS-1503-00706', '', '', '', '', '2017-10-09', 'SSS-1503-00634', 'S01-020', 'VSS 2.3.0', 'VSSS', 27, 'Run Yen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '2017-09-07 05:53:35', '2017-09-06 22:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `paths`
--

CREATE TABLE `paths` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_for` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paths`
--

INSERT INTO `paths` (`id`, `user_id`, `path`, `site_url`, `path_for`, `group_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 20, '  C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\leader\\bach\\9-4-2017  ', '  http://localhost/2017\\9-3-2017\\data\\planb_structor\\base\\leader\\bach\\9-4-2017  ', 'base', NULL, '<p>Directory store any file</p>', '2017-09-04 19:24:24', '2017-09-04 21:18:48'),
(2, 23, 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\base\\member\\kosal\\9-4-2017', '    http://localhost/2017/9-3-2017/data/planb_structor/base/member/kosal/9-4-2017 ', NULL, NULL, '<p>Store all of my data</p>', '2017-09-04 19:25:38', '2017-09-04 20:58:32'),
(3, 19, '  C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\first\\leader  ', ' http://localhost/2017/9-3-2017/data/planb_structor/first/leader', 'first', 9, '<p>Directory store first template</p>', '2017-09-05 21:22:17', '2017-09-06 05:57:05'),
(4, 22, 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\first\\member\\sinu', '   http://localhost/2017/9-3-2017/data/planb_structor/first/member/sinu  ', 'first', NULL, '<p>My directory</p>', '2017-09-06 01:15:24', '2017-09-06 15:38:22'),
(5, 27, 'C:\\xampp\\htdocs\\2017\\9-3-2017\\data\\planb_structor\\first\\member\\yen', ' http://localhost/2017\\9-3-2017\\data\\planb_structor\\first\\member\\yen ', 'first', NULL, '<p>ss</p>', '2017-09-06 22:55:10', '2017-09-06 22:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `patterns`
--

CREATE TABLE `patterns` (
  `id` int(10) UNSIGNED NOT NULL,
  `variation_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patterns`
--

INSERT INTO `patterns` (`id`, `variation_id`, `name`, `url`, `file_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(10, 11, 'First Pattern', 'http://localhost/2017\\9-3-2017\\data\\Feb 2017\\Pattern 1', '1504508269Pattern 1.zip', '<ol>\r\n<li>Block variation 3-7 (No readmore)</li>\r\n<li>Block eyecatch (no image)</li>\r\n</ol>', 1, '2017-09-03 23:57:49', '2017-09-04 00:49:12'),
(11, 11, 'Second Pattern', 'http://localhost/adap/PlanB/Base/Pattern/pattern-template-2/', '1504508316Pattern 2.zip', '<ol>\r\n<li>Block variation 3-7 (No readmore)</li>\r\n<li>Sitemap</li>\r\n<li>Block no border style</li>\r\n</ol>', 1, '2017-09-03 23:58:36', '2017-09-04 00:49:47'),
(12, 11, 'Third Pattern', 'http://localhost/2017\\9-3-2017\\data\\Feb 2017\\Pattern 3', '1504508415Pattern 3.zip', '<ol>\r\n<li>Block variation 3-7 (No readmore)</li>\r\n<li>Eye catch image (not square or rectangle)</li>\r\n</ol>', 1, '2017-09-04 00:00:15', '2017-09-04 00:50:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` varchar(22) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permission`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin', 'an administrator', '2017-07-18 17:00:00', '2017-07-25 17:00:00'),
(2, 'Manager', 'manager', 'Manager can view overall report', '2017-07-15 17:04:10', '2017-07-15 17:04:10'),
(5, 'Leader', 'leader', 'Leader each there group', '2017-07-15 17:06:33', '2017-07-15 17:06:33'),
(6, 'Member', 'member', 'can see only there infomation', '2017-07-15 17:06:50', '2017-07-15 17:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-07-15 17:00:00', '2017-07-15 17:00:00'),
(2, 2, 2, NULL, NULL),
(3, 3, 5, NULL, NULL),
(6, 6, 6, NULL, NULL),
(7, 4, 6, NULL, NULL),
(8, 7, 5, NULL, NULL),
(9, 8, 6, NULL, NULL),
(16, 12, 6, NULL, NULL),
(18, 9, 6, NULL, NULL),
(19, 13, 6, NULL, NULL),
(22, 11, 5, NULL, NULL),
(23, 5, 6, NULL, NULL),
(24, 14, 1, NULL, NULL),
(25, 15, 1, NULL, NULL),
(26, 16, 5, NULL, NULL),
(28, 10, 5, NULL, NULL),
(29, 17, 5, NULL, NULL),
(30, 18, 5, NULL, NULL),
(32, 19, 5, NULL, NULL),
(33, 20, 5, NULL, NULL),
(34, 21, 5, NULL, NULL),
(35, 22, 6, NULL, NULL),
(36, 23, 6, NULL, NULL),
(39, 26, 5, NULL, NULL),
(40, 27, 6, NULL, NULL),
(41, 24, 1, NULL, NULL),
(42, 25, 2, NULL, NULL),
(43, 28, 5, NULL, NULL),
(44, 29, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('141288269db2d932bc764843f20c9e6d59880404', 10, '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoibmNmOEQwYTh2QjM2dGVETFkwRW9vWU1yTDZwc0V4WVpWRnlnZ0d2QSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NjQ6Imh0dHA6Ly9sb2NhbGhvc3QvYWRhcC9UZXNoaXMvcHVibGljL2xlYWRlci9sZWFkZXItZmlyc3QtZ2V0LWJhc2UiO31zOjU6ImZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjEwO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNTA0NDk5MjM4O3M6MToiYyI7aToxNTA0NDk5MTk0O3M6MToibCI7czoxOiIwIjt9fQ==', 1504499238);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `auto_backup` tinyint(1) NOT NULL DEFAULT '1',
  `alert` tinyint(1) NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(25, 'VSSS', 'Template from japan ', 1, '2017-09-04 00:59:36', '2017-09-04 00:59:36'),
(26, 'VSSS-CA', 'Template from cambodia', 1, '2017-09-04 00:59:56', '2017-09-04 00:59:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `lavel` int(11) DEFAULT '1',
  `group_id` int(11) DEFAULT NULL,
  `remember_token` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `lavel`, `group_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'chann vuthy', 'channvuthyit@gmail.com', '$2y$10$PcZcRxJXysXj79YqZkukM.PV7Hulu7Nh8pA7psi8xlnsdma/NdeTe', 1, 1, NULL, 'EOgkwq4qkzaOm3dnT2OgKTXIDtooju1vBjdg9rMVrjY6kjMHJyYscjADpPiH', NULL, '2017-09-07 00:54:17'),
(19, 'TRY SREYNETH', 'sreynethtry.planb@gmail.com', '$2y$10$8iL6KsgwbRcIcTi0sqjl8OQRsiV.X9dwcT.32NS8eK5vTaXW/mUYW', 1, 1, 9, 'c6nywI7AvPM0JRi9esVX8BzZtzvhyv7F7l8ZhhpL4aawqyGCh0HuN3qxEwp9', '2017-09-03 23:37:43', '2017-09-07 01:17:14'),
(20, 'UY BACH', 'uybach.planb@gmail.com', '$2y$10$9APfqj8uXCbzo35riTfb4ec4YmU5/sh72ClNLWQ9/ULxA4.UPZ5UW', 1, 1, 10, 'uetg3wKzTnIBBV2KVlqjVb8iyLaiEIvoLMD1XZJ00ZnFxM1iqXup5fENIyEj', '2017-09-03 23:41:26', '2017-09-07 01:17:42'),
(21, 'TEAV PISETH', 'borayong.planb@gmail.com', '$2y$10$rI4EwUzrlP5N5gLE2P6xReYlT.WViheV2a2tprUXgpuPla6m3tMR6', 1, 1, 11, 'qMKC1hdPfQ9WEaQX3K2VWPIUqONa2l15yZi0UCTBCXeOOUsxzAlqRqTA6OrF', '2017-09-03 23:44:09', '2017-09-06 15:15:47'),
(22, 'DEAP SINOU', 'deapsinou.planb@gmail.com', '$2y$10$o4ffPwYt2Ex4OSFvXdPBMulKWeoJuYjTotInQMJho/jub1/Cp47NW', 1, 1, 9, 'oQMzG5lktG0cQKN4BgRAqOfo3vI0FYn3Gt2nEj0OeaQxDRB41DrGrzuI3rax', '2017-09-03 23:44:55', '2017-09-06 22:12:48'),
(23, 'HOK KOSAL', 'hokkosal.planb@gmail.com', '$2y$10$Xs41Nkb68PQw/6frCZvQV.RFWb2N/jnYyGNul5LyHsi8Oy0zXjvSi', 1, 1, 10, 'RykfKgBzv3OXJqT6gLMeX9u0dVU5wbFhllTdhvslvUN3ZilHvizwrz1JxM7U', '2017-09-03 23:45:44', '2017-09-07 01:16:22'),
(24, 'administrator', 'admin.planb@gmail.com', '$2y$10$GR9suFFewd2O1pbF16NYOunu7vkYp/qgSiqnY4jP6ptV5slCbT.IS', 1, 1, 9, 'ldS03wMXgDljIJuuGXhyja7MVPhOrErx0pg0kjwtTUVeL2drx0OQkXsI1QHo', '2017-09-04 19:13:25', '2017-09-06 00:37:16'),
(25, 'Chhun Chamrong', 'chhun.planb@gmail.com', '$2y$10$/9e/ftkS1VsShf/NFEGI1uVKXUKReBPqBM0LP0LVyQayHSbcbsQeq', 1, 1, 11, 'HrWukeikb3Phx7qAgnFYDFmz9Cvg0vhr6NeJ0bFcITLLBzlPGvkxuC3zevbQ', '2017-09-05 00:55:36', '2017-09-06 00:37:33'),
(26, 'Vanna Chorn', 'chhonvanna.planb@gmail.com', '$2y$10$Jd23IDb2kZvy3/yLY0Ue2O95cNqRp7TnecGodDq4ZLB5gP.HZ6ZRC', 1, 2, 12, 'xuwDRZOANPRBIaTSuJe6UDjbkVSAc1TrI3UvexCz7JdndhbNXaCexzcoMtf9', '2017-09-05 23:50:47', '2017-09-05 23:51:56'),
(27, 'Run Yen', 'runyen.planb@gmail.com', '$2y$10$5aO3Q8fETcZwea5ifegPHe5QXjVOr8U47tRdqn06yIHg2jbMc9CQ2', 1, 1, 9, 'Xqp6URTS4sQgoFNvr0J31vykmWUhtTM7tRZL2R2QAr9JSGMEL34u8ITSqdS4', '2017-09-06 00:18:04', '2017-09-06 23:47:56'),
(28, 'User AAA', 'aa@gmail.com', '$2y$10$qVcffwNlXdg7kzB60he1eOQekU6FN4HD2jQlb3RtRHb3EqO35GnA2', 1, 1, 14, 'hfEv15VwwaVk90rrXTuZtaUJdGkv558v8jaEDk8Lo9Hoa2Uh2drTI2B4g5Ti', '2017-09-06 22:14:59', '2017-09-06 22:37:13'),
(29, 'chheko', 'sunchheko@gmail.com', '$2y$10$FFpl2ekhxrBgByuAdsLS9O8BQ6RefFADag/JEcTMlp9HzPlVqew5O', 1, 1, 9, 'Ft0HHPfrntmPXuv4nOYt3MBmuUe7a29fDSPfXiywCOUJh1b5TNeRjeKDH8CT', '2017-09-06 22:39:18', '2017-09-06 22:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_layouts`
--

CREATE TABLE `user_layouts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `layout_id` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_layouts`
--

INSERT INTO `user_layouts` (`id`, `user_id`, `layout_id`, `created_at`, `updated_at`) VALUES
(4, 23, '26', '2017-09-04 09:40:01', '2017-09-04 02:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_patterns`
--

CREATE TABLE `user_patterns` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `pattern_id` int(10) UNSIGNED NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_patterns`
--

INSERT INTO `user_patterns` (`id`, `user_id`, `pattern_id`, `note`, `created_at`, `updated_at`) VALUES
(12, 23, 11, NULL, '2017-09-04 02:32:51', '2017-09-04 02:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `variations`
--

CREATE TABLE `variations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variations`
--

INSERT INTO `variations` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(11, 'Feb/2017', '<ol>\r\n<li><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Block variation 3-7 (No readmore)&quot;}" data-sheets-userformat="{&quot;2&quot;:513,&quot;3&quot;:[null,0],&quot;12&quot;:0}">Block variation 3-7 (No readmore)</span></li>\r\n<li><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Block variation 3-7 (No readmore)&quot;}" data-sheets-userformat="{&quot;2&quot;:513,&quot;3&quot;:[null,0],&quot;12&quot;:0}">Pankuzu</span></li>\r\n<li><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Block variation 3-7 (No readmore)&quot;}" data-sheets-userformat="{&quot;2&quot;:513,&quot;3&quot;:[null,0],&quot;12&quot;:0}">Sitemap</span></li>\r\n<li><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Block variation 3-7 (No readmore)&quot;}" data-sheets-userformat="{&quot;2&quot;:513,&quot;3&quot;:[null,0],&quot;12&quot;:0}">Home icon</span></li>\r\n<li><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Block variation 3-7 (No readmore)&quot;}" data-sheets-userformat="{&quot;2&quot;:513,&quot;3&quot;:[null,0],&quot;12&quot;:0}">Block eyecatch (no image)</span></li>\r\n</ol>', 1, '2017-09-03 23:55:52', '2017-09-04 00:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `versions`
--

CREATE TABLE `versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) DEFAULT '1',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `versions`
--

INSERT INTO `versions` (`id`, `name`, `status`, `description`, `created_at`, `updated_at`) VALUES
(3, 'VSS 2.3.0', 1, 'The first version for plan-b company source code', '2017-09-04 00:58:21', '2017-09-04 00:58:21'),
(4, 'VSS 2.3.1', 1, 'Just upgrade  version from VSS 2.3.0', '2017-09-04 00:59:06', '2017-09-04 00:59:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bases`
--
ALTER TABLE `bases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `base_layout`
--
ALTER TABLE `base_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exports`
--
ALTER TABLE `exports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `layouts`
--
ALTER TABLE `layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_name` (`order_id`);

--
-- Indexes for table `paths`
--
ALTER TABLE `paths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patterns`
--
ALTER TABLE `patterns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_layouts`
--
ALTER TABLE `user_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_patterns`
--
ALTER TABLE `user_patterns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `variations`
--
ALTER TABLE `variations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `versions`
--
ALTER TABLE `versions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bases`
--
ALTER TABLE `bases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `base_layout`
--
ALTER TABLE `base_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=319;
--
-- AUTO_INCREMENT for table `exports`
--
ALTER TABLE `exports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `layouts`
--
ALTER TABLE `layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `paths`
--
ALTER TABLE `paths`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `patterns`
--
ALTER TABLE `patterns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `user_layouts`
--
ALTER TABLE `user_layouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_patterns`
--
ALTER TABLE `user_patterns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `variations`
--
ALTER TABLE `variations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `versions`
--
ALTER TABLE `versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
