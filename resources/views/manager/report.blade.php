<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>Report</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<style type="text/css">
body{
    font-family: tahoma;
}
   .half{
      float:left;
      width:48%;
   }
   .half:last-child{
         margin-left:4%;
   }
   p.title {
    background: #337ab7;
    padding: 10px;
    color: #fff;
    font-family: tahoma;
    font-size: 14px;
    text-align: center;
}
.table>thead>tr>th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
    padding: 8px;
}
thead.bg-default {
    background: #f5f5f5;
}
.table {
    width: 100%;
    margin-bottom: 20px;
}
.clear{
   clear:both;
   display:block;
   margin:20px 0px;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 5px;
    line-height: 1.42857143;
    vertical-align: top;
    border-bottom: 1px solid #ddd;
}
</style>
<body>
<div class="full" >
   <p class="title">REPORT FOR {{date('d/m/Y')}}</p>
</div>
   <div class="half">
      <p class="title">ALL MEMBER GROUP FIRST</p>
    <table class="table table-responsive">
      <thead class="bg-default">
         <tr>
            <th>N<sup style="text-decoration: underline;">o</sup></th>
            <th>Name</th>
            <th>Total</th>
            <th>Evaluate</th>
         </tr>
      </thead>
      <tbody>
         <?php $i=1;?>
         @if(!empty($orders))
         @foreach($orders as $order)
         <tr>
            <td>{{$i}}</td>
            <td>{{$order->member_name}}</td>
            <td>{{$order->Total}}</td>
            <td>{{$i}}</td>
         </tr>
         <?php $i++;?>
         @endforeach
         @endif
       </tbody>
   </table>
   </div>
    <div class="half">
      <p class="title">TOP 5 MEMBER</p>
      <table class="table table-responsive">
   <thead class="bg-default">
      <tr>
         <th>N<sup style="text-decoration: underline;">o</sup></th>
         <th>Name</th>
         <th>Total</th>
         <th>Evaluate</th>
      </tr>
   </thead>
   <tbody>
      <?php $i=1;?>
      @if(!empty($orders))
      @foreach($orders as $order)
      @if($i<=5)
      <tr>
         <td>{{$i}}</td>
         <td>{{$order->member_name}}</td>
         <td>{{$order->Total}}</td>
         <td>{{$i}}</td>
      </tr>
      <?php $i++;?>
      @endif
      @endforeach
      @endif
   </tbody>
</table>
<div class="clear"></div>
<p class="title">FIRST TEMPLATE</p>
<table class="table table-responsive">
   <thead class="bg-default">
      <tr>
         <th>N<sup style="text-decoration: underline;">o</sup></th>
         <th>Group First</th>
         <th>Total</th>
         <th>Evaluate</th>
      </tr>
   </thead>
   <tbody>
      <?php $i=1;?>
      @if(!empty($group_first))
      @foreach($group_first as $order)
      <tr>
         <td>{{$i}}</td>
         <td style="text-transform:capitalize;">{{$order->group_name}}</td>
         <td>{{$order->Total}}</td>
         <td>{{$i}}</td>
      </tr>
      <?php $i++;?>
      @endforeach
      @endif
   </tbody>
</table>
<div class="clear"></div>
<p class="title">BASE TEMPLATE</p>
<table class="table table-responsive">
   <thead class="bg-default">
      <tr>
         <th>N<sup style="text-decoration: underline;">o</sup></th>
         <th>Name</th>
         <th>Total</th>
         <th>Evaluate</th>
      </tr>
   </thead>
   <tbody>
      <?php $i=1;?>
      @if(!empty($bases))
      @foreach($bases as $base)
      <tr>
         <td>{{$i}}</td>
         <td style="text-transform:capitalize;">{{$base->UserName}}</td>
         <td>{{$base->Total}}</td>
         <td>{{$i}}</td>
      </tr>
      <?php $i++;?>
      @endforeach
      @endif
   </tbody>
</table>
   </div>
</body>
</html>