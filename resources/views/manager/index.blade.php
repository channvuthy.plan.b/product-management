@extends('layout.manager.master')
@section('title')
    Manager Dashboard
@stop
@section('content')
    @include('layout.manager.widget.header')
    @include('layout.manager.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('manager')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('manager')}}"><i class=" glyphicon glyphicon-print "></i> Report</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Report</h4>
                </div>
                <div class="panel-body autoPadding">
                        <label class="radio-inline">
                            <input type="radio" name="status" value="ready"  @if(!empty($status)) @if($status=="ready") checked @endif @else checked @endif><p style="margin-top:10px;">Ready</p>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" value="not" @if(!empty($status)) @if($status=="not") checked @endif @endif><p style="margin-top:10px;">Not Ready</p>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status"  value="all" @if(!empty($status)) @if($status=="all") checked @endif @endif><p style="margin-top:10px;">All</p>
                        </label>
                        <hr>
                        <div class="form-group" style="width:25%;float:left;padding:0px 10px;">
                            <label>Start Date</label>
                            <input type="date" class="form-control start" name="start">
                        </div>
                         <div class="form-group" style="width:25%;float:left;padding:0px 10px;">
                            <label>End Date</label>
                            <input type="date" class="form-control end" name="end">
                        </div>
                         <div class="form-group" style="width:25%;float:left;padding:0px 10px;">
                            <button class="btn btn-primary btn-block" style="height:auto;padding:6px 0px;    margin-top: 25px !important;"><i class="glyphicon glyphicon-search"></i> SERACH </button>
                        </div>
                         <div class="form-group" style="width:25%;float:left;padding:0px 10px;">
                            <button class="btn btn-primary btn-block btn_print" style="height:auto;padding:6px 0px;    margin-top: 25px !important;"><i class="glyphicon glyphicon-print"></i> PRINT REPORT</button>
                        </div>
                        <br/>
                        <br/>
                        <div class="clearfix"></div>
                          <hr>
                        <div class="col-md-6">
                            <h5  class="text-center">ALL MEMBER GROUP FIRST</h5>
                            <br/>
                            <table class="table table-responsive">
                                <thead class="bg-default">
                                    <tr>
                                        <th>N<sup style="text-decoration: underline;">o</sup></th>
                                        <th>Name</th>
                                        <th>Total</th>
                                        <th>Evaluate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @if(!empty($orders))
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$order->member_name}}</td>
                                                <td>{{$order->Total}}</td>
                                                <td>{{$i}}</td>
                                            </tr>
                                        <?php $i++;?>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">

                             <h5  class="text-center">TOP 5 MEMBER</h5>
                            <br/>
                            <table class="table table-responsive">
                                <thead class="bg-default">
                                    <tr>
                                        <th>N<sup style="text-decoration: underline;">o</sup></th>
                                        <th>Name</th>
                                        <th>Total</th>
                                        <th>Evaluate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @if(!empty($orders))
                                        @foreach($orders as $order)
                                            @if($i<=5)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$order->member_name}}</td>
                                                <td>{{$order->Total}}</td>
                                                <td>{{$i}}</td>
                                            </tr>
                                             <?php $i++;?>
                                             @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <h5  class="text-center">FIRST TEMPLATE</h5>
                            <br/>
                            <table class="table table-responsive">
                                <thead class="bg-default">
                                    <tr>
                                        <th>N<sup style="text-decoration: underline;">o</sup></th>
                                        <th>Group First</th>
                                        <th>Total</th>
                                        <th>Evaluate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @if(!empty($group_first))
                                        @foreach($group_first as $order)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td style="text-transform:capitalize;">{{$order->group_name}}</td>
                                                <td>{{$order->Total}}</td>
                                                <td>{{$i}}</td>
                                            </tr>
                                        <?php $i++;?>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>

                            <h5  class="text-center">BASE TEMPLATE</h5>
                            <br/>
                            <table class="table table-responsive">
                                <thead class="bg-default">
                                    <tr>
                                        <th>N<sup style="text-decoration: underline;">o</sup></th>
                                        <th>Name</th>
                                        <th>Total</th>
                                        <th>Evaluate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;?>
                                    @if(!empty($bases))
                                        @foreach($bases as $base)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td style="text-transform:capitalize;">{{$base->UserName}}</td>
                                                <td>{{$base->Total}}</td>
                                                <td>{{$i}}</td>
                                            </tr>
                                        <?php $i++;?>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                </div>
             </div>
        </form>
    </div>
    <script type="text/javascript">
        $(document).on("click",'.btn_print',function(e){
                e.preventDefault();
                var status= $("input[name='status']:checked").val();
                var start=$('.start').val();
                var end=$('.end').val();
                $.ajax({
                    url:"{{route('manager_print')}}",
                    type:"GET",
                    dataType:"json",
                    data:{status:status,start:start,end:end},
                    success:function(response){

                    }
                });
                window.location.href="{{route('manager_print')}}?status="+status+"&start="+start+"&end="+end;
        });
    </script>
@stop