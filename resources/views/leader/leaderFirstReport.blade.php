<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order Report</title>
</head>
<style>
    table {
        width: 100%;
    }
    td{
        background: #eee;
        padding: 10px;
    }
    th {
        background: #337ab7;
        padding: 10px;
        color: #fff;
    }
</style>
<body>
@if(!empty($orders))
    <table>
        <thead>
        <tr>
            <th>N<sup style="text-decoration:underline;">o</sup></th>
            <th>Name</th>
            <th>Total</th>
            <th>Evaluate</th>

        </tr>
        <tbody>
        <?php $i = 1;$total=0;?>
        @foreach($orders as $order)
            <tr>
                <td>{{$i}}</td>
                <td>{{$order->member_name}}</td>
                <td>{{$order->Total}}</td>
                <td>{{$i}}</td>
            </tr><?php $i++;$total=$total+$order->Total;?>
        @endforeach
        <tr>
            <td colspan="4" align="right">Total: {{$total}}</td>
        </tr>
        </tbody>
        </thead>
    </table>
@else
    <h3>No data to report</h3>
@endif

</body>
</html>