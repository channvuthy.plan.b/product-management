@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.headerFirst')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>
        <div class="block-bread">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Base Resource</li>
            </ol>
        </div>
        <div class="modify-db">
            <ul class="list-inline">
                <li><input type="radio" name="modify" id="">Add Field</li>
                <li><input type="radio" name="modify" id="">Delete Field</li>
            </ul>
        </div>
        <div class="box" style="max-width:100%;overflow-y: auto;">
            <table class="excel">
                <thead>
                    @if(!empty($columns))
                    <tr>
                        @for($i=0;$i<count($columns);$i++)
                                <th nowrap ori-name="{{$columns[$i]}}" contenteditable ="true" class="row-header">{{str_replace("_"," ",$columns[$i])}}</th>
                        @endfor
                    </tr>
                    @endif
                </thead>
                <tbody>
                    @if(!empty($resources))
                        @foreach($resources as $resource)
                            <tr>
                                @if(!empty($columns))
                                    @for($i=0;$i<count($columns);$i++)
                                        <td>{{$resource->$columns[$i]}}</td>
                                    @endfor
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on('mouseout',".row-header",function(e){
            e.preventDefault();
            var original_name=$(this).attr('ori-name');
            var new_name=$(this).text();
            if(original_name.length==new_name.length){
                return ;
            }
            if(new_name==""){
                return;
            }
            jQuery.ajax({
                url:"{{route('edit-column-resource')}}",
                data:{original_name:original_name,new_name:new_name},
                type:"GET",
                success:function(data){
                   Command: toastr["success"]("Columns has been updated!")
                   toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }
                   
                }
            });
        });
    </script>
@stop