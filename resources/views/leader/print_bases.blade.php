<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Report</title>
</head>
<style type="text/css">
    table {
    width: 100%;
    }
    thead {
        background: #337ab7;
        color:#fff;
    }
    thead th{
        padding:10px;
    }
    table td{
        padding:10px;
        background:#eee;
    }

</style>
<body>
    @if(!empty($bases))
        <h3  style="background-color:#337ab7;padding:8px;text-align: center;color:#fff;font-family: tahoma;">Base Report {{date('m/d/Y')}}</h3>
       <table cellspaceing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>N<sup>0</sup></th>
                    <th>Member Name</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $total=0;$i=1;?>
                @foreach($bases as $base)
                <?php $total=$total+$base->Total;?>
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$base->user_name}}</td>
                        <td>{{$base->Total}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">Total: {{$total}}</td>
                    </tr>
                    <?php $i++;?>
                @endforeach
            </tbody>
       </table>
    @endif
    
</body>
</html>