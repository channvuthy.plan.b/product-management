@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.header')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('uploadLayout')}}" style="width:150px;"><i class="fa fa-map"></i> Upload Layout</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('uploadLayout')}}" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Upload Layout</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Choose File </label>
                        </div>
                        <div class="col-md-10">
                            <input type="file" name="name" id="" class="form-control">
                            <span class="text-danger">{{$errors->first('name')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Preview File</h4>
            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th style="text-align:left;">File Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($folders as $folder)
                            <tr>
                                <td style="width:50px;text-align:center;"><a href="{{route('deleteLayoutPDF',['name'=>$folder])}}" class="btn btn-danger btn-xs" onclick='return confirm("Do you want to delete?")'><i class="glyphicon glyphicon-trash"></i> Delete</a></td>
                                <td ><a href="{{route('previewFile',['name'=>$folder,'type'=>'pdf'])}}" target="_blank">{{$folder}}</a></td>
                            </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop