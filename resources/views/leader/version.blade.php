@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.header')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('uploadVersion')}}" style="width:150px;"><i class="glyphicon glyphicon-book"></i> Version</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('uploadVersion')}}" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            <div class="panel panel-default @if($errors->first('name'))  @else hidden @endif hidden_form">
                <div class="panel-heading">
                    <h4>Version</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Version</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="name" id="" class="form-control" placeholder="Enter version name">
                            <span class="text-danger">{{$errors->first('name')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="description" class="form-control textarea" ></textarea>
                            <span class="text-danger">{{$errors->first('description')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        <a href="#" class="pull-right add_form btn btn-primary" style="display:block;text-decoration:none;"> @if($errors->first('name'))  <i class="glyphicon glyphicon-minus"></i> Hide Form @else <i class="glyphicon glyphicon-plus"></i> Add New Version @endif  </a>
        <div class="clearfix" style=""></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Version</h4>
            </div>
            <div class="panel-body" style="padding:10px;">
                @if(!empty($versions))
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>ID</th>
                            <th>Version</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($versions as $version)
                            <tr>
                                <td style="text-align: center;width:190px;">
                                    <a href="{{route('editVersion',['id'=>$version->id])}}" class="btn btn-xs btn-primary"><i class="glyphicon-edit glyphicon"></i> Edit</a>
                                    <a href="{{route('deleteVersion',['id'=>$version->id])}}" onclick="return confirm('Do you want to delete?')" class="btn btn-danger btn-xs"><i class="glyphicon-trash glyphicon"></i> Delete</a>
                                     <a href="{{route('activeVersion',['id'=>$version->id])}}" class="btn btn-xs @if($version->status=="1")  btn-info @else  btn-warning @endif">@if($version->status=="1")  <i class="glyphicon glyphicon-ok-circle"></i> Enabled @else <i class="glyphicon glyphicon-ban-circle"></i> Disabled  @endif</a>
                                </td>
                                <td width="50px;" align="center">{{$version->id}}</td>
                                <td width="150px;" align="center">{{$version->name}}</td>
                                <td>{!!str_replace("&nbsp;", "", $version->description)!!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$versions->render()}}
                @endif
            </div>
        </div>
    </div>
    <script type="text/javascript">
         (function($) {
           
            $(".add_form").on("click",function(e){
                e.preventDefault();
                var bool=$(".hidden_form").hasClass("hidden");
                if(bool==true){
                 $(".hidden_form").removeClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-minus"></i> Hide Form');
                 return;
                }
                 $(".hidden_form").addClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-plus"></i> Add New Version');
           
            });
            
        })(jQuery);
        </script>
@stop
