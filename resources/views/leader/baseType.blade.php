@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.header')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('createBaseType')}}"><i class="fa fa-id-card"></i> Variation</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('createVariation')}}" class="SystemForm" method="post">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
             @if(!count($variations))
                <div class="alert alert-success">No Result</div>
            @endif
            <div class="panel panel-default @if(!$errors->first('name')) hidden @endif hidden_form">
                <div class="panel-heading">
                    <h4>Variation</h4>
                </div>
                <div class="panel-body" style="padding:10px;">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Variation Name</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="name" id="" class="form-control" placeholder="Enter variation name" required>
                            <span class="text-danger">{{$errors->first('name')}}</span>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="variationDescription" id="" cols="30" rows="7"
                                      class="form-control textarea" ></textarea>
                            <span class="text-danger">{{$errors->first('variationDescription')}}</span>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height:35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
            <a href="#" class="pull-right add_form btn btn-primary" style="display:block;text-decoration:none;">@if($errors->first('name')) <i class="glyphicon glyphicon-minus"></i> Hide Form @else <i class="glyphicon glyphicon-plus"></i> Add New Variation @endif </a>

        <div class="clearfix" style=""></div>
        <div class="panel panel-default SystemForm">
            <div class="panel-heading">
                <form action="{{route('searchVariation')}}" method="get">
                    <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch">
                    <input type="text" name="search" id="search" placeholder="Search variation name">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Action</th>
                        <th>ID</th>
                        <th>Variation Name</th>
                        <th>Description</th>
            

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($variations as $variation)
                        <tr>
                            <td style="width:190px;text-align:center;    vertical-align: middle;">
                                <a href="{{route('editVariation',['id'=>$variation->id])}}" class="btn btn-primary btn-xs"><span class="glyphicon-edit glyphicon"></span> Edit</a>
                                <a href="{{route('deleteVariation',['id'=>$variation->id])}}" onclick="return confirm('Are you sure to delete this item?')"  class="btn btn-danger btn-xs"><i class="glyphicon-trash glyphicon"></i> Delete</a>
                                <a href="{{route('activeVriation',['id'=>$variation->id])}}" class="@if($variation->status=="1") btn btn-info btn-xs  @else btn btn-warning btn-xs @endif ">@if($variation->status=="1") <i class="glyphicon glyphicon-ok-circle"></i>  Enabled  @else <i class="	glyphicon glyphicon-ban-circle"></i> Disabled @endif</a></td>

                            <td >{{$variation->id}}</td>
                            <td style="width:112px;">{{$variation->name}}</td>
                            <td>{!!$variation->description!!}</td>
                
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$variations->render()}}
            
        </div>
        <script type="text/javascript">
         (function($) {
           $("#isearch").on("click",function(){
               $("form").submit();
           });
            $(".add_form").on("click",function(e){
                e.preventDefault();
                var bool=$(".hidden_form").hasClass("hidden");
                if(bool==true){
                 $(".hidden_form").removeClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-minus"></i> Hide Form');
                 return;
                }
                 $(".hidden_form").addClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-plus"></i> Add New Variation');
           
            });
            
        })(jQuery);
        </script>
    </div>

@stop