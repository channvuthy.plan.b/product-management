@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.header')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('createLayout')}}"><i class="glyphicon glyphicon-level-up"></i> Layout Name</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('createLayout')}}" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            <div class="panel panel-default @if($errors->first('name')) @else hidden @endif hidden_form">
                <div class="panel-heading">
                    <h4>Layout Name</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Layout Name</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="name" id="" class="form-control tokenfield"
                                   value="" data-role="tagsinput" placeholder="Enter layout name" >
                            <span class="text-danger">{{$errors->first('name')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="layoutDescription" id="" cols="30" rows="7"
                                      class="form-control textarea"></textarea>
                            <span class="text-danger">{{$errors->first('patternDescription')}}</span>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        <a href="#" class="pull-right add_form btn btn-primary" style="display:block;text-decoration:none;"> @if($errors->first('name')) <i class="glyphicon glyphicon-minus"></i> Hide Form @else <i class="glyphicon glyphicon-plus"></i> Add New Layout Name @endif </a>
        <div class="clearfix" style=""></div>
        <div class="panel panel-default SystemForm">
            <div class="panel-heading">
                <form action="{{route('search_layout')}}" method="get">
                    <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch" style="margin-left:19%;">
                    <input type="text" name="search" id="search" placeholder="Search layout name">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Action</th>
                        <th>ID</th>
                        <th>Layout Name</th>
                        <th>Created_at</th>
                        <th>Description</th>
                        
                        <th>Status</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($layouts as $layout)
                        <tr>
                            <td style="text-align:center;width:190px;">
                                <a href="{{route('editLayout',['id'=>$layout->id])}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                <a href="{{route('deleteLayout',['id'=>$layout->id])}}" onclick="return confirm('Do you want to delete ?')" class="btn btn-xs btn-danger" ><i class="glyphicon-trash glyphicon"></i> Delete</a>
                                <a href="{{route('activeLayout',['id'=>$layout->id])}}" class="btn btn-xs @if($layout->status=="1")  btn-info @else  btn-warning @endif">@if($layout->status=="1")  <i class="glyphicon glyphicon-ok-circle"></i> Enabled @else <i class="glyphicon glyphicon-ban-circle"></i> Disabled  @endif</a>
                            </td>

                            <td style="text-align:center;">{{$layout->id}}</td>
                            <td style="text-align:center;width:110px;">{{$layout->name}}</td>
                            <td style="text-align:center;width:120px;">{{date('d F Y', strtotime($layout->created_at))}}</td>
                              <td style=""><div style="max-width:300px;overflow: auto;">{!!$layout->description!!}</div></td>
                             <td style="text-align:center;">{{($layout->status)?"active":"inactive"}}</td>
                          
                            
                           
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$layouts->render()}}
            </div>
            <script type="text/javascript">
                    (function($) {
                        $("#isearch").click(function(){
                            $("form").submit();
                        });
                        $(".add_form").on("click",function(e){
                            e.preventDefault();
                            var bool=$(".hidden_form").hasClass("hidden");
                            if(bool==true){
                            $(".hidden_form").removeClass("hidden");
                            $(this).html('<i class="glyphicon glyphicon-minus"></i> Hide Form');
                            return;
                            }
                            $(".hidden_form").addClass("hidden");
                            $(this).html('<i class="glyphicon glyphicon-plus"></i> Add New Layout Name');
                    
                        });
                    })(jQuery);
            </script>
        </div>
    </div>
@stop