@extends('layout.leaders.master')

@section('title')
    Member
@stop
@section('content')
    @if(Auth::user()->group->type=="first")
        @include('layout.leaders.widget.headerFirst')
    @else
        @include('layout.leaders.widget.header')
    @endif
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10 normal">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><i class="glyphicon glyphicon-home"></i></a></li>
                <li><a href="#" style="width:170px;"><i class="glyphicon glyphicon-pencil"></i>
                        Order</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('leaderFirstGetOrder')}}" class="SystemForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            @if($errors->first('danger'))
                <div class="alert alert-danger">{{$errors->first('danger')}}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Order</h4>
                </div>
                <div class="panel-body" style="padding:15px;">
                    <div class="box_search">
                        <div class="box">
                            <select name="action" id="">
                                <option value="">Choose Action</option>
                                <option value="update">Update</option>
                                <option value="delete">Delete</option>
                            </select>
                            <div class="goTop">
                                <button type="submit" name="save" class="btn btn-primary btn-xs"><i
                                            class="glyphicon glyphicon-save"></i> Save
                                </button>
                            </div>
                        </div>
                        <div class="box">
                            <input type="text" placeholder="Enter location of your order " name="listFolder">
                            <div class="goTop">
                                <button type="submit" name="list" class="btn btn-primary btn-xs"><i
                                            class="glyphicon glyphicon-inbox"></i> List
                                </button>
                            </div>
                        </div>
                        <div class="box">
                            <input type="text" placeholder="Search order name" name="searchOrder">
                            <div class="goTop">
                                <button type="submit" name="search" class="btn btn-primary btn-xs"><i
                                            class="glyphicon glyphicon-search"></i> Search Order
                                </button>
                            </div>
                        </div>

                    </div>
                    @if(Auth::user()->level=="2")
                        <div class="clear-top-simple"></div>
                        <select name="" id="selectGroup" class="form-control">
                            <option value="">Show</option>
                            <option value="all">All</option>
                            <option value="your_group">Your Group</option>
                        </select>
                        <div class="clear-top-simple"></div>

                    @endif
                    <div class="clear-top-simple"></div>
                    <div class="col-md-6" style="padding-left: 0px;">
                        <div class="form-group">
                            <p>Start Date</p>
                            <input type="date" class="form-control" name="start">
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-right:0px;">
                        <div class="form-group">
                            <p>End Date</p>
                            <input type="date" class="form-control" name="end">
                        </div>
                    </div>
                    <hr style="display: block;clear: both;"/>

                    <div class="clear-top-simple" style="clear: both;"></div>
                    <div class="f" style="width:50%;float:left;">
                        <br>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="all"
                                   @if(!empty($status)) @if($status=="all") checked @endif @endif><span
                                    style="padding-top: 10px;display: block;">All Group</span>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="in"
                                   @if(!empty($status)) @if($status=="in") checked @endif @endif><span
                                    style="padding-top: 10px;display: block;">In Group</span>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="upload"
                                   @if(!empty($status)) @if($status=="upload") checked @endif @endif><span
                                    style="padding-top: 10px;display: block;">Uploaded</span>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="rest"
                                   @if(!empty($status)) @if($status=="rest") checked @endif) @endif><span
                                    style="padding-top: 10px;display: block;">Rest Deadline</span>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="not"
                                   @if(!empty($status)) @if($status=="not") checked @endif @endif><span
                                    style="padding-top: 10px;display: block;">Not Ready</span>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="filter_status" value="ready"
                                   @if(!empty($status)) @if($status=="ready") checked @endif @endif><span
                                    style="padding-top: 10px;display: block;">Ready</span>
                        </label>
                    </div>
                    <div style="width:30%;float:left;">
                        <div class="col-md-6">
                            <button type="submit" class="" style="height: 36px;"><i
                                        class="glyphicon glyphicon-sort-by-order-alt"></i> Filter
                            </button>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="" style="height: 36px;"
                                    name="print_report" value="print"><i class="glyphicon glyphicon-print"></i> Print
                            </button>

                        </div>
                    </div>

                    <div class="clear-top-simple" style="clear:both;"></div>

                    <hr>

                    <div class="clear-top-simple"></div>
                    <div class="order_list">
                        <table class="excel">
                            <thead>
                            <tr>
                                <th nowrap></th>
                                <th nowrap>ORDER ID</th>
                                <th nowrap>BASE NAME</th>
                                <th nowrap>LAYOUT ID</th>
                                <th nowrap>STAFF NAME</th>
                                <th nowrap>GROUP NAME</th>
                                <th nowrap>TYPE</th>
                                <th nowrap>VERSION</th>
                                <th nowrap>BLOCK</th>
                                <th nowrap>SUB</th>
                                <th nowrap>LEADER CHECK RESULT</th>
                                <th nowrap>LEADER CHECK PROBLEM</th>
                                <th nowrap>QC NAME</th>
                                <th nowrap>QC CHECK RESULT</th>
                                <th nowrap>QC CHECK PROBLEM</th>
                                <th nowrap>STATUS</th>
                                <th nowrap>DEADLINE</th>
                                <th nowrap>DATE READY</th>
                            </tr>
                            </thead>
                            @if(!empty($orders))
                                @foreach($orders as $order)
                                    <tbody>
                                    <tr>
                                        <td contenteditable="true" data="{{$order->id}}">
                                            <input type="checkbox" name="order_id[]" value="{{$order->id}}">
                                        </td>
                                        <td nowrap class="site_url" data="{{$order->id}}">
                                            <a href="{{(!empty(Auth::user()->path->site_url))?Auth::user()->path->site_url:''}}/{{$order->order_id}}"
                                               target="_blank">{{$order->order_id}}</a>
                                        </td>
                                        <td nowrap contenteditable="true" class="base_name" data="{{$order->id}}">
                                            {{$order->base_name}}
                                        </td>
                                        <td nowrap contenteditable="true" class="layout" data="{{$order->id}}">
                                            {{$order->layout}}
                                        </td>
                                        <td nowrap  class="user_name" data="{{$order->id}}">
                                            <?php $u = "";?>
                                            @if(!empty($order->user->name))
                                                <?php   $u = $order->user->name;?>
                                            @endif
                                            <select name="userID[]" id="" class="select user_select"
                                                    style="min-width:150px;">
                                                @if(!empty($users))
                                                    @foreach($users as $user)
                                                        <option value="{{$user->id}}" data-name="{{$user->name}}"
                                                                @if($u==$user->name) selected @endif>{{$user->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td nowrap  class="group" data="{{$order->id}}">
                                            <select class="select user_select  group_first" name="group[]">
                                                @if(!empty($groups))
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}"
                                                                @if($order->group_name==$group->name) selected @endif>{{$group->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td nowrap contenteditable="true" class="type" data="{{$order->id}}">
                                            {{$order->type}}
                                        </td>
                                        <td nowrap contenteditable="true" class="version" data="{{$order->id}}">
                                            {{$order->version}}
                                        </td>
                                        <td nowrap contenteditable="true" class="top_page" data="{{$order->id}}">
                                            {{$order->top_page}}
                                        </td>
                                        <td nowrap contenteditable="true" class="sub_page" data="{{$order->id}}">
                                            {{$order->sub_page}}
                                        </td>
                                        <td nowrap class="leader_check_result"  data="{{$order->id}}">
                                            <select name="" id="leader_check_result" data-id="{{$order->id}}"
                                                    class="leader_check_result select user_select"
                                                    @if($order->leader_check_result=="0") style="background-color:#c9302c;color:#fff;"
                                                    @elseif($order->leader_check_result=="1") style="background-color:#5cb85c;color:#fff;"
                                                    @elseif($order->leader_check_result=="2" )style="background-color:#f0ad4e;color:#fff;"
                                                    @else style="background-color:#269abc;color:#fff;" @endif>
                                                <option value="0" @if($order->leader_check_result=="0") selected @endif>
                                                    Recorect
                                                </option>
                                                <option value="1" @if($order->leader_check_result=="1") selected @endif>
                                                    Complete
                                                </option>
                                                <option value="2" @if($order->leader_check_result=="2") selected @endif>
                                                    Edited
                                                </option>
                                                <option value="3"
                                                        @if($order->leader_check_result=="3" || $order->leader_check_result=="") selected @endif>
                                                    Not yet check
                                                </option>
                                            </select>
                                        </td>
                                        <td contenteditable="true" class="leader_check_description" data="{{$order->id}}">
                                            {{$order->leader_check_description}}
                                        </td>
                                        <td nowrap contenteditable="true" data="{{$order->id}}">
                                            {{$order->qc_name}}
                                        </td>
                                        <td nowrap class="qc_check_result" data="{{$order->id}}" >
                                            <select name="qc_result[]" id="" class="select user_select" disabled

                                                    @if($order->qc_check_result=="")
                                                    style="color:#292b2c;"
                                                    @elseif($order->qc_check_result=="1")
                                                    style="color:#d9534f;"

                                                    @elseif($order->qc_check_result=="2")
                                                    style="color:#f0ad4e;"

                                                    @elseif($order->qc_check_result=="3")
                                                    style="color:#5cb85c;"

                                                    @elseif($order->qc_check_result=="4")
                                                    style="color:#0275d8;"
                                                    @endif
                                            >
                                                <option value="0"
                                                        @if($order->qc_check_result=="") selected="selected" @endif>QC
                                                    Check
                                                </option>
                                                <option value="1"
                                                        @if($order->qc_check_result=="1") selected="selected" @endif>
                                                    Recorrect
                                                </option>
                                                <option value="2"
                                                        @if($order->qc_check_result=="2") selected="selected" @endif>
                                                    Warning
                                                </option>
                                                <option value="3"
                                                        @if($order->qc_check_result=="3") selected="selected" @endif>
                                                    Complete
                                                </option>
                                                <option value="4"
                                                        @if($order->qc_check_result=="4") selected="selected" @endif>Ok
                                                </option>
                                            </select>
                                        </td>
                                        <td nowrap contenteditable="true" class="qc_description" data="{{$order->id}}">
                                            {{$order->qc_description}}
                                        </td>
                                        <td nowrap class="status" >
                                            <select name="" data-id="{{$order->id}}"
                                                    class="select user_select updateStatus"
                                                    @if($order->status==1) style="background-color: #5cb85c;color: #fff;"
                                                    @elseif($order->status==2) style="background-color: #3d5afe;color: #fff;"
                                                    @else style="background-color: #ac2925;color: #fff;" @endif>
                                                <option value="2" @if($order->status=="2")selected @endif>Ok</option>
                                                <option value="1" @if($order->status=="1") selected="selected" @endif>
                                                    Complete
                                                </option>
                                                <option value="0" @if($order->status=="0")selected @endif>Incomplete
                                                </option>

                                            </select>

                                        </td>
                                        <td nowrap contenteditable="true" class="dateline" data="{{$order->id}}">
                                            {{$order->dateline }}
                                        </td>
                                        <td nowrap contenteditable="true" class="date_ready" data="{{$order->id}}">
                                            {{$order->date_ready}}
                                        </td>

                                    </tr>
                                    </tbody>
                                @endforeach
                            @endif
                        </table>
                    </div>
                    @if(!empty($users))
                        <ul class="list-unstyled user_in_group">
                            @foreach($users as $user)
                                @if(Auth::user()->group->name==$user->group->name)
                                    <li value="{{$user->id}}" data-name="{{$user->name}}">{{$user->name}}</li>

                                @endif

                            @endforeach
                        </ul>
                    @endif
                    @if(!empty($groups))
                        <ul class="list-unstyled group_firsts">
                            @foreach($groups as $group)
                                <li value="{{$group->id}}" name="{{$group->name }}">{{$group->name }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        {{--Modal Option Filer--}}
        <div class="modal fade modal_option" id="modal-1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <form action="{{route('leaderFirstGetOrder')}}" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title">Filter</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <input type="text" name="start" id="start" class="form-control">
                                </div>
                                <div class="col-md-1">
                                    <b style="display: block;margin-top:9px;">To</b>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="to" id="to" class="form-control">
                                </div>

                            </div>
                            <br>
                            <hr>
                            <br>

                            <div class="row filter">
                                <div class="col-md-12">
                                    <label class="checkbox-inline">
                                        <input type="radio" value="stock" name="filter_component">Stock
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="upload" name="filter_component">Uploaded
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="rest_date" name="filter_component">Risk Dateline
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="not_complete" name="filter_component">Not Complete
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="ready" name="filter_component">Ready
                                    </label>

                                </div>

                                <br>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>
                                <button type="submit" class="btn btn-primary"><span class="fa fa-search"></span>
                                    Search
                                </button>
                            </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        {{--End Modal Option--}}
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".user_in_group").css({"display": "none"});
            $(".userby").mouseover(function (e) {
                var x = $(this).position().top;
                var y = $(this).position().left;
                $(".user_in_group").css({"margin-top": (x ) + "px"});
                $(".user_in_group").css({"margin-left": (y + 140) + "px"});

                $(".group_firsts").removeAttr("style").hide();
                $(".user_in_group").css({"display": "block"});

            });
            $(".userby").keyup(function (e) {
                var x = $(this).position().top;
                var y = $(this).position().left;
                $(".user_in_group").css({"margin-top": (x + 200) + "px"});
                $(".user_in_group").css({"margin-left": (y + 125) + "px"});
                $(".user_in_group").css({"display": "block"});

            });
            $(".user_in_group li").click(function () {
                var id = $(this).attr('value');
                var name = $(this).text();
                $(".useByActive input[type='text']").val(name);
                $(".useByActive input[type='hidden']").val(id);
                $(".user_in_group").css({"display": "none"});
                $(".useByActive").removeClass("useByActive");

            });
            $("body").click(function () {
                $(".user_in_group").css({"display": "none"});
                $(".group_firsts").removeAttr("style").hide();
            })
        });
        $("table td .userby").on('mouseover', function () {
            var isActive = $(this).parent().hasClass('useByActive');
            if (isActive == false) {
                $(this).parent().addClass("useByActive")
                isActive = true;
            } else if (isActive == true) {
                $(this).parent().removeClass("useByActive")
                isActive = false;
            }

        });
        $(".user_select").on('change', function () {
            var id = $(this).val();
            var text = $(".user_select option:selected").text();
            $(".useByActive .userby").val(text);
            $(".useByActive .userHidden").val(id);

        });

        $(".groupName").mouseover(function (event) {
            /* Act on the event */
            $(this).parent().addClass('isActive');
            var x = $(this).position().top;
            var y = $(this).position().left;
            $(".group_firsts").css({"margin-top": (x) + "px"});
            $(".group_firsts").css({"margin-left": (y + 132) + "px"});
            $(".group_firsts").css({"display": "block"});
            $(".user_in_group").css({"display": "none"});

        });


        $(".group_first").change(function (event) {
            /* Act on the event */
            var value = $(".group_first option:selected").text();
            $(".isActive input").val(value);
        });
        $(".group_firsts li").on('click', function () {
            var textThis = $(this).text();
            $(".isActive input").val(textThis);
            $(".isActive").removeClass('isActive');
        });

        $('.leader_description').on('keyup keypress blur change', function (e) {
            // e.type is the type of event fired
            var id = $(this).attr('data-order');
            var value = $(this).val();
            jQuery.ajax({
                url: "{{route('update.order')}}",
                type: "GET",
                dataType: "json",
                data: {id: id, value: value},
                success: function (data) {
                    console.log(data);
                },
                error: function () {

                }
            });

        });
        $('.leader_check_result').on('change', function (e) {
            // e.type is the type of event fired
            var CheckResult = $(this).val();
            if (CheckResult == "0") {
                $(this).css({"background-color": "#c9302c"});
            } else if (CheckResult == "1") {
                $(this).css({"background-color": "#5cb85c"});
            } else if (CheckResult == "2") {
                $(this).css({"background-color": "#f0ad4e"});
            } else {
                $(this).css({"background-color": "#269abc"});
            }
            var id = $(this).attr('data-id');
            jQuery.ajax({
                url: "{{route('update.order')}}",
                type: "GET",
                dataType: "json",
                data: {CheckResult: CheckResult, id: id},
                complete: function () {
                    message();
                    notification();
                    qc();
                }
            });

        });
        $(function () {
            $("#start").datepicker({dateFormat: 'yy-dd-mm'}).val();
            $("#to").datepicker({dateFormat: 'yy-dd-mm'}).val();
        });
        $(".button_option").click(function () {
            $(".modal_option").modal('show');
        });
        $("#selectGroup").on("change", function () {
            var s = window.location.href;
            var g = $(this).val();
            var n = s.indexOf('?');
            s = s.substring(0, n != -1 ? n : s.length);
            window.location.href = s + "?filter_group=" + g;
        });

        $("body").on("change", ".updateStatus", function () {
            var vl = $(this).val();
            if (vl == "1") {
                $(this).css({"background-color": "#5cb85c"});
            } else if (vl == "0") {
                $(this).css({"background-color": "#ac2925"});
            } else {
                $(this).css({"background-color": "#3D5AFE"});

            }
            var id = $(this).attr('data-id');
            $.ajax({
                url: "{{route('updateStatusOrder')}}",
                data: {
                    id: id,
                    vl: vl,
                },
                dataType: "json",
                success: function (response) {
                    console.log(response)
                },
                error: function (err) {

                }
            });
        });


        $(document).on("keyup","td",function(){
            var class_name=$(this).attr('class');
            var text=$(this).text();
            var id=$(this).attr('data');
            jQuery.ajax({
                url:"{{route('edit_order_content')}}",
                type:"GET",
                data:{name:class_name,value:text,id:id},
                success:function(data){
                    console.log(data)
                }
            });
        });
    </script>
@stop

