@extends('layout.leaders.master')
@section('title')
    Leader
@stop
@section('content')
    @include('layout.leaders.widget.header')
    @include('layout.leaders.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('leader.index')}}"><span class="glyphicon-home glyphicon"></span></a></li>
                <li><a href="{{route('createPattern')}}"><i class="fa fa-tags"></i> Pattern</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('createPattern')}}" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            @if(!count($patterns))
                <div class="alert alert-success">No Result</div>
            @endif
            
            <div class="panel panel-default  hidden_form @if($errors->first('name')||$errors->first('patternURL')) @else hidden @endif">
                <div class="panel-heading">
                    <h4>Pattern</h4>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Choose Variation</label>
                        </div>
                        <div class="col-md-10">
                            <select name="variation" id="" class="form-control" required>
                                @foreach($variations as $variation)
                                    <option value="{{$variation->id}}">{{$variation->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Pattern Name</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="name" id="" class="form-control"
                                   value="{{old('name')}}" placeholder="Enter pattern name">
                            <span class="text-danger">{{$errors->first('name')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Pattern URL</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="patternURL" id="" class="form-control"
                                   value="{{old('patternURL')}}" placeholder="Enter URL(Ex:http://localhost/2017\9-3-2017\data\Feb 2017\Pattern)">
                            <span class="text-danger">{{$errors->first('patternURL')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Pattern File</label>
                        </div>
                        <div class="col-md-10">
                            <input type="file" name="patternFile" id="" class="form-control">
                            <span class="text-danger">{{$errors->first('patternFile')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="patternDescription" id="" cols="30" rows="7"
                                      class="form-control textarea"></textarea>
                            <span class="text-danger">{{$errors->first('patternDescription')}}</span>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        <a href="#" class="pull-right add_form btn btn-primary" style="display:block;text-decoration:none;"> @if($errors->first('name')) <i class="glyphicon glyphicon-minus"></i> Hide Form @else <i class="glyphicon glyphicon-plus"></i> Add New Pattern @endif </a>
        <div class="clearfix" style=""></div>
        <div class="panel panel-default SystemForm">
        <div class="panel-heading">
                <form action="{{route('searchPattern')}}" method="get">
                    <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch" style="margin-left:19%;">
                    <input type="text" name="search" id="search" placeholder="Search Pattern Name">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Action </th>
                        <th>ID</th>
                        <th>Variation </th>
                        <th>Pattern</th>
                        <th style="overflow-y: auto;width: 100px">Pattern URL</th>
                        <th>File</th>
                        <th>Description</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($patterns))
                        @foreach($patterns as $pattern)
                            <tr>
                                <td class="text-center;" style="width:190px;text-align:center;">
                                    <a href="{{route('editPattern',['id'=>$pattern->id])}}" class="btn btn-xs btn-primary"><i class="glyphicon-edit glyphicon"></i> Edit </a>
                                    <a href="{{route('deletePattern',['id'=>$pattern->id])}}" onclick="return confirm('Do you want to delete?')" class="btn btn-xs btn-danger"><i class="glyphicon-trash glyphicon"></i> Delete </a>
                                    <a href="{{route('activePattern',['id'=>$pattern->id])}}" class="@if($pattern->status=="1")  btn btn-xs btn-info @else btn btn-xs btn-warning  @endif ">@if($pattern->status=="1") <i class=" glyphicon glyphicon-ok-circle"></i> Enabled  @else <i class="    glyphicon glyphicon-ban-circle"></i>  Disabled @endif</a>
                                </td>
                                <td>{{$pattern->id}}</td>
                                <td><div> {{(!empty($pattern->variation->name))?$pattern->variation->name:''}}</div></td>
                                <td><div> {{$pattern->name}}</div></td>
                                <td><div> <a href="{{$pattern->url}}" target="_blank">{{$pattern->name}}</a></div></td>
                                <td><a href="{{asset('uploads')}}/{{$pattern->file_name}}">Download</a></td>
                                <td><div>{!! $pattern->description !!}</div></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                {{$patterns->render()}}
            </div>
        </div>
    </div>
{{$patterns->render()}}
<script type="text/javascript">
     (function($) {
            $( "#search" ).autocomplete({
                source: "{{route('getTable')}}",
                minLength: 1,
                select: function(event, ui) {
                    $('#search').val(ui.item.value);
                }
            });
            $("#isearch").on("click",function(){
                $("form").submit();
            })
            $(".add_form").on("click",function(e){
                e.preventDefault();
                var bool=$(".hidden_form").hasClass("hidden");
                if(bool==true){
                 $(".hidden_form").removeClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-minus"></i> Hide Form');
                 return;
                }
                 $(".hidden_form").addClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-plus"></i> Add New Pattern');
           
            });
            
        })(jQuery);
</script>
@stop