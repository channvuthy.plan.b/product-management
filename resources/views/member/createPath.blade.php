@extends('layout.member.master')
@section('title')
    Member
@stop
@section('content')
    @include('layout.member.widget.header')
    @include('layout.member.widget.navbar')
    <div class="col-md-10">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('member.index')}}"><i class="glyphicon-home glyphicon"></i></a></li>
                <li><a href="{{route('createPath')}}" style="width:170px;"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Your &nbsp;Directory</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>

        <form action="{{route('createPath')}}" class="SystemForm" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{Session::token()}}">
            @if($errors->first('notice'))
                <div class="alert alert-success">{{$errors->first('notice')}}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Directory</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Directory Name</label>
                        </div>
                        <div class="col-md-10" style="position: relative;">
                            {{-- <input type="text" name="pathName" id="" class="form-control"value="@if(!empty($path)) {{$path->path}} @endif"> --}}
                            
                            {{-- <span class="text-danger">{{$errors->first('pathName')}}</span> --}}

                            <div class="form-group  has-feedback">
                              <div class="input-group">
                                <input type="text" class="form-control" name="pathName" value="@if(!empty($path)) {{trim($path->path)}} @endif"  id="inputGroupSuccess1" aria-describedby="inputGroupSuccess1Status">
                                <span class="input-group-addon browse" ><i class="glyphicon glyphicon-paperclip"></i></span>
                                <input type="file" name="" id="browse" class="hidden">
                              </div>
                              
                              <span class="text-danger">{{$errors->first('pathName')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Path Site URL</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="site_url" id="" class="form-control"
                                   value="@if(!empty($path)) {{trim($path->site_url)}} @endif">
                            <span class="text-danger">{{$errors->first('site_utl')}}</span>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Directory Description</label>
                        </div>
                        <div class="col-md-10">
                            <textarea name="pathDescription" id="" cols="30" rows="7"
                                      class="form-control textarea">@if(!empty($path)) {{$path->description}} @endif</textarea>
                            <span class="text-danger">{{$errors->first('pathDescription')}}</span>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"> <i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $(document).on('click','.browse',function(){
            document.getElementById("browse").click();
        });
    </script>
@stop