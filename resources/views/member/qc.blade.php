
@php $ordersBlades =App\Models\Order::orderBy('id','desc')->paginate(200);@endphp
@php $total=count($ordersBlades);@endphp


@if(!empty($ordersBlades))
    <form action="{{route('member.index')}}" class="SystemForm" method="get" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{Session::token()}}">
        @if($errors->first('notice'))
            <div class="alert alert-success">{{$errors->first('notice')}}</div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>My Orders</h4>
            </div>
            <div class="panel-body" style="padding:10px;">
                <div class="boxList">
                    <div class="boxs">
                        <input type="text" name="listOrder" id="" class="form-control"
                               placeholder="Enter your order locatin ">
                    </div>
                    <div class="boxs">
                        <div class="bt">
                            <button type="submit" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-inbox"></i> List</button>
                        </div>

                    </div>
                    <div class="boxs">
                        <input type="text" name="search" id="" class="form-control" placeholder="Search order by name">
                    </div>
                    <div class="boxs">
                        <div class="bt">
                            <button type="submit" class="btn btn-primary btn-block"><i class="glyphicon-search glyphicon"></i> Search</button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="memberFilter">
                    <div class="box">
                        <input type="date" name="start" id="start" class="form-control">
                    </div>
                    <div class="box">
                        <b>To</b>
                    </div>
                    <div class="box">
                        <input type="date" name="to" id="to" class="form-control">
                    </div>
                    <div class="box">
                        <label class="checkbox-inline">
                            <span>Ready</span><input type="radio" value="ready" name="optionFilter">
                        </label>
                        <label class="checkbox-inline">
                            <span>Not Yet Complete</span><input type="radio" value="not_yet" name="optionFilter">
                        </label>
                        <label class="checkbox-inline">
                            <span>Rest Deadline</span><input type="radio" value="reast_deadline" name="optionFilter">
                        </label>
                    </div>
                    <div class="box">
                        <div>
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-filter"></i> Filter</button>

                        </div>
                    </div>

                </div>

                <hr>

                <table class="excel">
                    <thead>
                    <tr>
                        <th nowrap>ID</th>
                        <th nowrap>Name</th>
                        <th nowrap>Deadline</th>
                        <th nowrap>base_name</th>
                        <th nowrap>layout_name</th>
                        <th nowrap>type</th>
                        <th nowrap>qc_check_name</th>
                        <th nowrap>qc_check_result</th>
                        <th nowrap>qc_check_description</th>
                        <th nowrap>qc_second_check_name</th>
                        <th nowrap>qc_second_check_result</th>
                        <th nowrap>qc_second_check_description</th>
                        <th nowrap>status</th>
                        <th nowrap>date_ready</th>

                    </tr>
                    </thead>
                    <tbody>
                
                    @if(!empty($ordersBlades))
                        <?php $total_Incorrect = 0;$total_qc = 0;?>
                        @foreach($ordersBlades as $order)
                            @if($order->leader_check_result!="")
                                @php $total_Incorrect=$total_Incorrect+1;  @endphp
                            @endif
                            @if($order->qc_check_result!="")
                                @php $total_qc=$total_qc+1;  @endphp

                            @endif
                            <tr>
                                <td contenteditable="true" nowrap>{{$order->id}}</td>
                                <td contenteditable="true" nowrap><a href="">{{$order->order_id}}</a></td>
                                <td contenteditable="true" nowrap>{{$order->dateline}}</td>
                                <td contenteditable="true" nowrap>{{$order->base_name}}</td>
                                <td contenteditable="true" nowrap>{{$order->layout}}</td>
                                <td contenteditable="true" nowrap>{{$order->type}}</td>


                                <td contenteditable="true" nowrap column="qc_check_name" data-id="{{$order->id}}">
                                    {{-- <input type="text" name="qc_check_name" id="" value="{{$order->qc_check_name}}"> --}}
                                    {{$order->qc_check_name}}
                                </td>
                                <td contenteditable="true" nowrap>

                                    <select name="qc_check_result" id=""
                                            class="leader_check_result"  data-id="{{$order->id}}"
                                            @if($order->qc_check_result=="")
                                            style="color:#292b2c;"
                                            @elseif($order->qc_check_result=="1")
                                            style="color:#d9534f;"

                                            @elseif($order->qc_check_result=="2")
                                            style="color:#f0ad4e;"

                                            @elseif($order->qc_check_result=="3")
                                            style="color:#5cb85c;"

                                            @elseif($order->qc_check_result=="4")
                                            style="color:#0275d8;"
                                            @endif

                                    >
                                        <option value="0"
                                                @if($order->qc_check_result=="") selected="selected" @endif>
                                            Leader Check
                                        </option>
                                        <option value="1"
                                                @if($order->qc_check_result=="1") selected="selected" @endif>
                                            Re-correct
                                        </option>
                                        <option value="2"
                                                @if($order->qc_check_result=="2") selected="selected" @endif>
                                            Warning
                                        </option>
                                        <option value="3"
                                                @if($order->qc_check_result=="3") selected="selected" @endif>
                                            Complete
                                        </option>
                                        <option value="4"
                                                @if($order->qc_check_result=="4") selected="selected" @endif>
                                            Ok
                                        </option>
                                    </select>

                                </td>
                                <td contenteditable="true" nowrap column="qc_check_description" data-id="{{$order->id}}">
                                    {{-- <input type="text" name="qc_check_description" id="" value="{{$order->qc_check_description}}"> --}}
                                    {{$order->qc_check_description}}
                                </td>
                                <td contenteditable="true"  column="qc_second_check_name" data-id="{{$order->id}}">
                                    {{-- <input type="text" name="qc_second_check_name" id="" value="{{$order->qc_second_check_name}}"> --}}
                                    {!!$order->qc_second_check_name!!}
                                </td>
                                <td contenteditable="true" nowrap>

                                    <select name="qc_check_result" id=""
                                            class="leader_check_result " data-id="{{$order->id}}"
                                            @if($order->qc_second_check_result=="")
                                            style="color:#292b2c;"
                                            @elseif($order->qc_second_check_result=="1")
                                            style="color:#d9534f;"

                                            @elseif($order->qc_second_check_result=="2")
                                            style="color:#f0ad4e;"

                                            @elseif($order->qc_second_check_result=="3")
                                            style="color:#5cb85c;"

                                            @elseif($order->qc_second_check_result=="4")
                                            style="color:#0275d8;"
                                            @endif

                                    >
                                        <option value="0"
                                                @if($order->qc_second_check_result=="") selected="selected" @endif>
                                            Leader Check
                                        </option>
                                        <option value="1"
                                                @if($order->qc_second_check_result=="1") selected="selected" @endif>
                                            Re-correct
                                        </option>
                                        <option value="2"
                                                @if($order->qc_second_check_result=="2") selected="selected" @endif>
                                            Warning
                                        </option>
                                        <option value="3"
                                                @if($order->qc_second_check_result=="3") selected="selected" @endif>
                                            Complete
                                        </option>
                                        <option value="4"
                                                @if($order->qc_second_check_result=="4") selected="selected" @endif>
                                            Ok
                                        </option>
                                    </select>
                                </td>
                                <td contenteditable="true" nowrap  column="qc_second_check_description" data-id="{{$order->id}}">
                                    {{-- <input type="text" name="qc_second_check_description" id="" value="{{$order->qc_second_check_description}}"> --}}
                                    {{$order->qc_second_check_description}}
                                </td>
                                <td contenteditable="true" nowrap>
                                    {{-- <input type="text" name="" id="" value="@if($order->status==1) Complete @else Not Yet Complete @endif"> --}}
                                    @if($order->status==1) Complete @else Not Yet Complete @endif
                                </td>
                                <td contenteditable="true" nowrap>
                                    {{-- <input type="text" name="" id="" value="{{$order->date_ready}}"> --}}
                                    {{$order->date_ready}}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <hr>
                <div class="alert alert-success">All Group</div>
                <div class="clearfix"></div>
                <p>Total Order: {{@$total}} = 100%</p>
                <p>Re-Correct QC: {{@$total_qc}}= {{(@$total_qc)?(@$total_qc/@$total*100).'%':' 0%'}}</p>
                <p>Evaluate:

                    @if(@$total_qc>0)
                        @php $evaluate=@$total_qc/@$total*100; @endphp
                        @if($evaluate==0)
                            Perfect
                        @elseif($evaluate<=20)
                            Very Good
                        @elseif($evaluate<=30)
                            Good
                        @elseif($evaluate<=40)
                            Accepted
                        @else
                            Failed/Unsatisfactory

                        @endif
                    @else
                        Perfect
                    @endif
                </p>
                <hr>
            </div>

        </div>


        <script type="text/javascript">
            // $('#start').datepicker({dateFormat: 'yy-mm-d'});
            // $('#to').datepicker({dateFormat: 'yy-mm-d'});
            // $('table').DataTable();

            $(document).on("keyup","td",function(){
                var column=$(this).attr('column');
                var description=$(this).text();
                var id=$(this).attr('data-id');
                $.ajax({
                    url:"{{route('update_status_first')}}",
                    type:"get",
                    data:{column:column,description:description,id:id},
                    dataType:"json"
                });
            });
        </script>
    </form>
@endif