<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <script type="text/javascript" src="{{asset('ui')}}/jquery-ui.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('ui/jquery-ui.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>

<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Base</li>
                        </ol>
                        <div class="pangasu">

                        </div>
                        <div class="row">
                            <form method="post" enctype="multipart/form-data">
                                @if(!empty($bases))
                                    @if(!empty($bases->search_by))
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for=""><i class="glyphicon glyphicon-search"></i> Search By</label>
                                                <input type="text" name="{{$bases->search_by}}" id=""
                                                       class="form-control" placeholder="{{str_replace("_"," ",strtoupper($bases->search_by))}}"
                                                       title="{{$bases->search_by}}">
                                            </div>
                                        </div>
                                    @endif
                                        @if(!empty($bases->logic))
                                            <div class="col-md-3" style="position: relative;">
                                                <div class="box_logic">
                                                    <p class="or">OR</p>
                                                    <p class="and active">AND</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-question-sign"></i> Logic</label>
                                                    <input type="hidden" name="logic_hidden" value="AND">

                                                    <input type="text" name="{{$bases->logic}}" id=""
                                                           class="form-control" placeholder="{{str_replace("_"," ",strtoupper($bases->logic))}}"
                                                           title="{{$bases->search_by}}">
                                                </div>
                                            </div>
                                        @endif
                                    @if(!empty($bases->order_by))
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for=""><i class="glyphicon glyphicon-align-left"></i> Order
                                                    By</label>
                                                <input type="text" id="" class="form-control" disabled=""
                                                       value="{{str_replace("_"," ",strtoupper($bases->order_by))}}">
                                                <input type="hidden" name="order_by" id="" class="form-control"
                                                       value="{{$bases->order_by}}">
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($bases->group_by))
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for=""><i class="glyphicon glyphicon-pencil"></i> Group
                                                    By</label>
                                                <input type="text" id="" class="form-control"
                                                       placeholder="Group  by {{$bases->group_by}}" disabled
                                                       value=" {{$bases->group_by}}">
                                                <input type="hidden" name="group_by" value="{{$bases->group_by}}">
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($bases->order))
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label for=""><i class="glyphicon glyphicon-align-justify"></i>
                                                    Order</label>
                                                <input type="text" name="order" id="" class="form-control"
                                                       placeholder="Order {{$bases->order}}" value="{{$bases->order}}"
                                                       disabled>
                                                <input type="hidden" name="order_hidden" value="{{$bases->order}}">
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($bases->deadline))
                                       {{--  <div class="col-md-2">
                                            <div class="form-group">
                                                <label for=""><i class="glyphicon glyphicon-calendar"></i> Risk Deadline</label>
                                                <input type="hidden" name="deadline_hidden"
                                                       value="{{$bases->deadline}}">
                                                <input type="text" name="deadline" id="" class="form-control"
                                                       disabled="" value="{{$bases->deadline}}">
                                            </div>
                                        </div> --}}


                                    @endif
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="">&nbsp;</label>
                                            <button type="submit" class="btn btn-default form-control search"
                                                    style="height:34px;"><i
                                                        class="glyphicon glyphicon-search"></i> <b>GO</b>
                                            </button>
                                        </div>
                                    </div>
                            </form>
                            @endif
                        </div>
                        <form action="{{route('save-new-data-base')}}" method="post" enctype="multipart/form-data">
                            <table class="excel" width="100%">
                                <thead>
                                @if(!empty(@screen_leaders))
                                    <tr>
                                        @for($i=0;$i<count($screen_leaders);$i++)
                                            <th nowrap data-column="{{$screen_leaders[$i]}}">{{$screen_leaders[$i]}}</th>
                                        @endfor
                                        @if(!empty($total))
                                            @if($total==true)
                                                <th> Total</th>
                                            @endif
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($info as $data)
                                    <tr>
                                        @for($i=0;$i<count($screen_leaders);$i++)
                                            <td nowrap data-id="{{$data->id}}" contentEditable="true" class="dynmic"
                                                data-table="bases"
                                                data-columns="{{$screen_leaders[$i]}}">@if(!empty($base_options)) @if(!empty($base_options[$screen_leaders[$i]]))
                                                    @php
                                                        $c=$data->$screen_leaders[$i];
                                                    @endphp
                                                    <select name="" id=""
                                                            class="select_style @if(strtolower($c)=='re-correct' ||strtolower($c)=='recorrect' ||strtolower($c)=='re-correct') red  @elseif(strtolower($c)=='complete') blue @elseif(strtolower($c)=='warning') yellow @endif">
                                                        <option value=""></option>
                                                        @php
                                                            $options=explode("|",$base_options[$screen_leaders[$i]]);
                                                        @endphp
                                                        @if(!empty($options))
                                                            @foreach($options as $option)
                                                                <option value="{{$option}}"
                                                                        @if($option==$data->$screen_leaders[$i]) selected @endif>{{$option}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                @else {!!$data->$screen_leaders[$i]!!} @endif @else  {!!$data->$screen_leaders[$i]!!} @endif
                                            </td>
                                        @endfor
                                        @if(!empty($total))
                                            @if($total==true)
                                                <td> {!!$data->total!!}</td>
                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                                @else

                                @endif
                            </table>
                            <br>

                            {{$info->render()}}

                            <button class="one" type="submit"><span class="glyphicon glyphicon-plus-sign"></span> Add
                                One Row
                            </button>
                            <button class="multi" type="submit"><span class="glyphicon glyphicon-plus-sign"></span> Add
                                Multi Row
                            </button>
                            <button class="csvFile b-save" type="submit"><span class="glyphicon glyphicon-plus-sign"></span> Add CSV
                            </button>
                        </form>
                        <div class="add_row">
                            <table class="excel hidden" width="100%">
                                <tr class="append_tr">
                                    @for($i=0;$i<count($screen_leaders);$i++)
                                        <td contentEditable="true" nowrap data-columns="{{$screen_leaders[$i]}}"
                                            data-table="bases" class="td_new"><input type="hidden"
                                                                                     name="{{$screen_leaders[$i]}}[]"
                                                                                     class="form-control"></td>
                                    @endfor
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modal save cs v file --}}
            <div class="modal fade csvFileModal" id="modal-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title">Upload Your  CSV File</h4>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('csv-table')}}"  method="post" enctype="multipart/form-data" class="baseForm">
                                <input type="hidden" name="table_name" value="bases">
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Chose File</label>
                                            <input type="file" name="csv" id="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary csvSave">Save changes</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    {{-- modal edit field name --}}
    <div class="modal fade modal-edit" id="modal-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Edit Field</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Original Fild Name</label>
                                <input type="text" name="" id="origi" class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="">New Field Name</label>
                                <input type="text" name="" id="new" class="form-control" placeholder="Enter new column">
                            </div>
                            <div class="form-group">
                                <label for="">Data Type</label>
                                <select name="data_type" id="data_type" class="form-control">
                                    <option value="int">int</option>
                                    <option value="text">text</option>
                                    <option value="varchar">varchar</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Length</label>
                                <input type="number" name="length" id="length" class="form-control" value="255">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary save_update_field">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- end modal edit field --}}
    {{--modal drop column--}}
    <div class="modal fade modal-drop" id="modal-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title"> Drop Column</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</body>
@include('bases_update.error')
@include('bases_update.success')
<script type="text/javascript" charset="utf-8" async defer>
    $(document).on("click", ".delete", function (e) {
        var con = confirm("Do you want to delete this table?");
        if (!con) {
            e.preventDefault();
        }
    });
    @if(Session::has('success')) // Laravel 5 (Session('error')
    Command: toastr["success"]("{{Session::get('success')}}")
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @endif
    @if(Session::has('error')) // Laravel 5 (Session('error')
    Command: toastr["error"]("{{Session::get('error')}}")
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @endif
  $(document).on("change", ".select_style ", function (e) {
        var value = $(this).val();
        var id = $(this).parent().attr('data-id');
        var table = $(this).parent().attr('data-table');
        var column = $(this).parent().attr('data-columns');
        jQuery.ajax({
            url: "{{route('dynamic')}}",
            type: "POST",
            dataType: "json",
            cache: "false",
            jsonpCallback: "onJSONPLoad",
            data: {value: value, id: id, table: table, _token: "{{Session::token()}}", column: column},
            complete: function (data) {
                if (data.responseText == "Data base been updated successfully") {
                    // successMessage("Data base been updated successfully");
                } else {
                    errorMessage(data.responseText);
                }
            }
        });
    });
    $(document).on("keyup", ".dynmic", function () {
        var value = $(this).text();
        var id = $(this).attr('data-id');
        var table = $(this).attr('data-table');
        var column = $(this).attr('data-columns');
        jQuery.ajax({
            url: "{{route('dynamic')}}",
            type: "POST",
            dataType: "json",
            cache: "false",
            jsonpCallback: "onJSONPLoad",
            data: {value: value, id: id, table: table, _token: "{{Session::token()}}", column: column},
            complete: function (data) {
                if (data.responseText == "Data base been updated successfully") {
                    // successMessage("Data base been updated successfully");
                } else {
                    errorMessage(data.responseText);
                }
            }
        });
    });
    //add one row
    $(document).on("click", ".one", function (e) {
        e.preventDefault();
        var add_one_row = $(".append_tr").html();
        $("table tbody").append("<tr>" + add_one_row + "</tr>");
    });

    //add multi
    $(document).on("click", ".multi", function (e) {
        e.preventDefault();
        var row = prompt("Please enter number of row ");
        if (isNaN(row)) {
            errorMessage("Please enter only digits");
            return;
        }
        if (row <= 0) {
            errorMessage("Please enter item number greater than 0 ");
            return;
        }
        var add_one_row = $(".append_tr").html();
        for (var i = 1; i <= row; i++) {
            $("table tbody").append("<tr>" + add_one_row + "</tr>");
        }
    });
    $(document).on('keyup', ".td_new", function (e) {
            $(this).parent().find("td").addClass('new_value');
            var text_value=$(this).text();
            var col_name=$(this).attr('data-columns');
            var data_id=$(this).attr('data-id');
            if (typeof data_id == 'undefined'){
                    data_id="";
            }
            jQuery.ajax({
                    url:"{{route('save-new-data-base')}}",
                    type:"POST",
                    data:{_token:"{{Session::token()}}",col_name:col_name,text_value:text_value,data_id:data_id},
                    success:function(data){
                        if(!data){
                            return;
                        }
                        data_id=data;
                        $(".new_value").attr('data-id',data_id);
                    },
                     async: false
            });
    });

    $(document).on("click",".csvFile",function(e){
        e.preventDefault();
        $(".csvFileModal").modal("show");
    });
    $(document).on("click",".csvSave",function(e){
        e.preventDefault();
        $(".baseForm").submit();
    });
    $(document).on("click","table th",function(e){
        $("#origi").val($(this).attr('data-column'));
        $(".modal-edit").modal("show");
    });
    $(document).on("click",".save_update_field",function(){
        var tbl_name="bases";
        var orig=$("#origi").val();
        var news=$("#new").val();
        var data_type=$("#data_type").val();
        var len=$("#length").val();
        if(origi=="" || news==""){
            errorMessage("Please complete the fields in form. All fields are required ");
            return;
        }
        jQuery.ajax({
            url:"{{route('rename-column-base')}}",
            type:"POST",
            data:{tbl_name:tbl_name,origi:orig,news:news,_token:"{{Session::token()}}",data_type:data_type,len:len},
            success:function(data){
                successMessage(data);
                $(".active_edit").text(news.replace(" ","_"));
                $(".modal-edit").modal("hide");
                $(".active_edit").attr('field-name',news);
            }
        });
    });
    $( "table th" ).contextmenu(function(e) {
        var txt=$(this).text();
        var con=confirm("Do you want delete this column");
        if(!con){
            return false;
        }
        jQuery.ajax({
            url:"{{route('drop-column')}}",
            data:{col_name:txt,tbl_name:"bases"},
            type:"GET",
            success:function(){
            }
        });
        return false;
    });
    $(document).on("click",".box_logic p",function () {
       $(".box_logic p").removeClass("active");
       $(this).addClass('active');
       $("input[name='logic_hidden']").val($(this).text());
    });
</script>
@include('bases_update.resizeable')
</html>