<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
        <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="">Database</a></li>
                            <li><a href="">Delete</a></li>
                            <li class="tbl_name">{{$name}}</li>
                        </ol>
                        <div class="pangasu">
                            <div class="alert alert-info">
                                  Table {{$name}}
                            </div>
                        </div>
                        <table class="excel" width="100%">
                            <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Data Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($columns))
                                @foreach($columns as $column)
                                    <tr>
                                        <td field-name="{{$column->Field}}">{{$column->Field}}</td>
                                        <td>{{$column->Type}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@include('bases_update.error')
@include('bases_update.success')
<script  type="text/javascript" charset="utf-8" async defer>
      $(document).on("click",".delete",function(e){
            var con=confirm("Do you want to delete this table?");
            if(!con){
                e.preventDefault();
            }
      });
      @if(Session::has('success')) // Laravel 5 (Session('error')   
            Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('error')) // Laravel 5 (Session('error')   
            Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif

        $(document).on("click","table td:first-child",function(){
            $(".modal-edit").modal("show");
            var tbl_name=$(".tbl_name").text();
            $(this).addClass('active_edit');
            var origi=$(this).attr('field-name');
            var c=confirm("Do you want to delete this field");
            if(!c){
                return;
            }
            jQuery.ajax({
                url:"{{route('delete-column-table')}}",
                type:"POST",
                data:{tbl_name:tbl_name,origi:origi,_token:"{{Session::token()}}"},
                success:function(data){
                    successMessage(data);
                    window.location.reload();
                }
            });
        }); 
</script>
</html>