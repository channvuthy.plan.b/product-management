<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Database</a></li>
                            <li class="breadcrumb-item "><a href="">Add Query Builder</a></li>
                            <li class="breadcrumb-item active">{{ucfirst($name)}}</li>
                        </ol>
                        {{-- <div class="pangasu">
                            <ul class="list-inline">
                                <li class="breadcrumb-item active"><i class="glyphicon glyphicon-edit"></i> Create Query Builder
                                </li>
                                </li>
                            </ul>
                        </div> --}}
                        <div class="alert alert-info">
                            <p>Custom Query  {{$name}} table below</p>
                        </div>
                        <form action="{{route('create-query-builder')}}" method="post">
                            <input type="hidden" name="name" value="{{$name}}">
                            <input type="hidden" name="table_name" value="{{$name}}">
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                            @if(!empty($columns))
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-search"></i> Search By</label>
                                        <select name="search_by" id="" class="form-control">
                                           <option value=""></option>
                                            @foreach($columns as $column)
                                            <option value="{{$column}}" @if(!empty($queries)) @if($queries->search_by==$column)  selected  @endif  @endif> {{$column}}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('search_by')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-pencil"></i> Group By</label>
                                        <select name="group_by" id="" class="form-control">
                                            <option value=""></option>
                                            @foreach($columns as $column)
                                            <option value="{{$column}}" @if(!empty($queries)) @if($queries->group_by==$column)  selected  @endif  @endif >{{$column}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-align-left"></i> Order By</label>
                                        <select name="order_by" id="" class="form-control">
                                           <option value=""></option>
                                            @foreach($columns as $column)
                                            <option value="{{$column}}" @if(!empty($queries)) @if($queries->order_by==$column)  selected  @endif  @endif>{{$column}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for=""><i class="glyphicon glyphicon-align-justify"></i> Order</label>
                                    <select name="order" id="" class="form-control">
				<option value=""></option>
                                        <option value="DESC"  @if(!empty($queries)) @if($queries->order=="DESC")  selected  @endif  @endif>DESC</option>
                                        <option value="ASC"  @if(!empty($queries)) @if($queries->order=="ASC")  selected  @endif  @endif>ASC</option>

                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <label for=""><i class="glyphicon glyphicon-calendar"></i> Deadline</label>
                                    <select name="deadline" id="" class="form-control">
                                           <option value=""></option>
                                            @foreach($columns as $column)
                                            <option value="{{$column}}" @if(!empty($queries)) @if($queries->deadline==$column)  selected  @endif  @endif>{{$column}}</option>
                                            @endforeach
                                        </select>
                                </div>
                                
                            </div>
                            @endif

                        </form>
                        <hr>
                        <a href="" class="btn btn-xs btn-primary" id="submit"><i class="glyphicon glyphicon-save"></i> Save Query Builder</a>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on("click","#submit",function(e){
            e.preventDefault();
            $("form").submit();
        });
        @if(Session::has('success')) // Laravel 5 (Session('error')   
            Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('error')) // Laravel 5 (Session('error')   
            Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
    </script>
</body>
</html>
