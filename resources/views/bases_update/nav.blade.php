<nav id="spy">
    <ul class="sidebar-nav nav">
        <li>
            <a href="{{route('listBaseMember')}}">
                <i class="glyphicon glyphicon-edit"></i> Database
            </a>
        </li>
        @if(!empty($tables))
            @foreach($tables as $table)
                <li>
                    <a href="{{route('database-list',['name'=>$table->name])}}">
                        <i class="@if($table->icon) {{$table->icon}} @else  glyphicon glyphicon-menu-hamburger @endif"></i> {{ucfirst($table->name)}}
                    </a>
                </li>
            @endforeach
        @endif
        <li>
                <a href="{{route('base-management')}}">
                        <i class="glyphicon glyphicon-pencil"></i> Base
                </a>
        </li>
        <li>
                <a href="{{route('base-configuration')}}">
                        <i class="glyphicon glyphicon-wrench"></i> Base Configuration
                </a>
        </li>
    </ul>
</nav>