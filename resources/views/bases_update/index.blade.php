<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Database</li>
                        </ol>
                        <div class="pangasu">
                            <ul class="list-inline">
                                <li><a href="{{route('add-new-table')}}" class=""><i class="glyphicon glyphicon-plus-sign"></i> Add New </a>
                                </li>
                            </ul>
                        </div>
                        <table class="excel" width="100%">
                            <thead>
                            <tr>
                                <th>N<sup>0</sup></th>
                                <th>Table Name</th>
                                <th>Create By</th>
                                <th>Form Builder</th>
                                <th>Query Builder</th>
                                <th>Add|Delete Column</th>
                                <th>Table Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($tables))
                                @foreach($tables as $table)
                                    <tr>
                                        <td>{{$table->id}}</td>
                                        <td>{{ucfirst($table->name)}}</td>
                                        <td>{{Auth::user()->name}}</td>
                                        <td><a href="{{route('form-builder',['name'=>$table->name])}}" class=""><i class="glyphicon glyphicon-plus-sign"></i> Add Form Builder</a></td>
                                        <td><a href="{{route('query-builder',['name'=>$table->name])}}" class=""><i class="glyphicon glyphicon-plus-sign"></i> Add Query Builder</a></td>
                                        <td>
                                            <a href="{{route('edit-column-table',['name'=>$table->name])}}"><i class="glyphicon glyphicon-pencil"></i> Add & Edit Columns</a> |
                                            <a href="{{route('delete-column',['name'=>$table->name])}}" class="text-danger"><i class="glyphicon glyphicon-trash"></i> Delete Columns</a>
                                        </td>
                                        <td><a href="{{route('delete-table',['name'=>$table->name])}}" class="text-danger delete" ><i class="glyphicon glyphicon-trash"></i> Delete Table</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script  type="text/javascript" charset="utf-8" async defer>
      $(document).on("click",".delete",function(e){
            var con=confirm("Do you want to delete this table?");
            if(!con){
                e.preventDefault();
            }
      });
      @if(Session::has('success')) // Laravel 5 (Session('error')   
            Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('error')) // Laravel 5 (Session('error')   
            Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
</script>
</html>