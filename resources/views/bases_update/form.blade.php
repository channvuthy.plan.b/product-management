<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Database</a></li>
                            <li class="breadcrumb-item "><a href="">Add Form Builder</a></li>
                             <li class="breadcrumb-item active">{{ucfirst($name)}}</li>
                        </ol>
                        <div class="pangasu">
                            {{-- <ul class="list-inline">
                                <li class="breadcrumb-item active"><i class="glyphicon glyphicon-edit"></i> Create  Form Builder
                                </li>
                                </li>
                            </ul> --}}
                        </div>
                        <div class="alert alert-info">
                            <p>Edit the row for {{$name}} table below</p>
                        </div>
                        <form action="{{route('create-form-builder')}}" method="post">
                            <input type="hidden" name="name" value="{{$name}}">
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                        <table class="excel" width="100%">
                            <thead>
                                <tr>
                                    <th>Field</th>
                                    <th>Field Info</th>
                                    <th>Input Type</th>
                                    <th>Display Name</th>
                                </tr>
                            </thead>
                           @if(!empty($columns))
                                <tbody>
                                    @for($i=0;$i<count($columns);$i++)
                                        <tr>
                                            <input type="hidden" name="field[]" value="{{$columns[$i]->Field}}">
                                            <td nowrap>{{$columns[$i]->Field}}</td>
                                            <td>{!!$columns[$i]->Type."<br/>".( $columns[$i]->Null == "NO" ? 'required' : '' )!!}</td>
                                            <td>
                                                
                                                <select name="input[]" id=""  class="form-control">
                                                    <option value="input">Input</option>
                                                    <option value="file">Image</option>
                                                    <option value="textarea">Textarea</option>
                                                    <option value="none">No Display</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="display[]" id="" value="{{ucfirst(str_replace("_", " ", $columns[$i]->Field))}}" class="form-control">
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            @endif
                        </table>
                        </form>
                        <hr>
                        <a href="" class="btn btn-xs btn-primary" id="submit"><i class="glyphicon glyphicon-save"></i> Save Form Builder</a>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).on("click","#submit",function(e){
            e.preventDefault();
            $("form").submit();
        });
        @if(Session::has('success')) // Laravel 5 (Session('error')   
        Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
    </script>
</body>
</html>
