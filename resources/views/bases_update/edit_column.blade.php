<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
        <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="">Database</a></li>
                            <li><a href="">Add || Edit Columns</a></li>
                            <li class="tbl_name">{{$name}}</li>
                        </ol>
                        <div class="pangasu">
                            <div class="alert alert-info">
                                Add || Edit table {{$name}}
                            </div>
                        </div>
                     
                        <a href="" class="add"><i class="glyphicon glyphicon-plus-sign"></i> Add New Field</a>
                        <br>
                        <table class="excel" width="100%">
                            <thead>
                            <tr>
                                <th>Field Name</th>
                                <th>Data Type</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($columns))
                                @foreach($columns as $column)
                                    <tr>
                                        <td field-name="{{$column->Field}}">{{$column->Field}}</td>
                                        <td>{{$column->Type}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        {{-- modal edit field name --}}
            <div class="modal fade modal-edit" id="modal-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title">Edit Field</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Original Fild Name</label>
                                        <input type="text" name="" id="origi" class="form-control" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">New Field Name</label>
                                        <input type="text" name="" id="new" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary save_update_field">Save changes</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        {{-- end modal edit field --}}

        {{-- modal add column --}}
                <div class="modal fade modal-add-field" id="modal-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title">Add Field</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="2">Field Name</label>
                                            <input type="text" name="name" id="add_field_name" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Data Type</label>
                                            <select name="type" id="data_type" class="form-control">
                                                <option value="interger">Integer</option>
                                                <option value="string">String</option>
                                                <option value="text">Text</option>
                                                <option value="float">Float</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">After</label>
                                            <select name="" id="after" class="form-control">
                                                @if(!empty($columns))
                                                    @foreach($columns as $column)
                                                       <option value="{{$column->Field}}">{{$column->Field}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary add_field">Save changes</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
        {{--end modal add field  --}}
</body>
@include('bases_update.error')
@include('bases_update.success')
<script  type="text/javascript" charset="utf-8" async defer>
      $(document).on("click",".delete",function(e){
            var con=confirm("Do you want to delete this table?");
            if(!con){
                e.preventDefault();
            }
      });
      @if(Session::has('success')) // Laravel 5 (Session('error')   
            Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('error')) // Laravel 5 (Session('error')   
            Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif

        $(document).on("click","table td:first-child",function(){
            $(".modal-edit").modal("show");
            $(this).addClass('active_edit');
            var ori=$(this).attr('field-name');
            $("#origi").val(ori);
        });
        $(document).on("click",".add",function(e){
            e.preventDefault();
            $(".modal-add-field").modal("show");
        });

        
        $(document).on("click",".save_update_field",function(){
            var tbl_name=$(".tbl_name").text();
            var origi=$("#origi").val();
            var news=$("#new").val();
            if(origi=="" || news==""){
                errorMessage("Please complete the fields in form. All fields are required ");
                return;
            }
            jQuery.ajax({
                url:"{{route('edit-column')}}",
                type:"POST",
                data:{tbl_name:tbl_name,origi:origi,news:news,_token:"{{Session::token()}}"},
                success:function(data){
                        successMessage(data);
                        $(".active_edit").text(news.replace(" ","_"));
                        $(".modal-edit").modal("hide");
                        $(".active_edit").attr('field-name',news);
                }
            });
        });
        $(document).on("click",".add_field",function(){
            var _name=$("#add_field_name").val();
            var data_type=$("#data_type").val();
            var tbl_name=$(".tbl_name").text();
            var after=$("#after").val();
            if(_name=="" || data_type==""){
                 errorMessage("Please complete the fields in form. All fields are required ");
                return ;
            }
            jQuery.ajax({
                url:"{{route('add-new-column')}}",
                type:"POST",
                data:{name:_name,data_type:data_type,after:after,tbl_name:tbl_name,_token:"{{Session::token()}}"},
                success:function(data){
                    successMessage(data);
                    window.location.reload();
                }
            });
        });
</script>
</html>