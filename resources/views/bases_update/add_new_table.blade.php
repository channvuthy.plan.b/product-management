<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <form action="{{route('create-table')}}" method="POST" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="#">Database</a></li>
                                <li class="breadcrumb-item active">Add New</li>
                            </ol>
                            <div class="pangasu">
                    
                            </div>
                            <hr>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Table Name</label>
                                        <input type="text" name="name" id="" class="form-control" required>
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""><a href="" class="icon" ><i class=" glyphicon glyphicon-asterisk"></i> Icon</a></label>
                                        <input type="text" name="icon" id="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Create model for this table?</label>
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="model" value="model">
                                                <span class="slider"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Create migration for this table?</label>
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" name="migrate" value="migrate">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h2 class="h2_tag">Table Fields</h2>
                                    <table class="excel" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Field Name</th>
                                            <th>DB Type</th>
                                            <th>Allow Null?</th>
                                            <th>Key</th>
                                            <th>Default Value</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="parent">
                                            <td>
                                                <input type="text" name="field_name[]" id="" value="id"
                                                       class="form-control">
                                                <span class="text-danger">{{$errors->first('field_name')}}</span>
                                            </td>
                                            <td>
                                                <select name="type[]" class="form-control" id="">
                                                    <option value="integer">Integer</option>
                                                    <option value="string">String</option>
                                                    <option value="float">Float</option>
                                                    <option value="text">Text</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="allow[]" id="" class="form-control">
                                                    <option value="No">No</option>
                                                    <option value="Yes">Yes</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="key[]" id="" class="form-control">
                                                    <option value="">Default</option>
                                                    <option value="primary">Primary Key</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="number" name="default[]" id="" class="form-control">
                                            </td>
                                            <td>
                                                <div class="btn  btn-danger delete_row" ><i class="glyphicon glyphicon-trash"></i>
                                                </div>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    <hr>
                                    <div class="tool">
                                        <ul class="list-inline">
                                            <li><a href="" class="btn btn-default btn-xs" id="add"><i
                                                            class="glyphicon glyphicon-plus-sign"></i> Add New Field</a>
                                            </li>
                                        </ul>
                                        <hr>
                                        <u class="list-inline pull-right">
                                            <li><a href="" class="btn btn-primary submit btn-xs"><i class="glyphicon glyphicon-plus-sign"></i> Add New Table </a></li>
                                        </u>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- modal ion --}}
            <div class="modal fade modal-icon" id="modal-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title">Icon</h4>
                        </div>
                        <div class="modal-body">
                            @include('bases_update.icon')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        {{-- end modal icon --}}
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", "#add", function (e) {
                e.preventDefault();
                var firstRow = $("table .parent");
                $("table tbody").append("<tr>" + firstRow.html() + "</tr>");
            });
            $(document).on("click", ".submit", function (e) {
                e.preventDefault();
                var name = $("input[name='name']").val();
                if (name == "") {
                    $("input[name='name']").focus();
                    return;
                }
                $("form").submit();
            });
            $(document).on("click",".icon",function(e){
                e.preventDefault();
                $(".modal-icon").modal("show");
            });
        });
        $(document).on("click",".delete_row",function(e){
            e.preventDefault();
            var length =$("table tr").length;
            if(length >2){
                $(this).parent().parent().remove();
            }
        });
        @if(Session::has('error')) // Laravel 5 (Session('error')   
        Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('success')) // Laravel 5 (Session('error')   
        Command: toastr["success"]("{{Session::get('success')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
    </script>
</body>
</html>