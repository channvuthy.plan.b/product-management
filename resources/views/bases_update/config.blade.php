<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/select2.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <link rel="stylesheet" href="{{asset('css/select2.css')}}">
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">Base Configuration</li>
                        </ol>
                        <div class="pangasu">
                            <ul class="list-inline">
                                <li>Screen Option</li>

                            </ul>
                            <hr>
                        </div>
                        <form action="{{route('save-config')}}" method="post" enctype="multipart/form-data">
                            <p><b>Leader Screen</b></p>
                            <fieldset class="metabox-prefs">
                                @if(!empty($columns))
                                    <div class="row">
                                        @foreach($columns as $column)
                                            <div class="col-md-3">
                                                <label for=""><input class="" name="leader_screen[]" type="checkbox"
                                                                     id="" value="{{$column}}"
                                                                     @if(!empty($screen_leaders)) @if(in_array($column,$screen_leaders)) checked @endif @endif> {{$column}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </fieldset>
                            <hr>
                            <p><b>Member Screen</b></p>
                            <fieldset class="metabox-prefs">
                                @if(!empty($columns))
                                    <div class="row">
                                        @foreach($columns as $column)
                                            <div class="col-md-3">
                                                <label for=""><input class="" name="member_screen[]" type="checkbox"
                                                                     id="" value="{{$column}}"
                                                                     @if(!empty($screen_members)) @if(in_array($column,$screen_members)) checked @endif @endif> {{$column}}
                                                </label>
                                            </div>
                                        @endforeach
                                        @endif
                                    </div>
                            </fieldset>
                            <hr>
                            <p><b>Add select box to table</b></p>
                            <hr>
                            <fieldset class="metabox-prefs">
                                @if(!empty($columns))
                                    <div class="row">
                                        <div class="col-md-11">
                                            <select name="" class="form-control add_tool" multiple="">
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}">{{$column}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <a href="" class="add"><span
                                                        class="glyphicon glyphicon-plus-sign"></span></a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        @if(!empty($base_options))

                                            @foreach($base_options as $key =>$value)
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="">{{$key}} <span
                                                                    class="glyphicon glyphicon-remove remove"></span></label>
                                                        <input type="hidden" name="column_name[]" value="{{$key}}">
                                                        <textarea name="column_value[]"
                                                                  class="form-control">{{$value}}</textarea>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="add_more"></div>
                                    </div>
                                @endif
                            </fieldset>
                            <hr>
                            <p><b>Query Builder</b></p>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><span class="glyphicon glyphicon-search"></span> Search By</label>
                                        <select name="search_by" id="" class="form-control">
                                            <option value=""></option>
                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($bases)) @if($bases->search_by==$column) selected @endif  @endif>{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><span class="glyphicon glyphicon-question-sign"></span> OR|AND</label>
                                        </label>
                                        <select name="logic" id="" class="form-control">
                                            <option value=""></option>
                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($bases)) @if($bases->logic==$column) selected @endif  @endif>{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-pencil"></i> Group By</label>
                                        <select name="group_by" id="" class="form-control">
                                            <option value=""></option>
                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($bases)) @if($bases->group_by==$column) selected @endif  @endif >{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-align-left"></i> Order By</label>
                                        <select name="order_by" id="" class="form-control">
                                            <option value=""></option>
                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($bases)) @if($bases->order_by==$column) selected @endif  @endif >{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-align-justify"></i> Order</label>
                                        <select name="order" id="" class="form-control">
                                            @if(!empty($columns))
                                                <option value="ASC"
                                                        @if(!empty($bases)) @if($bases->order=='ASC') selected @endif  @endif>
                                                    ASC
                                                </option>
                                                <option value="DESC"
                                                        @if(!empty($bases)) @if($bases->order=='DESC') selected @endif  @endif>
                                                    DESC
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="tbl_name" value="bases">
                              <!--   <div class="col-md-3">
                                    <div class="form-group">
                                        <label for=""><i class="glyphicon glyphicon-calendar"></i> Deadline</label>
                                        <select name="deadline" id="" class="form-control">
                                            <option value=""></option>
                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($bases)) @if($bases->deadline==$column) selected @endif  @endif >{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div> -->
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <p><b>Column Checking</b></p>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Enable Quality Check</label>
                                        <select name="colunm_qc" id="" class="form-control">
                                            <option value=""></option>

                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($qc)) @foreach($qc as $key =>$value) @if($column==$key) selected @endif  @endforeach  @endif>{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Value</label>
                                        <textarea name="value_qc" id="" class="form-control"
                                                  placeholder="Enter  value that qc  can check (Ex: complete)"> @if(!empty($qc)) @foreach($qc as $key =>$value){{$value}}@endforeach @endif</textarea>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Column Status Group</label>
                                        <select name="colunm_group" id="" class="form-control">
                                            <option value=""></option>

                                            @if(!empty($columns))
                                                @foreach($columns as $column)
                                                    <option value="{{$column}}"
                                                            @if(!empty($qc)) @foreach($qc as $key =>$value) @if($column==$key) selected @endif  @endforeach  @endif>{{$column}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Value</label>
                                        <textarea name="value_group" id="" class="form-control" placeholder="Enter  value that qc  can check (Ex: complete)"> @if(!empty($qc)) @foreach($qc as $key =>$value){{$value}}@endforeach @endif</textarea>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary ">Save Change</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" charset="utf-8" async defer>
    $(document).on("click", ".delete", function (e) {
        var con = confirm("Do you want to delete this table?");
        if (!con) {
            e.preventDefault();
        }
    });
    @if(Session::has('success')) // Laravel 5 (Session('error')   
    Command: toastr["success"]("{{Session::get('success')}}")
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @endif
    @if(Session::has('error')) // Laravel 5 (Session('error')   
    Command: toastr["error"]("{{Session::get('error')}}")
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    @endif
    $("select").select2();
    $(document).on("click", ".add", function (e) {
        e.preventDefault();
        var add_tool = $(".add_tool").val();
        try {
            if (add_tool.length > 0) {
                var tool = "";
                for (var i = 0; i < add_tool.length; i++) {
                    tool += "<div class='col-md-6'><div class='form-group'><input type=\"hidden\" name=\"column_name[]\" value=\"" + add_tool[i] + "\"><label>" + add_tool[i] + "</label><textarea name=\"column_value[]\" class='form-control' placeholder='Multi value separated by | symbol'></textarea></div></div>";
                }
                $(".add_more").html(tool);
            }
        } catch (error) {
        }
    });

    $(document).on("click", ".remove", function () {
        var con = confirm("Do you want to remove?");
        if (!con) {
            return;
        }
        $(this).parent().parent().parent().remove();
    });

</script>
</html>