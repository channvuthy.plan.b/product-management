<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Base Management</title>
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <script src="{{asset('js/jquery-1.9.1.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <script type="text/javascript" src="{{asset('js')}}/toastr.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}"/>
</head>
<script type="text/javascript">
    /*Menu-toggle*/
    $(document).ready(function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        });
    });
</script>
<body>
<div id="wrapper" class="">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a id="menu-toggle" href="#" class="glyphicon glyphicon-align-justify btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                    <a href="#">Employee Management System of PLAN-B</a>
                </div>
            </div>
            <div id="navbar" class="collapse navbar-collapse nav navbar-nav navbar-right ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><span class="glyphicon glyphicon-globe"></span></a></li>
                    <li><a href="#about"><span class="glyphicon glyphicon-user"></span> Account Setting</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        @include('bases_update.nav')
    </div>
    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{ucfirst($name)}}</li>
                        </ol>
                        <div class="pangasu">
                            <ul class="list-inline">
                                <li><a href="" class="insert" table="{{$name}}" ><i class="glyphicon glyphicon-plus-sign"></i> Add </a></li>
                                <li><a href="" class="csv"  table="{{$name}}"><i class="glyphicon glyphicon-folder-open"></i>  CSV File</a></li>
                            </ul>
                        </div>
                        <hr>
                        <div class="form_tag row">
                            <form action="{{route('database-search')}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="name" value="{{$name}}">
                            @if(!empty($input_boxs))
                                    @if(!empty($input_boxs->search_by))
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-search"></i> Search By</label>
                                                    <input type="text" name="{{$input_boxs->search_by}}" id="" class="form-control" placeholder="{{$input_boxs->search_by}}" title="{{$input_boxs->search_by}}">
                                                </div>
                                            </div>
                                    @endif
                                    @if(!empty($input_boxs->order_by))
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-align-left"></i> Order By</label>
                                                    <input type="text" id="" class="form-control"  disabled="" value="{{$input_boxs->order_by}}">
                                                    <input type="hidden" name="order_by" id="" class="form-control" value="{{$input_boxs->order_by}}">
                                                </div>
                                            </div>
                                    @endif
                                    @if(!empty($input_boxs->group_by))
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-pencil"></i> Group By</label>
                                                    <input type="text"  id="" class="form-control" placeholder="Group  by {{$input_boxs->group_by}}" disabled value=" {{$input_boxs->group_by}}">
                                                    <input type="hidden" name="group_by" value="{{$input_boxs->group_by}}">
                                                </div>
                                            </div>
                                    @endif
                                    @if(!empty($input_boxs->order))
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-align-justify"></i> Order</label>
                                                    <input type="text" name="order" id="" class="form-control" placeholder="Order {{$input_boxs->order}}" value="{{$input_boxs->order}}" disabled>
                                                    <input type="hidden" name="order" value="{{$input_boxs->order}}">
                                                </div>
                                            </div>
                                    @endif
                                    @if(!empty($input_boxs->deadline))
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for=""><i class="glyphicon glyphicon-calendar"></i>  Deadline</label>
                                                    <input type="hidden" name="deadline_hidden" value="{{$input_boxs->deadline}}">
                                                     <input type="date" name="deadline" id="" class="form-control">
                                                </div>
                                            </div>
                                    @endif
                                    <div class="col-md-2">
                                        <div class="form-group">
                                        <label for="">&nbsp;</label>
                                            <button type="submit" class="btn btn-default form-control"><i class="glyphicon glyphicon-search"></i> Search</button>
                                        </div>
                                    </div>
                                    <hr>
                                @endif
                            </form>
                        </div>
                        
                        <div class="div_tag" style="width:100%;overflow-y: auto;">
                            <table class="excel" width="100%">
                            @if(!empty($columns))
                            <thead>
                            <tr>
                                @for($i=0;$i<count($columns);$i++)
                                <th nowrap>{{$columns[$i]}}</th>
                                @endfor
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($info as $data)
                            <tr>
                                @for($i=0;$i<count($columns);$i++)
                                <td data-id="{{$data->id}}"  contentEditable="true" class="dynmic" data-table="{{$name}}" data-columns="{{$columns[$i]}}">{!!$data->$columns[$i]!!}</td>
                                @endfor
                            </tr>
                            @endforeach
                            </tbody>
                            @endif
                        </table>
                        {{$info->render()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modal form builder --}}
    <div class="form_builder">
    </div>
    {{-- end modal form builder --}}

    {{-- modal upload csv file --}}
    <div class="modal fade" id="csv" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
             <form action="{{route('csv-table')}}" id="form_csv" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="_token" value="{{Session::token()}}">
                 <input type="hidden" name="table_name" id="table_name">
                 <div class="row">
                     <div class="col-md-12">
                         <div class="form-group">
                             <label for="">Upload CSV File</label>
                             <input type="file" name="csv" id="" class="form-control" required="">
                         </div>
                     </div>
                 </div>
             </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary btn-xs" onclick="document.getElementById('form_csv').submit();return false;" ><i class="glyphicon glyphicon-save"></i> Save</a>
              <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
            </div>
          </div>
          
        </div>
      </div>
      
    </div>
    {{-- end modal upload csv file --}}
    @include('bases_update.error');
    @include('bases_update.success')
    <script type="text/javascript">
        $(document).on("click",".insert",function(e){
            e.preventDefault();
            var table_name=$(this).attr('table');
            jQuery.ajax({
                url:"{{route('create-form-builder')}}",
                type:"GET",
                dataType:"html",
                data:{_token:"{{Session::token()}}",table_name:table_name},
                success:function(data){
                    $(".form_builder").html(data);
                    $(".form_builder .modal").modal();
                }
            });
        });
        $(document).on("click",".save",function(e){
            e.preventDefault();
            $(".form_builder form").submit();
        });
        @if(Session::has('error')) // Laravel 5 (Session('error')   
            Command: toastr["error"]("{{Session::get('error')}}")
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        @if(Session::has('success')) // Laravel 5 (Session('error')   
            Command: toastr["success"]("{{Session::get('success')}}")
            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        @endif
        $(document).on("keyup",".dynmic",function(){
            var value=$(this).html();
            var id=$(this).attr('data-id');
            var table=$(this).attr('data-table');
            var column=$(this).attr('data-columns');
            jQuery.ajax({
                url:"{{route('dynamic')}}",
                type:"POST",
                dataType: "json",
                cache: "false",
                jsonpCallback: "onJSONPLoad",
                data:{value:value,id:id,table:table,_token:"{{Session::token()}}",column:column},
                complete:function(data){
                        if(data.responseText=="Data base been updated successfully"){
                            // successMessage("Data base been updated successfully");
                        }else{
                            errorMessage(data.responseText);
                        }
                }
            });
        });
        $(document).on("click",".csv",function(e){
            e.preventDefault();
            var table_name=$(this).attr('table');
            $("#table_name").val(table_name);
            $("#csv").modal("show");
            $(".modal-title").html(table_name);

        });
        (function () {
            var thElm;
            var startOffset;

            Array.prototype.forEach.call(
                document.querySelectorAll("table th"),
                function (th) {
                    th.style.position = 'relative';

                    var grip = document.createElement('div');
                    grip.innerHTML = "&nbsp;";
                    grip.style.top = 0;
                    grip.style.right = 0;
                    grip.style.bottom = 0;
                    grip.style.width = '5px';
                    grip.style.position = 'absolute';
                    grip.style.cursor = 'col-resize';
                    grip.addEventListener('mousedown', function (e) {
                        thElm = th;
                        startOffset = th.offsetWidth - e.pageX;
                    });

                    th.appendChild(grip);
                });

            document.addEventListener('mousemove', function (e) {
                if (thElm) {
                    thElm.style.width = startOffset + e.pageX + 'px';
                }
            });

            document.addEventListener('mouseup', function () {
                thElm = undefined;
            });
        })();
    </script>
</body>
</html>