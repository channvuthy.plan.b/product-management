@extends('layout.admin.master')
@section('content')
    @include('layout.admin.widget.header')
    @include('layout.admin.widget.navbar')
    <div class="col-md-9">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('adminManage')}}"><i class="glyphicon-home glyphicon"></i>&nbsp;</a></li>
                <li><a href="{{route('createUser')}}"><i class="glyphicon-user glyphicon"></i> User</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>
        @if($errors->first('notice'))
            <div class="alert alert-success">
                {{$errors->first('notice')}}
            </div>
            @if(!count($users))
            <div class="alert alert-success">
                No Result
            </div>
        @endif

        @endif
        <div class="panel panel-default SystemForm">
            <div class="panel-heading" style="height:50px;">
                <form action="{{route('searchUser')}}" method="get">
                    <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch">
                    <input type="text" name="search" id="search" placeholder="search...">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Action</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Permission</th>
                        <th>Group</th>
                        <th>Group Type</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td style="width:180px;">
                                <a href="{{route('editUser',['id'=>$user->id])}}" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                                <a href="{{route('deleteUser',['id'=>$user->id])}}" onclick="return confirm('Are you sure to delete this user?')" class="btn btn-danger btn-xs"><span class="glyphicon-trash glyphicon" ></span> Delete</a> 
                                <a href="{{route('activeUser',['id'=>$user->id])}}" class="btn btn-xs @if($user->status==1) btn-warning @else btn-info @endif ">@if($user->status=="1")   <i class="glyphicon glyphicon-ok-circle"></i> Enable&nbsp;@else <i class="glyphicon glyphicon-ban-circle"></i> Disable @endif</a></td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @foreach($user->roles as $role)
                                    {{$role->permission}}
                                @endforeach
                            </td>

                            <td>
                                {{$user->group['name']}}
                            </td>
                            <td>{{$user->group['type']}}</td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
         (function($) {
            $( "#search" ).autocomplete({
                source: "{{route('getTableUser')}}",
                minLength: 1,
                select: function(event, ui) {
                    $('#search').val(ui.item.value);
                }
            });
        })(jQuery);
    </script>
@stop