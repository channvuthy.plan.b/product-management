@extends('layout.admin.master')
@section('content')
    @include('layout.admin.widget.header')
    @include('layout.admin.widget.navbar')
    <div class="col-md-9">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('adminManage')}}"><i class="glyphicon glyphicon-home"></i>&nbsp;</a></li>
                <li><a href="{{route('createGroup')}}"><i class="glyphicon glyphicon-th-large"></i> Group</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>
        @if($errors->first('notice'))
            <div class="alert alert-success">
                {{$errors->first('notice')}}
            </div>
        @endif
          @if(!count($groups))
            <div class="alert alert-success">
                No Result
            </div>
        @endif
        <div class="panel panel-default SystemForm">
            <div class="panel-heading" style="height:52px;">
                <form action="{{route('searchGroup')}}" method="get">
                    <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch">
                    <input type="text" name="search" id="search" placeholder="Search group name">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th style="width:200px;">Action</th>
                        <th>ID</th>
                        <th style="width:140px;">Group Name</th>
        
                        <th style="width:140px;">Group Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                        <tr>
                            <td>
                                <a href="{{route('editGroup',['id'=>$group->id])}}" class="btn btn-primary btn-xs" ><i class="glyphicon glyphicon-edit"></i> Edit</a> 
                                <a href="{{route('deleteGroup',['id'=>$group->id])}}" onclick="return confirm('Are you sure to delete this group?')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a> 
                                <a href="{{route('groupActive',array('id'=>$group->id))}}" class="btn @if($group->active==0) btn-warning @else btn-info @endif btn-xs">@if($group->active=="0")  <i class="glyphicon glyphicon-ban-circle"></i> Disable   @else <i class="  glyphicon glyphicon-ok-circle"></i> Enable &nbsp; @endif</a></td>
                            <td>{{$group->id}}</td>
                            <td>{{$group->name}}</td>
                            <td>{{$group->type}}</td>
                            <td>{{$group->description}}</td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
     <script type="text/javascript">
        (function($) {
            $( "#search" ).autocomplete({
                source: "{{route('getTable')}}",
                minLength: 1,
                select: function(event, ui) {
                    $('#search').val(ui.item.value);
                }
            });
        })(jQuery);

    </script>
@stop