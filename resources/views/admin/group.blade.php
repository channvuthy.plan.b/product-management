@extends('layout.admin.master')
@section('content')
    @include('layout.admin.widget.header')
    @include('layout.admin.widget.navbar')
    <div class="col-md-9">
        <div class="pangasu float">
            <ul class="list-unstyled text-center">
                <li><a href="{{route('adminManage')}}"><i class="glyphicon glyphicon-home"></i>&nbsp;</a></li>
                <li><a href="{{route('createGroup')}}"><i class="glyphicon glyphicon-th-large"></i> Group</a></li>
            </ul>
        </div>
        <div class="clearfix clear-top-normal" style="margin-top:15px;"></div>
        @if($errors->first('notice'))
            <div class="alert alert-success">
                {{$errors->first('notice')}}
            </div>


        @endif
        <form action="{{route('createGroup')}}" class="SystemForm" method="post">
            <input type="hidden" name="_token" value="{{Session::token()}}">

            <div class="panel panel-default  hidden_form @if($errors->first('name')) @else hidden @endif">
                <div class="panel-heading">
                    <h4> Group</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Group Name*</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="name" id="" class="form-control" placeholder="Enter Group Name">
                            <label for="" class="text-danger">{{$errors->first('name')}}</label>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Group Type*</label>
                        </div>
                        <div class="col-md-8">
                            <select name="groupType" id="" class="form-control" required>
                                <option>Choose Group Type</option>
                                <option value="first">First</option>
                                <option value="base">Base</option>
                                <option value="qc">QC</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-2" style="padding-left: 0px;">
                            <label for="">Description</label>
                        </div>
                        <div class="col-md-8">
                            <textarea name="groupDescription" id="" cols="30" rows="5" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="clearfix clear-top-simple"></div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary addPadding" style="height: 35px;"><i class="glyphicon-save glyphicon"></i> Save</button>
                        </div>
                    </div>
                    <div class="clearfix clear-top-simple"></div>
                </div>
                <div class="panel-footer"><h1></h1></div>
            </div>
        </form>
        <a href="#" class="pull-right add_form btn btn-primary" style="display:block;text-decoration:none;"> @if(!$errors->first('name')) <i class="glyphicon glyphicon-plus"></i> Add New Group @else <i class="glyphicon glyphicon-minus"></i>  Hide Form @endif</a>

        <div class="clearfix" style=""></div>
        <div class="panel panel-default SystemForm">
            <div class="panel-heading" style="height:50px;">
                <form action="{{route('searchGroup')}}" method="get">
                    <input type="text" name="search" id="search" placeholder="Search group name">
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                     <img src="{{asset('icon/1489866801_icon-111-search.png')}}" alt="" id="isearch">
                </form>

            </div>
            <div class="panel-body" style="padding:10px;">
                <table class="table table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th style="width:200px;">Action</th>
                        <th>ID</th>
                        <th style="width:140px;">Group Name</th>
                        <th style="width:140px;">Group Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                        <tr>
                            <td style="text-align:center;">
                                <a href="{{route('editGroup',['id'=>$group->id])}}" class="btn btn-primary btn-xs" ><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                <a href="{{route('deleteGroup',['id'=>$group->id])}}" onclick="return confirm('Are you sure to delete this group?')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a> 
                                <a href="{{route('groupActive',array('id'=>$group->id))}}" class="@if($group->active=="0")  btn btn-warning btn-xs  @else  btn btn-info btn-xs @endif ">@if($group->active=="0")  <i class="glyphicon glyphicon-ban-circle"></i> Disable   @else <i class="	glyphicon glyphicon-ok-circle"></i> Enable &nbsp; @endif</a></td>
                            <td>{{$group->id}}</td>
                            <td>{{$group->name}}</td>
                            <td>{{$group->type}}</td>
                            <td>{{$group->description}}</td>
                            
                        </tr>
                    @endforeach
                     <tr>
                            <td colspan="10">Total Group: {{$groupCount}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {{$groups->render()}}
    </div>
    <script type="text/javascript">
        (function($) {
            $( "#search" ).autocomplete({
                source: "{{route('getTable')}}",
                minLength: 1,
                select: function(event, ui) {
                    $('#search').val(ui.item.value);
                }
            });
            $(".add_form").on("click",function(e){
                e.preventDefault();
                var bool=$(".hidden_form").hasClass("hidden");
                if(bool==true){
                 $(".hidden_form").removeClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-minus"></i> Hide Form');
                 return;
                }
                 $(".hidden_form").addClass("hidden");
                 $(this).html('<i class="glyphicon glyphicon-plus"></i> Add New Group');
           
            });
            
        })(jQuery);

    </script>
@stop