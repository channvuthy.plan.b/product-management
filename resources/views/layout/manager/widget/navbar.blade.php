<div class="col-md-2 navLeft">
    <ul class="list-unstyled">
       
            <li class="{{ \Request::route()->getName()=='manager' ? 'active' : '' }} "><a href="{{route('manager')}}"><i class="glyphicon glyphicon-print"></i> Report</a></li>
            {{--  <li><a href="{{route('manager_bases')}}"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Base</a></li>  --}}
            <li class="{{ Request::is('logout') ? 'class="active"' : '' }}" ><a href="{{route('logout')}}"><i class="glyphicon-log-out glyphicon"></i> Logout</a></li>
       
    </ul>
</div>