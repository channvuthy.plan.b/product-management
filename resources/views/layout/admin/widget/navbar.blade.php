<div class="col-md-2 navLeft">

    <ul class="list-unstyled">
        <li class="{{ \Request::route()->getName()=='adminManage' ? 'active' : '' }}"><a href="{{route('adminManage')}}"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</a></li>
        <li class="{{ \Request::route()->getName()=='searchGroup' ? 'active' : '' }}{{ \Request::route()->getName()=='createGroup' ? 'active' : '' }} {{ \Request::route()->getName()=='editGroup' ? 'active' : '' }}"><a href="{{route('createGroup')}}"><i class="glyphicon glyphicon-th-large"></i> Group</a></li>
        <li class="{{ \Request::route()->getName()=='searchUser' ? 'active' : '' }}{{ \Request::route()->getName()=='createUser' ? 'active' : '' }} {{ \Request::route()->getName()=='editUser' ? 'active' : '' }}"><a href="{{route('createUser')}}" ><i class="glyphicon glyphicon-user"></i> User</a></li>
        <li class="{{ \Request::route()->getName()=='createRole' ? 'active' : '' }} {{ \Request::route()->getName()=='editRole' ? 'active' : '' }}"><a href="{{route('createRole')}}"><span class="glyphicon glyphicon-asterisk"></span> Role</a></li>
        <li class="{{ \Request::route()->getName()=='database' ? 'active' : '' }}"><a href="{{route('database')}}"><span class="glyphicon glyphicon-cloud"></span> Database Backup</a></li>
        {{--<li><a href=""><img src="{{asset('icon/1489860445_ic_history_48px.png')}}" alt=""> Log</a></li>--}}
        {{--<li><a href="{{route('getSetting')}}"><img src="{{asset('icon/1489860597_SEO_cogwheels_setting.png')}}" alt=""> Setting</a></li>--}}
        <li><a href="{{route('logout')}}"><span class="	glyphicon glyphicon-send"></span> Logout</a></li>
    </ul>
</div>