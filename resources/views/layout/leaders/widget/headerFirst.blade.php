<div class="header bg-primary">
    <div class="col-md-6">
        <div class="systemTitle font-impact" style="margin:10px 0px 0px 0px;">
            Employee Management System of Plan-B
        </div>

    </div>
    <div class="col-md-6">
        <div class="systemAlert pull-right colorWrite">
            <ul class="list-unstyled">

                <li  style="position: relative" class="message"><a href="#"><i class="glyphicon glyphicon-comment" title="Notification From You"></i>&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <li class="notification" style="position:relative;"><a href=""><i class="glyphicon glyphicon-globe" title="Message form member"></i>&nbsp;&nbsp;&nbsp;</a></li>
                <li class="qcNotification" style="position:relative;"><a href=""><i class="glyphicon glyphicon-envelope" title="Message form QC"></i>&nbsp;&nbsp;&nbsp;</a></li>
                <li style="position: relative"><a href="{{route('profileLeader')}}" style="font-weight: normal"><i class="glyphicon-user glyphicon"></i> {{Auth::user()->name}}</a></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        message();
        notification();
        qc();
        $(".notification a").click(function(e){
            e.preventDefault();
            $(".members").toggle();
            $(".messages").hide();

         $(".qc").hide();
        });
        $("body").on('click','.ProblemNot a',function(){

            var hr=$(this).attr('href');
            hr=hr.replace("#","");
            $("td#"+hr+"").parent().css({"background-color":"rgba(178, 243, 189, 0.44)"});
        });

        $(".message a").click(function(e){
            e.preventDefault();
            $(".messages").toggle();
            $(".members").hide();
            $(".qc").hide();
        });
        $("body").on('click','.Promessage a',function(){
            var hr=$(this).attr('href');
            hr=hr.replace("#","");
            $("td#"+hr+"").parent().css({"background-color":"rgba(178, 243, 189, 0.44)"});
        });
        $("body").on("click",".qcNotification",function (e) {
            e.preventDefault();
            $(".qc").toggle();
            $(".messages").hide();
            $(".members").hide();

        });
    });


    function message(){
        jQuery.ajax({
            url:"{{route('MessageFromLeaderFirst')}}",
            type:"GET",
            dataType:"json",
            data:{},
            success:function(data){
                var notification="";
                var sub="<ul class='messages'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".message").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li><a href="#'+data[i]['order_id']+'">'+data[i]['order_id']+'</a> make by <a href="">'+data[i]['member_name']+'</a> is <b>'+data[i]['leader_check_description']+'</b></li>';
                }
                sub+="<ul>";
                $(".message").append(sub);
            }
        });
    }

    function notification(){
        jQuery.ajax({
            url:"{{route('messageFirst')}}",
            type:"GET",
            dataType:"json",
            data:{},
            success:function(data){
                var notification="";
                var sub="<ul class='members'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".notification").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li><a href="#'+data[i]['order_id']+'">'+data[i]['order_id']+'</a> updated from <a href="">'+data[i]['member_name']+'</a> with problem  <b>'+data[i]['leader_check_description']+'</b></li>';
                }
                sub+="<ul>";
                $(".notification").append(sub);
            }
        });
    }

    function qc(){
        jQuery.ajax({
            url:"{{route('messageFirstQC')}}",
            type:"GET",
            dataType:"json",
            data:{},
            success:function (data) {
                var notification="";
                var sub="<ul class='qc'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".qcNotification").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li><a href="#'+data[i]['order_id']+'">'+data[i]['order_id']+'</a> Re-Correct form QC <a href="">'+data[i]['member_name']+'</a> with problem  <b>'+data[i]['first_checker_problem']+'</b></li>';
                }
                sub+="<ul>";
                $(".qcNotification").append(sub);
            }
        });
    }
</script>