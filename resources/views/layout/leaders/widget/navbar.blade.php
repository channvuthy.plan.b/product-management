<div class="col-md-2 navLeft">
    <ul class="list-unstyled">

        @if(Auth::user()->group['type']=='base')
            <li class="{{ \Request::route()->getName()=='searchVariation' ? 'active' : '' }} {{ \Request::route()->getName()=='createBaseType' ? 'active' : '' }} {{ \Request::route()->getName()=='editVariation' ? 'active' : '' }}"><a href="{{route('createBaseType')}}"><i class="glyphicon glyphicon-th"></i> Variation</a></li>
            <li class="{{ \Request::route()->getName()=='createPattern' ? 'active' : '' }} {{ \Request::route()->getName()=='editPattern' ? 'active' : '' }}"><a href="{{route('createPattern')}}"><i class="glyphicon glyphicon-tags"></i>&nbsp; Pattern</a></li>
            <li class="{{ \Request::route()->getName()=='uploadLayout' ? 'active' : '' }}"><a href="{{route('uploadLayout')}}"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i> &nbsp;Upload Layout</a></li>
            <li class="{{ \Request::route()->getName()=='createLayout' ? 'active' : '' }} {{ \Request::route()->getName()=='editLayout' ? 'active' : '' }}{{ \Request::route()->getName()=='search_layout' ? 'active' : '' }}"><a href="{{route('createLayout')}}"><i class="glyphicon glyphicon-level-up"></i> Layout Name</a></li>
            <li class="{{ \Request::route()->getName()=='uploadVersion' ? 'active' : '' }}{{ \Request::route()->getName()=='editVersion' ? 'active' : '' }}"><a href="{{route('uploadVersion')}}"><i class="glyphicon glyphicon-book"></i> Version</a></li>
            <li class="{{ \Request::route()->getName()=='uploadType' ? 'active' : '' }}{{ \Request::route()->getName()=='editType' ? 'active' : '' }}"><a href="{{route('uploadType')}}"><i class="glyphicon glyphicon-leaf"></i> Type of Template</a></li>
            <li class="{{ \Request::route()->getName()=='getMemberBase' ? 'active' : '' }}"><a href="{{route('getMemberBase')}}"><i class="glyphicon glyphicon-link"></i>&nbsp;Assign Task</a></li>
            <li class="{{ \Request::route()->getName()=='listBaseMember' ? 'active' : '' }} {{ \Request::route()->getName()=='leaderUpdateBase' ? 'active' : '' }}"><a href="{{route('listBaseMember')}}"><i class="glyphicon glyphicon-folder-open"></i>&nbsp; All Base</a>
            </li>
            <li class="{{ \Request::route()->getName()=='baseDirectory' ? 'active' : '' }}"><a href="{{route('baseDirectory')}}"><i class="glyphicon glyphicon-folder-open"></i> &nbsp; Directory</a></li>
            <li><a href="{{route('base-resource')}}"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Base Resource</a></li>
            <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>

        @elseif(Auth::user()->group['type']=='first')
            <li class="{{ \Request::route()->getName()=='leaderFirstGetBase' ? 'active' : '' }}"><a href="{{route('leaderFirstGetBase')}}"><i class="   glyphicon glyphicon-download-alt"></i> Get Base</a></li>
            @if(Auth::user()->lavel=="1")
                <li class="{{ \Request::route()->getName()=='addNewOrder' ? 'active' : '' }}"><a href="{{route('addNewOrder')}}"><i class="glyphicon glyphicon-plus"></i> Add New Order</a></li>
            @endif
            <li class="{{ \Request::route()->getName()=='leaderFirstDirectory' ? 'active' : '' }}"><a href="{{route('leaderFirstDirectory')}}"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Directory</a></li>
            <li class="{{ \Request::route()->getName()=='leaderFirstGetOrder' ? 'active' : '' }}"><a href="{{route('leaderFirstGetOrder')}}"><i class="glyphicon glyphicon-pencil"></i> Order</a></li>
            {{--<li><a href="{{route('FirstLeaderReport')}}"><i class="fa fa-print"></i> Report</a></li>--}}
            <li class="{{ \Request::route()->getName()=='createRole' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>

        @elseif(Auth::user()->group['type']=='qc')
            <li class="{{ \Request::route()->getName()=='leaderCheckBase' ? 'active' : '' }}"><a href="{{route('leaderCheckBase')}}"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Check Base</a></li>
            <li class="{{ \Request::route()->getName()=='leaderCheckFirst' ? 'active' : '' }}"><a href="{{route('leaderCheckFirst')}}"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Check First</a></li>
            <li class="{{ \Request::route()->getName()=='FirstLeaderReport' ? 'active' : '' }}"><a href="{{route('FirstLeaderReport')}}"><i class="   glyphicon glyphicon-print"></i> &nbsp;Report</a></li>
            <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
        @else
            <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
        @endif
    </ul>
</div>