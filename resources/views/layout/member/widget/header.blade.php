<div class="header bg-primary">
    <div class="col-md-6">
        <div class="systemTitle font-impact" style="margin:10px 0px 0px 0px;">
            Employee Management System of Plan-B
        </div>

    </div>
    <div class="col-md-6">
        <div class="systemAlert pull-right colorWrite">
            <ul class="list-unstyled">

                <li style="position: relative" class="message"><a href="#" title="Message From Your Leader"><i class="glyphicon glyphicon-comment"></i></a>&nbsp;</li>
                <li class="notification" style="position: relative"><a href="#" title="You Has Been Update Re-Correct"><i class="glyphicon glyphicon-globe"></i></a>&nbsp;</li>
                <li class="qcMessage" style="position: relative"><a href="#" title="Message From QC(Quality Site)"><i class="glyphicon glyphicon-envelope"></i></a>&nbsp;</li>
                <li><a href="{{route('memberBaseProfile')}}" style="font-weight:normal;"><i class="glyphicon glyphicon-user"></i>&nbsp;{{Auth::user()->name}}</a></li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        notification();
        message();;
        qc();
        $(".notification a").click(function(e){
            e.preventDefault();
            $(".ProblemNot").toggle("100");
        });
        $("body").on('click','.ProblemNot a',function(){
            var hr=$(this).attr('href');
            hr=hr.replace("#","");
            $("td#"+hr+"").parent().css({"background-color":"rgba(178, 243, 189, 0.44)"});
        });

        $(".message a").click(function(e){
            e.preventDefault();
            var x=$(this).parent().parent().parent().find(".messages");
            x.slideToggle();
        });
        $("body").on('click','.Promessage a',function(){
            var hr=$(this).attr('href');
            hr=hr.replace("#","");
            $("td#"+hr+"").parent().css({"background-color":"rgba(178, 243, 189, 0.44)"});
        });
        $("body").on("click",".qcMessage",function (e) {
            e.preventDefault();
            $(".qcNot").toggle();
        });
    });

    function  message() {
        //End notification alert
        jQuery.ajax({
            url:"{{route('singleMessage')}}",
            type:"GET",
            dataType:"json",
            data:{},
            success:function(data){
                var notification="";
                var sub="<ul class='ProblemNot Promessage'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".message").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li><a href="#'+data[i]['name']+'">'+data[i]['name']+'</a> is <b>'+data[i]['leader_check_problem']+'</b></li>';
                }
                sub+="<ul>";
                $(".message").append(sub);
            }
        });
    }
    function  notification() {
        jQuery.ajax({
            url:"{{route('singleNot')}}",
            type:"GET",
            dataType:"json",
            data:{},
            success:function(data){
                var notification="";
                var sub="<ul class='ProblemNot'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".notification").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li>you has been updated <a href="#'+data[i]['name']+'">'+data[i]['name']+'</a>  with problem  <b>'+data[i]['leader_check_problem']+'</b></li>';
                }
                sub+="<ul>";
                $(".notification").append(sub);
            }
        });
    }
    function qc() {
        jQuery.ajax({
            url:"{{route('messageQC')}}",
            type:"GET",
            dataType:"json",
            data:{qc:1},
            success:function (data) {

                var notification="";
                var sub="<ul class='qcNot'>";
                var len=data.length;
                if(len >=0){
                    notification+='<b class="bnot">'+len+'</b>';
                    $(".qcMessage").append(notification);

                }
                for(var i=0;i<data.length;i++){
                    sub+='<li><a href="#'+data[i]['name']+'">'+data[i]['name']+'</a>-<a href="">'+data[i]['userName']+'</a> with problem  <b>'+data[i]['first_checker_problem']+'</b></li>';
                }
                sub+="<ul>";

                $(".qcMessage").append(sub);
            }
        });
    }
</script>