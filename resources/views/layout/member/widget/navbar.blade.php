<div class="col-md-2 navLeft">
    <ul class="list-unstyled">
        @if(!empty(Auth::user()->group['type']))
            @if(Auth::user()->group['type']=='base')
                <li class="{{ \Request::route()->getName()=='createPath' ? 'active' : '' }}"><a href="{{route('createPath')}}"><i class="glyphicon glyphicon-folder-open"></i> &nbsp;Your Directory</a></li>
                <li class="{{ \Request::route()->getName()=='tool' ? 'active' : '' }}"><a href="{{ route('tool') }}"><i class="glyphicon glyphicon-hdd"></i> Tool</a>
                </li>
                <li class="{{ \Request::route()->getName()=='createBase' ? 'active' : '' }}">
                    <a href="@if(Auth::check()) @if(!empty(Auth::user()->path)){{route('createBase')}} @else {{route('createBase')}}@endif @endif"> <i class="glyphicon glyphicon-folder-open"></i>&nbsp; Create Base
                    </a></li>

                <li class="{{ \Request::route()->getName()=='memberViewBase' ? 'active' : '' }}"><a href="{{route('memberViewBase')}}"><i class="glyphicon glyphicon-pencil"></i> Bases</a>
                <li class="{{ \Request::route()->getName()=='memberViewLayout' ? 'active' : '' }}"><a href="{{route('memberViewLayout')}}"><i class="glyphicon glyphicon-book"></i> View
                        Layout</a>
                </li>
                <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="glyphicon glyphicon-log-in"></i> Logout</a>
                </li>
            @elseif(Auth::user()->group['type']=='qc')
                <li class="{{ \Request::route()->getName()=='leaderFirstGetBase' ? 'active' : '' }}"><a href="{{route('leaderFirstGetBase')}}"><i class="glyphicon glyphicon-pencil"></i> &nbsp;Check Base</a>
                <li class="{{ \Request::route()->getName()=='leaderCheckFirst' ? 'active' : '' }} {{ \Request::route()->getName()=='member.index' ? 'active' : '' }}"><a href="{{route('leaderCheckFirst')}}"><i class="glyphicon glyphicon-pencil"></i> &nbsp;Check First</a>
                <li class="{{ \Request::route()->getName()=='FirstLeaderReport' ? 'active' : '' }}"><a href="{{route('FirstLeaderReport')}}"><i class=" glyphicon glyphicon-print"></i> Report</a></li>
                <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout</a>
                </li>

            @elseif(Auth::user()->group['type']=='first')
                <li class="{{ \Request::route()->getName()=='member.index' ? 'active' : '' }}"><a href="{{route('member.index')}}"><i class="glyphicon glyphicon-pencil"></i> Order</a></li>
                <li class="{{ \Request::route()->getName()=='member.directory' ? 'active' : '' }}"><a href="{{route('member.directory')}}"><i class="glyphicon glyphicon-folder-open"></i>&nbsp; Directory</a></li>
                <li class="{{ \Request::route()->getName()=='create.first' ? 'active' : '' }}"><a href="{{route('create.first')}}"><i class="   glyphicon glyphicon-plus"></i> Create First</a></li>
                <li class="{{ \Request::route()->getName()=='logout' ? 'active' : '' }}"><a href="{{route('logout')}}"><i class="glyphicon glyphicon-log-in"></i> Logout</a></li>
            @endif
        @endif
    </ul>
</div>