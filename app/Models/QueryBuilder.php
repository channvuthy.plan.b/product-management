<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueryBuilder extends Model
{
    //
    protected $table="query_builders";
    protected $fillable=['table_name'];
}
