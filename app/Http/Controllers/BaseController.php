<?php

namespace App\Http\Controllers;

use App\Models\Base;
use App\Models\BaseOption;
use App\Models\LeaderScreen;
use App\Models\MemberScreen;
use App\Models\QC;
use App\Models\QueryBuilder;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class BaseController extends Controller
{
    public function getIndex()
    {
        $bases = QueryBuilder::where('table_name', 'bases')->first();
        $base_options = BaseOption::first()->column_name;
        if (!empty($base_options)) {
            $base_options = json_decode($base_options, true);
        }
        $columns = Schema::getColumnListing("bases");
        $tables = \App\Models\TableList::where('created_by', Auth::user()->id)->get();
        $screen_leaders = LeaderScreen::first()->display;
        $info = DB::table("bases")->orderBy('id', 'DESC')->paginate(50);
        if (!empty($screen_leaders)) {
            $screen_leaders = json_decode($screen_leaders);
        }
        return view('bases_update.base')->with('tables', $tables)->with('columns', $columns)->with('screen_leaders', $screen_leaders)->with('info', $info)->with('base_options', $base_options)->with('bases', $bases);
    }

    public function postIndex(Request $request)
    {

        $bases = QueryBuilder::where('table_name', 'bases')->first();
        $base_options = BaseOption::first()->column_name;
        if (!empty($base_options)) {
            $base_options = json_decode($base_options, true);
        }
        $total = false;
        $columns = Schema::getColumnListing("bases");
        $tables = \App\Models\TableList::where('created_by', Auth::user()->id)->get();
        $screen_leaders = LeaderScreen::first()->display;
        $order = "DESC";
        if (!empty($request->order_hidden)) {
            $order = $request->order_hidden;
        }
        if (!empty($request->order_by)) {
            $info = DB::table("bases")->orderBy($request->order_by, $order);
        } else {
            $info = DB::table("bases")->orderBy('id', $order);
        }
        foreach ($request->except(['order_by', 'version_name', 'group_by', 'order', 'deadline_hidden', 'order_hidden', 'logic_hidden']) as $key => $value) {
            if (!empty($value)) {

                if ($request->logic_hidden == "OR") {
                    $info = $info->orWhere("$key", "LIKE", '%' . $value . '%');
                } else {
                    $info = $info->where("$key", "LIKE", '%' . $value . '%');
                }
            }
        }
        if (!empty($request->group_by)) {
            $info = $info->select("*", DB::raw('count(id) as total'));
            $info = $info->groupBy($request->group_by);
            $total = true;
        }
        $info = $info->paginate(50);
        if (!empty($screen_leaders)) {
            $screen_leaders = json_decode($screen_leaders);
        }
        return view('bases_update.base')->with('tables', $tables)->with('columns', $columns)->with('screen_leaders', $screen_leaders)->with('info', $info)->with('base_options', $base_options)->with('bases', $bases)->with('total', $total);
    }

    public function getConfiguration()
    {
        $bases = QueryBuilder::where('table_name', 'bases')->first();
        $base_options = BaseOption::first()->column_name;
        $qc = QC::first();
        if (!empty($qc)) {
            $qc = json_decode($qc->display, true);
        }
        if (!empty($base_options)) {
            $base_options = json_decode($base_options);
        }
        $screen_leaders = LeaderScreen::first()->display;
        if (!empty($screen_leaders)) {
            $screen_leaders = json_decode($screen_leaders);
        }
        $screen_members = MemberScreen::first()->display;
        if (!empty($screen_members)) {
            $screen_members = json_decode($screen_members);
        }
        $columns = Schema::getColumnListing("bases");
        $tables = \App\Models\TableList::where('created_by', Auth::user()->id)->get();
        return view('bases_update.config')->with('tables', $tables)->with('columns', $columns)->with('screen_leaders', $screen_leaders)->with('screen_members', $screen_members)->with('base_options', $base_options)->with('bases', $bases)->with('qc', $qc);
    }

    public function postConfi(Request $request)
    {
        $tbl_name = $request->tbl_name;
        $search_by = $request->search_by;
        $order_by = $request->order_by;
        $group_by = $request->group_by;
        $order = $request->order;
        $logic = $request->logic;
        $deadline = $request->deadline;
        $bases = QueryBuilder::where('table_name', $tbl_name)->first();
        if (count($bases) > 0) {

        } else {
            $bases = new  QueryBuilder();
        }
        $bases->table_name = $tbl_name;
        $bases->search_by = $search_by;
        $bases->group_by = $group_by;
        $bases->order_by = $order_by;
        $bases->order = $order;
        $bases->logic = $logic;
        $bases->deadline = $deadline;
        $bases->save();
        $leaderScreen = LeaderScreen::first();
        $leaderCount = count($leaderScreen);
        try {
            if ($leaderCount == false) {
                $leaderScreen = new LeaderScreen();
                $leaderScreen->display = json_encode($request->leader_screen, true);
            } else {
                $leaderScreen->display = json_encode($request->leader_screen, true);
            }
            $leaderScreen->save();
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        $memberScreen = MemberScreen::first();
        $memberCount = count($memberScreen);
        try {
            if ($memberCount == false) {
                $memberScreen = new MemberScreen();
                $memberScreen->display = json_encode($request->member_screen, true);
            } else {
                $memberScreen->display = json_encode($request->member_screen, true);
            }
            $memberScreen->save();
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        $column_value = $request->column_value;
        $column_name = $request->column_name;
        $array = [];
        if (!empty($column_name)) {
            $i = 0;
            foreach ($column_name as $key => $value) {
                $array[$value] = $column_value[$i];
                $i++;
            }
            $baseOption = BaseOption::first();
            if (count($baseOption) > 0) {
                $baseOption->column_name = json_encode($array);
            } else {
                $baseOption = new BaseOption();
                $baseOption->column_name = json_encode($array);
            }
            $baseOption->save();
        }
        try {
            $qc = QC::first();
            $array = [];
            if (count($qc) > 0) {
                $array[$request->colunm_qc] = $request->value_qc;
                $array = json_encode($array);
                $qc->display = $array;
                $qc->save();
            } else {
                $qc = new QC();
                $array[$request->colunm_qc] = $request->value_qc;
                $array = json_encode($array);
                $qc->display = $array;
                $qc->save();
            }

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        return redirect()->back()->with('sucess', 'Screen option has been saved!');
    }

    public function postSaveNewData(Request $request)
    {
        $data_id = $request->data_id;
        $col_name = $request->col_name;
        $text_value = $request->text_value;
        if (!$data_id) {
            $base = new Base();
            $base->$col_name = "$text_value";
            $base->save();
            return $base->id;
        } else {
            $base = Base::find($data_id);
            $base->$col_name = "$text_value";
            $base->save();
            return $data_id;
        }
    }

    public function postBaseSaveCSVFile()
    {

    }
}
