<?php

namespace App\Http\Controllers;

use App\Models\Base;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Auth;
use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Doctrine;
class TableController extends Controller
{
    public function getEditColumnTable($name){
    	$tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
    	$columns = DB::select('show columns from ' . $name);
    	return view('bases_update.edit_column')->with('tables',$tables)->with('name',$name)->with('columns',$columns);
    }

    public function postEditColumnTable(Request $request){
    	$tbl_name=$request->tbl_name; $tbl_name= str_replace(" ", "_", $tbl_name);
    	$original_column=$request->origi; $original_column= str_replace(" ", "_", $original_column);
    	$new_column_name=$request->news; $new_column_name= str_replace(" ", "_", $new_column_name);
    	if(!empty($tbl_name) && !empty($original_column) && !empty($new_column_name)){
	    	try{
		    	Schema::table("$tbl_name", function (Blueprint $table) use($original_column,$new_column_name){
			    $table->renameColumn("$original_column", "$new_column_name");
			});
		    	return "$original_column has been rename to $new_column_name";
	    	}catch(\Exception $ex){
	    		return response($ex->getMessage(),404);
	    	}
    	}
    }
    public function postAddField(Request $request){
    	$name=$request->name;$name=str_replace(" ", "_", $name);
    	$data_type=$request->data_type; $data_type=str_replace(" ", "_", $data_type);
    	$after=$request->after;
    	$tbl_name=$request->tbl_name;
    	try{
		Schema::table("$tbl_name", function($table) use($name,$data_type,$after,$tbl_name)
		{
		    $table->string("$name")->after("$after");
		});
		return "column has been added to table";
    	}catch(\Exception $ex){
    		return response($ex->getMessage(),404);
    	}
    }

    public function getDeleteTable($name){
    	$tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
    	$columns = DB::select('show columns from ' . $name);
    	return view('bases_update.delete')->with('tables',$tables)->with('name',$name)->with('columns',$columns);
    }

    public function postDeleteField(Request $request){
    	$tbl_name=$request->tbl_name;
    	$origi=$request->origi;
    	try{
    		Schema::table("$tbl_name", function($table) use($origi)
		{
		    $table->dropColumn("$origi");
		});
    		return "Column has been delete successfully";
    	}catch(\Exception $ex){
    		return $ex->getMessage();
    	}
    }
    public function postRenameColumnBase(Request $request){
        $tbl_name=$request->tbl_name;
        $original_name=$request->origi;
        $new_name=$request->news;
        $data_type=$request->data_type;
        $len=$request->len;
        $base=\App\Models\LeaderScreen::first();
        $bases=$base->display;
        $bases=json_decode($bases,true);
        $i=0;
        foreach($bases as $base){
            if($base==$original_name){
                $bases[$i]=$new_name;
            }
            $i++;
        }
        $bases=json_encode($bases,true);
        $base=\App\Models\LeaderScreen::find(1);
        $base->display=$bases;
        $base->save();
        try{
            \DB::statement("ALTER TABLE `$tbl_name` CHANGE `$original_name` `$new_name` $data_type($len)" );
        }catch (\Exception $ex){
            return response($ex->getMessage(),404);
        }
    }
    public function getDropColumn(Request $request){

        $tbl_name=$request->tbl_name;
        $col_name=$request->col_name;
        $col_name=trim($col_name," ");
        $base=\App\Models\LeaderScreen::first();
        $bases=$base->display;
        $bases=json_decode($bases,true);
        $i=0;
        $sql="ALTER TABLE `$tbl_name` DROP $col_name";
        foreach($bases as $base){
            if($base==$col_name){
                $bases[$i]=$col_name;
            }
            $i++;
        }
        $bases=json_encode($bases,true);
        $base=\App\Models\LeaderScreen::find(1);
        $base->display=$bases;
        $base->save();
       try{
           \DB::statement($sql);
           return response("$col_name has been droped",202);
       }catch (\Exception $ex){
           return response($ex->getMessage(),404);
       }
    }
}
