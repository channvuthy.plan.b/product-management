<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\FormBuilder;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;
class FormController extends Controller
{
    public function getFormBuilder(Request $request, $name)
    {
        $columns = DB::select('show columns from ' . $name);
        $tables = \App\Models\TableList::where('created_by', Auth::user()->id)->get();
        return view('bases_update.form')->with('columns', $columns)->with('tables', $tables)->with('name', $name);
    }

    public function postCreateFormBuilder(Request $request)
    {

        if($request->ajax()){
            $table_name=$request->table_name;
            $form_builders=FormBuilder::where('name',$table_name)->get();
            return $this->modalFormBuilder($table_name,$form_builders);
        }
        $user = FormBuilder::firstOrNew(array('name' => $request->name));
        $user->field_name = json_encode($request->field, true);
        $user->display = json_encode($request->display, true);
        $user->input = json_encode($request->input, true);
        $user->created_by = Auth::user()->id;
        $user->save();
        return redirect()->back()->with('success', 'Form has been created');
    }


    function modalFormBuilder($table_name, $form_builder)
    {
        ?>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo ucfirst($table_name); ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <form action="<?php echo route('save-database');?>" method="post" enctype="multipart/form-data">
                                <?php foreach ($form_builder as $form): ?>
                                    <input type="hidden" name="table_name" value="<?php echo $table_name;?>">
                                    <input type="hidden" name="_token" value="<?php echo Session::token();?>">
                                    <?php $i=0; $display=json_decode($form->display); $input=json_decode($form->input); ?>
                                    <?php foreach(json_decode(($form->field_name)) as $form):?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for=""><?php echo $display[$i];?></label>
                                                <?php if($input[$i]=="textarea"):?>
                                                    <textarea name="<?php echo $form;?>" class="form-control"></textarea>
                                                <?php else:?>
                                                    <?php if($input[$i]=="none"):?>
                                                        <input type="<?php echo $input[$i];?>" class="form-control" disabled>
                                                    <?php else:?>
                                                        <input type="<?php echo $input[$i];?>" name="<?php echo $form;?>" id="" class="form-control">
                                                    <?php  endif;?>
                                                <?php endif;?>

                                            </div>
                                        </div>
                                        <?php $i++; endforeach; ?>
                                <?php endforeach; ?>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="" class="btn btn-xs btn-primary save"><i class="glyphicon glyphicon-save"></i>Save Data</a>
                        <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Close</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php
    }
}

