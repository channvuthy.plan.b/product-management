<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Models\Layout;
use Illuminate\Support\Facades\Response;
use App\Models\Type;
use App\Models\Version;

class LayoutController extends Controller
{
    public function getLayout(){
        $layouts=Layout::orderBy('id','DESC')->paginate(10);
        return view('leader.layout')->withLayouts($layouts);

    }
    public function getSearchLayout(Request $request){
        $search=$request->search;
        $layouts=Layout::where('name','LIKE',"%$search%")->paginate(10);
        return view('leader.layout')->withLayouts($layouts);
    }

    public function postLayout(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:layouts'
        ]);
        $name=$request->name;
        $name=rtrim($name,",");
        $layoutDescription=$request->layoutDescription;

        $arrayLayoutName=explode(",",$name);
        foreach ($arrayLayoutName as $name){
            $layout=new Layout();
            $layout->name=$name;
            $layout->description=$layoutDescription;
            $layout->save();
        }

        return redirect()->route('createLayout')->withInput()->withErrors(['notice'=>'The layout has been created']);
    }
    public function getActiveLayout($id){
        $layout=Layout::find($id);
        if($layout->status=="1"){
            $layout->status="0";
        }else if($layout->status=="0"){
            $layout->status="1";
        }
        $layout->save();
        return redirect()->route('createLayout')->withInput()->withErrors(['notice'=>'The layout has been updated']);
    }
    public function getDeleteLayout($id){
        $layout=Layout::find($id);
        $layout->delete();
        return redirect()->route('createLayout')->withInput()->withErrors(['notice'=>'The layout has been deleted']);
    }
    public function getEditLayout($id){
        $layout=Layout::find($id);
        return view('leader.editLayout')->withLayout($layout);
    }
    public function postUpdateLayout(Request $request){
        $id=$request->id;
        $name=$request->name;
        $description=$request->layoutDescription;
        $layout=Layout::find($id);
        $layout->name=$name;
        $layout->description=$description;
        $layout->save();
        return redirect()->route('createLayout')->withInput()->withErrors(['notice'=>'The layout has been updated']);
    }
    public function getUploadLayout(){
        $folders=$this->listFolderFiles('layout');
        return view('leader.uploadLayout')->withFolders($folders);
    }

    public function getDeletePDFFile(Request $request,$name){
       File::delete('layout/'.$name);
       return redirect()->back()->withInput()->withErrors(['notice'=>'The file has been deleted']);
    }
    public function postUploadLayout(Request $request){

        $this->validate($request,[
           'name'=>'required'
        ]);
        $file=$request->file('name');
        $fileName=$file->getClientOriginalName();
        $ex=explode('.',$fileName);
        $ex=end($ex);
        if($ex!="pdf"){
            return redirect()->back()->withInput()->withErrors(['name'=>'Please upload only pdf file']);
        }
        $file->move('layout',$fileName);

        return redirect()->back()->withInput()->withErrors(['notice'=>'The layout has been uploaded']);
    }
    public function listFolderFiles($dir)
    {
        $ffs = scandir($dir);
        $folders="";
        foreach ($ffs as $ff) {
            if ($ff != '.' && $ff != '..') {
                if (is_dir($dir . '/' . $ff)) {
                    listFolderFiles($dir . '/' . $ff);

                }else{
                    $folders[]=$ff;
                }
            }
        }
        return $folders;
    }

    public function getPreview($name,$type){
        $filename = $name;
        $path = public_path("layout/".$filename);
        header('Content-Type', 'application/pdf');
        return response()->file($path);
    }
    public function getUploadVersion(){
        $versions=Version::orderBy('id','DESC')->paginate(10);
        return view('leader.version')->with('versions',$versions);
    }
    public function  postUploadVersion(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:versions'
        ]);
        $name=$request->name;
        $description=$request->description;
        $version=new Version();
        $version->name=$name;
        $version->description=$description;
        $version->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The version has been added']);

    }
    public function postUpdateVersion(Request $request){
        $this->validate($request,[
            'name'=>'required'
        ]);
        $id=$request->id;
        $name=$request->name;
        $description=$request->description;
        $version=Version::find($id);
        $version->name=$name;
        $version->description=$description;
        $version->status=$request->status;
        $version->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The version has been updated']);
    }

    public function getDeleteVersion($id){
        $id=Version::find($id);
        $id->delete();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The version has been deleted']);
    }

    public function getEditVersion($id){

        $version=Version::find($id);
        $versions=Version::orderBy('id','DESC')->paginate(10);
        return view('leader.editVersion')->with('version',$version)->with('versions',$versions);
    }
    public function getUploadType(){
        $types =Type::orderBy('id','DESC')->paginate(10);
        return view('leader.type')->with('types',$types);
    }
    public function getActiveVersion($id){
        $id=Version::find($id);
        if($id->status=="0"){
            $id->status=1;
        }else{
            $id->status=0;
        }
        $id->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The version has updated']);
    }

    public function getActiveType($id){
        $id=Type::find($id);
        if($id->status=="0"){
            $id->status=1;
        }else{
            $id->status=0;
        }
        $id->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The type has updated']);
    }
    public function postUploadType(Request $request){
        $this->validate($request,[
            'name'=>'required:unique:types'
        ]);
        $name=$request->name;
        $description=$request->description;
        $type=new Type();
        $type->name=$name;
        $type->description=$description;
        $type->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The type has been added']);
    }
    public function postUpdateType(Request $request){
        $id=$request->id;
        $name=$request->name;
        $description=$request->description;
        $status=$request->status;
        $type=Type::find($id);
        $type->name=$name;
        $type->description=$description;
        $type->status=$status;
        $type->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The type has been updated']);
    }
    public function  getDeleteType($id){
        $id=Type::find($id);
        $id->delete();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The type has been deleted']);

    }

    public function getEditType($id){
        $type=Type::find($id);
        return view('leader.editType')->with('type',$type);
    }
}
