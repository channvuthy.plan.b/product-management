<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Auth;
use App\Models\TableList;
use DB;
use Excel;
use File;
use App\Models\QueryBuilder;
use Session;

class DatabaseController extends Controller
{
    public function getDatabase()
    {
        $columns = Schema::getColumnListing("resources");
        $resources = Resource::orderBy('id', "DESC")->paginate(100);
        return view('leader.database-management')->with('columns', $columns)->with('resources', $resources);
    }

    public function getEditResource(Request $request)
    {
        $original_name = $request->original_name;
        $new_name = $request->new_name;
        $new_name = str_replace(" ", "_", $new_name);
        Schema::table('resources', function ($table) use ($original_name, $new_name) {
            $table->renameColumn("$original_name", "$new_name");
        });
        return "Success";
    }

    public function getCreateTable()
    {
        $tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
        return view('bases_update.add_new_table')->with('tables',$tables);
    }

    public function postCreateTable(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'field_name'=>'required'
        ]);
        if (!empty($request->model)) {
            if ($model = ucfirst($request->name)) {
                $model=str_replace("", "_", $model);
                $exitCode = \Artisan::call("make:model", ['name' => "Models/$model"]);
            }
        }

        $newtableschema = array(
            'tablename' => str_replace(" ", "_", $request->name),
        );
        $field_name=$request->field_name;
        $type=$request->type;
        $allow=$request->allow;
        $key=$request->key;
        $default=$request->default;
        try{
              Schema::create($newtableschema['tablename'], function ($table) use ($newtableschema,$field_name,$type,$allow,$key,$default) {
               // $table->increments('id')->unique(); //primary key 
                $index_of_array=0;
                foreach($field_name as $name){
                    $name=str_replace(" ", "_", $name);
                    if($key[$index_of_array]=="primary"){
                        $table->increments($name);
                    }else{
                        $bool=($allow[$index_of_array]=="Yes")?true:false;
                        $default_value=($default[$index_of_array])?:Null;
                        $table->$type[$index_of_array]($name)->default($default_value)->nullable($bool);
                    }
                    $index_of_array++;
                }
            });
              $user_id=Auth::user()->id;
              $table_list=new TableList();
              $table_list->name=strtolower($newtableschema['tablename']);
              $table_list->icon=$request->icon;
              $table_list->created_by=$user_id;
              $table_list->save();
              return redirect()->back()->with('success',$newtableschema['tablename']." has been created");
        }catch(\Exception $ex){
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    public function getDatabaseParamete(Request $request,$name){
        $tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
        $columns = Schema::getColumnListing("$name");
        $info=DB::table("$name")->orderBy('id','DESC')->paginate(50);
        $input_boxs=QueryBuilder::where('table_name',$name)->first();
        return view('bases_update.database')->with('tables',$tables)->with('name',$name)->with('columns',$columns)->with('info',$info)->with('input_boxs',$input_boxs);
    }

    public function getSaveDatabase(Request $request){
        $table_name=$request->table_name;
        $data_all = $request->except(['_token','table_name']);
        try{
            if(DB::table($table_name)->insert($data_all)){
                    return redirect()->back()->with('success','Transaction completed!');
            }else{
                throw new Exception("Error Processing Request", 1);
            }
        }catch(\Exception $ex){
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    public function postDynamic(Request $request){
        $id=$request->id;
        $table_name=$request->table;
        $value=$request->value;
        $column=$request->column;
        try{
            DB::table("$table_name")
            ->where('id', "$id")
            ->update(["$column" => $value]);
            return "Data base been updated successfully";
        }catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    public function getSaveCSV(Request $request){
        if(empty($request->csv)){
            return redirect()->back()->with('error','Please choose file');
        }
        $table_name=$request->table_name;
        $file_name=$request->csv;
        $file_name = $file_name->getClientOriginalName();
        $explode_string=explode(".", $file_name);
        $extention=end($explode_string);
        if($extention=="csv" || $extention=="xlsx"){
            try{
                Excel::load($request->file('csv'), function ($reader) use($table_name) {
                    $reader->each(function ($sheet) use($table_name) {
                        DB::table("$table_name")->insert($sheet->toArray());
                    });
                    return redirect()->back()->with('success','Data base uploaded successfully');
                });
            }catch(\Exception $ex){
                return redirect()->back()->with('error',$ex->getMessage());
            }
            return redirect()->back()->with('success','Data base uploaded successfully');
        }
        return redirect()->back()->with('error','File allow only Excel or CSV');
    }

    public function getQueryBuilder($name){
        $columns = Schema::getColumnListing("$name");
        $tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
        $queries=QueryBuilder::where('table_name',$name)->first();
        $info=DB::table("$name")->orderBy('id','DESC')->paginate(50);
        return view('bases_update.query_builder')->with('name',$name)->with('info',$info)->with('columns',$columns)->with('tables',$tables)->with('queries',$queries);
    }

    public function postCreateQueryBuilder(Request $request){
        $this->validate($request,[
            'table_name'=>'required',
            'search_by'=>'required'
        ]);
        try{
            $user = QueryBuilder::firstOrNew(array('table_name' => $request->name));
            $data = $request->except(['_token','name']);
            foreach ($data as $key => $value) {
                $user->$key=$value;
            }
            $user->save();
            return redirect()->back()->with('success','Data has been save successfully!');
        }catch(\Exception $ex){
            return $ex->getMessage();
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    public function postSearch(Request $request){
        $name=$request->name;
        Session::put('tbl_name',$name);
        $q = DB::table("$name");
        $data = $request->except(['_token','deadline','name','order','group_by','order_by','deadline_hidden']);
        foreach($data as $key =>$value){
            $q=$q->where($key,'LIKE','%'.$value.'%');
        }
        $order="DESC";
        if(!empty($request->deadline)){
            $q=$q->where("$request->deadline_hidden",$request->deadline);
        }
        if(!empty($request->order)){
            $order=$request->order;
        }
        if(!empty($request->order_by)){
           $q=$q->orderBy($request->order_by,$order);
        }
        if(!empty($request->group_by)){
            $q=$q->groupBy($request->group_by);
        }
        $tables=\App\Models\TableList::where('created_by',Auth::user()->id)->get();
        $columns = Schema::getColumnListing("$name");
        $info=$q->paginate(100);
        $input_boxs=QueryBuilder::where('table_name',$name)->first();
        return view('bases_update.database')->with('tables',$tables)->with('name',$name)->with('columns',$columns)->with('info',$info)->with('input_boxs',$input_boxs);

    }


    public function getDeleteTableName(Request $request,$name){
        try{
             DB::table('table_lists')->where('name',$name)->delete();
             DB::table('query_builders')->where('table_name',$name)->delete();
             DB::table('form_builders')->where('name',$name)->delete();
             Schema::dropIfExists("$name");
             return redirect()->back()->with('success','Table has been deleted');
        }catch(\Exception $ex){
            $ex->getMessage();
            return redirect()->back()->with('error',$ex->getMessage());
        }
    }

    public function getSearch(){
            $tbl_name=Session::get('tbl_name');
            return redirect()->route('database-list',['name'=>$tbl_name]);  
    }

    public function postEditColumn(Request $request){
        $tblName=$request->tblName;
        $colName=$request->colName;

    }
}
