<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\Group;
use Auth;
use App\Models\Role;
use App\Models\User;
use DB;
use File;
use Response;

class AdminController extends Controller
{
    /*
     * admin index
     */
    public function getIndex()
    {
        return view('admin.index');
    }

    public function getCreateUser()
    {
        $roles=Role::all();
        $groups=Group::where('active',1)->get();
        $userCount=count(User::all());
        $users=User::orderBy('id','DESC')->paginate(10);
        return view('admin.user')->withRoles($roles)->withGroups($groups)->withUsers($users)->with('userCount',$userCount);
    }

    public function postCreateUser(Request $request){
        $this->validate($request,[
           'name'=>'required|unique:users',
           'email'=>'required|email|unique:users',
           'password'=>'required|min:5'

        ]);
        $groupName=$request->groupName;
        $level=$request->lavel;
        $email=$request->email;
        $password=bcrypt($request->password);
        $roleName=$request->roleName;
        $name=$request->name;
        $user=new User();
        $user->name=$name;
        $user->email=$email;
        $user->password=$password;
        $user->lavel=$level;
        $user->status=1;
        $user->group_id=$groupName;
        $user->save();
        if(!empty($roleName)){
            $user->roles()->attach($roleName);
        }

        return redirect()->back()->withInput()->withErrors(['notice'=>'The user has been created']);
    }

    public function getEditUser($id){
        $user=User::find($id);
        $roles=Role::all();
        $groups=Group::all();
        return view('admin.editUser')->withUser($user)->withRoles($roles)->withGroups($groups);

    }
    public function getActiveUser($id){
        $user=User::find($id);
        if($user->status=="1"){
            $user->status="0";
        }else if($user->status=="0"){
            $user->status="1";
        }
        $user->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The user has been updated']);
    }
    public function getDeleteUser($id){
        $user=User::find($id);
        $user->delete();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The user has been deleted']);
    }
    public function postUpdateUser(Request $request){
        $name=$request->name;
        $email=$request->email;
        $password=bcrypt($request->password);
        $roleName=$request->roleName;
        $name=$request->name;
        $user=User::find($request->id);
        $user->name=$name;
        $user->email=$email;
        $user->password=$password;
        $user->status=1;
        $user->lavel=$request->lavel;
        $user->group_id=$request->groupName;
        $user->save();
        $user->roles()->detach();
        if(!empty($roleName)){
            $user->roles()->attach($roleName);
        }

        return redirect()->route('createUser')->withInput()->withErrors(['notice'=>'The user has been deleted']);
    }

    public function getCreateGroup()
    {
        $groupCount=count(Group::all());
        $groups = Group::paginate(10);
        return view('admin.group')->withGroups($groups)->with('groupCount',$groupCount);
    }

    public function postCreateGroup(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:groups',
            'groupType' => 'required'
        ]);
        $name = $request->name;
        $groupType = $request->groupType;
        $groupDescription = $request->groupDescription;
        $group = new Group();
        $group->name = $name;
        $group->type = $groupType;
        $group->description = $groupDescription;
        $group->save();
//        $group->users()->attach(Auth::user()->id);
        return redirect()->back()->withInput()->withErrors(['notice' => 'The group has been created']);

    }

    public function getEditGroup($id)
    {
        $group = Group::find($id);
        return view('admin.editGroup')->withGroup($group);
    }

    public function getDeleteGroup($id)
    {
        $group = Group::find($id);
        $group->delete();
        $users=User::where('group_id',$id)->get();
        foreach ($users as $user){
            $user=User::find($user->id);
            $user->group_id=Null;
            $user->save();
        }
        return redirect()->back()->withInput()->withErrors(['notice' => 'The group has been deleted']);
    }

    public function getActive($id)
    {
        $group = Group::find($id);
        if ($group->active == '1') {
            $group->active = "0";
        } else if ($group->active == '0') {
            $group->active = '1';
        }
        $group->save();
        return redirect()->back()->withInput()->withErrors(['notice' => 'Status group has been updated']);
    }

    public function postUpdateGroup(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $groupType = $request->groupType;
        $groupDescription = $request->groupDescription;
        $group = Group::find($id);
        $group->name = $name;
        $group->type = $groupType;
        $group->description = $groupDescription;
        $group->save();
        return redirect()->route('createGroup')->withInput()->withErrors(['notice' => 'The group has been updated']);

    }

    public function getSearchGroup(Request $request){
        $search=$request->search;
        $groups=Group::where('name','LIKE','%'.$search.'%')->get();
        return view('admin.searchGroup')->with('groups',$groups);
    }

    public function getCreateRole()
    {
        $roles = Role::paginate(10);
        return view('admin.role')->withRoles($roles);
    }

    public function postCreateRole(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles',

        ]);
        $roleName = $request->name;
        $roleName=trim($roleName);
        $roleDescription = $request->roleDescription;
        $role = new Role();
        $role->name = $roleName;
        $role->permission = $request->rolePermission;
        $role->description = $roleDescription;
        $role->save();
        return redirect()->back()->withInput()->withErrors(['notice' => 'The role has been created']);
    }

    public function getEditRole($id)
    {
        $role = Role::find($id);
        return view('admin.editRole')->withRole($role);
    }

    public function getDeleteRole($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->back()->withInput()->withErrors(['notice' => 'The role has been deleted']);

    }

    public function postUpdateRole(Request $request)
    {
        $this->validate($request, [
            'roleName' => 'required',

        ]);
        $roleName = $request->roleName;
        $roleDescription = $request->roleDescription;
        $role = Role::find($request->id);
        $role->name = $roleName;
        $role->permission = $request->rolePermission;
        $role->description = $roleDescription;
        $role->save();
        return redirect()->route('createRole')->withInput()->withErrors(['notice'=>'The role has been updated']);
    }

    public function getSetting(){
        $setting=Setting::first();
        return view('admin.setting')->withSetting($setting);
    }

    public function postSetting(Request $request){
        $setting=Setting::first();
        $autoBackup=$request->autoBackup;
        $backupDate=$request->backupDate;
        $alertDateline=$request->alertDateline;
        if($autoBackup){
            $autoBackup="1";
        }else{
            $autoBackup="0";
        }

        if($alertDateline){
            $alertDateline="1";
        }else{
            $alertDateline="0";
        }

        if(!count($setting)){
            $setting=new Setting();
            $setting->auto_backup=$autoBackup;
            $setting->alert=$alertDateline;
            $setting->date=$backupDate;
            $setting->save();
            return redirect()->back()->withInput()->withErrors(['notice'=>'The setting has been saved']);

        }else{
            $setting=Setting::find($setting->id);
            $setting->auto_backup=$autoBackup;
            $setting->alert=$alertDateline;
            $setting->date=$backupDate;
            $setting->save();
            return redirect()->back()->withInput()->withErrors(['notice'=>'The setting has been updated']);
        }
    }

    public function getAccountAdmin(){
        return view('admin.adminAccount');
    }
    public function getUpdateProfile(Request $request){
        $id=$request->id;
        $user=User::find($id);
        $name=$request->name;
        $password=$request->password;
        if(!empty($password)){
            $password=bcrypt($password);
            $user->password=$password;
        }
        $user->name=$name;
        $user->email=$request->email;
        $user->save();
        return redirect()->route('AccountAdmin')->withInput()->withErrors(['notice'=>'Profile has been updated']);
    }
    public  function  getUpdateMyProfile(Request $request){
        return view('admin.profile');
    }
    public  function getLeaderUpdateMyProfile(Request  $request){
        return view('leader.profile');
    }
    public  function getMemberUpdateMyProfile(){
        return view('member.profile');
    }
    public function postUpdateMyProfile(Request $request){
        $id=Auth::user()->id;
        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);

        $user->save();
        return redirect()->back()->withInput()->withErrors(['notice'=>'The profile has been updated']);
    }

    public function getDatabase(Request $request){
        $tables = DB::select('SHOW TABLES');
        return view('admin.database')->with('tables',$tables);
    }

    public function getBackupDatabase(Request $request){
       $exitCode = \Artisan::call('backup:run');
       return "Dabase backup success";
    }

    public function getListDatabase(Request $request){
             $path ='http---localhost';
             $databases=$this->openDir($path);
             return json_encode($databases);
    }


    public function openDir($dir = null)
        {
            try {
                $folders = "";
                if (!empty($dir)) {
                    $ds = scandir($dir);
                    foreach ($ds as $d) {
                        if ($d != "." && $d != "..") {
                            $folders[] = $d;
                        }
                    }
                    return $folders;

                }
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }
        }

        public function getDeleteDatabase(Request $request){
            $fileName=$request->fileName;
            File::delete("http---localhost/$fileName");
            return "Database has been delete successfully!";
        }

        public function getTable(Request $request){
            $term=$request->term;
            $results = array();
            $queries = DB::table('groups')
                ->where('name', 'like', '%'.$term.'%')
                ->get();
        
            foreach ($queries as $query)
            {
                $results[] = ['value' => $query->name];
            }
            return Response::json($results);
        }

        public function getTableUser(Request $request){
            $term=$request->term;
            $results = array();
            $queries = DB::table('users')
                ->where('name', 'like', '%'.$term.'%')
                ->get();
        
            foreach ($queries as $query)
            {
                $results[] = ['value' => $query->name];
            }
            return Response::json($results);
        }
    public function getSearchUser(Request $request){
        $roles=Role::all();
        $groups=Group::where('active',1)->get();
        $users=User::where('name','LIKE',"%$request->search%")->get();
        return view('admin.searchUser')->withRoles($roles)->withGroups($groups)->withUsers($users);
    }
}
