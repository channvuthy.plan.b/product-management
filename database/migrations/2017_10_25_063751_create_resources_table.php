<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('to_do_completed_copy_to');
            $table->string('location_base_sample');
            $table->string('flow_create_base_df');
            $table->string('qc_sheet');
            $table->string('location_df_qc');
            $table->string('base_df_code_variation_and_layout');
            $table->string('base_format');
            $table->string('base_var_part');
            $table->string('block_layout_var');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources');
    }
}
